# 守护进程 Daemon

一、什么是daemon？
daemon是什么

我们先来简单介绍下什么是服务（service）。Linux系统可以提供很多系统或网络功能(例如http,ftp,mysql等)，提供这些功能当然需要运行一些程序，这些运行的程序我们称为进程，也就是说系统中运行的进程提供了功能，这些进程就是服务。例如：提供了http功能的进程就是http服务。
服务可以说是某一进程，而进程是需要程序去运行产生，也就是说程序运行提供某种服务，这些程序就称为daemon。似乎还是有点绕口，简单地说，daemon是静态的，就是某一程序，daemon运行后会提供某种服务，服务是动态的，是程序运行产生的进程。但通常情况下我们不需要是详细区分daemon和服务，可以将两者理解为等同，即daemon就是服务。（通常说的Linux守护进程就是指daemon或者服务）

二、daemon的命名规则
daemon命名

daemon的名称通常为相应的服务后加上一个d。如cron服务的daemon为crond，http服务的daemon为httpd。

三、daemon主要分类
daemon分类

从daemon的启动和管理方式区分，可以将daemon分为两大类：可独立启动的daemon(stand alone)和由一个超级daemon(super daemon)来统一管理的daemon。
stand alone：可单独自行启动的daemon。这种daemon启动后会一直占用内存和系统资源，最大的优点是响应速度快，多用于能够随时接受远程请求的服务，如WWW的daemon（httpd）、FTP的daemon（vsftpd）等。
super daemon：由一个特殊的daemon来统一管理。这种服务通过一个统一的daemon在需要时负责唤醒，当没有远程请求时，这些服务都是未启动的，等到有远程请求过来时，super daemon才唤醒相应的服务。当远程请求结束后，被唤醒的服务会关闭并释放系统资源。早期的super daemon是inetd，后来被xinetd替代了。
注意：super daemon本身是一个stand alone的服务，因为它需要管理后续的其他服务，所以它自己本身当然需要常驻内存中。

四、daemon的启动脚本和启动方式
服务启动脚本存放位置
Linux系统中有一个公认的目录用来存放服务启动脚本: ”/etc/init.d/“。即使有些Linux系统的服务启动脚本没有放在/etc/init.d/目录下，它也会设置连接文件到/etc/init.d/下的。

服务启动方式
(1)、通用的启动方式：/etc/init.d/* {start|stop|status|restart}。
(2)、部分系统特有的：service [service name] {start|stop|status|restart}
和 rc[service name] {start|stop|status|restart}。
例如重启crontab服务，分别用以上三种方式：
/etc/init.d/crond restart
service crond restart
rccrond restart