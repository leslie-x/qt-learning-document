# Roboshop元件样式调整

在appinfo->images->skin->skin.css中，在特定的类中添加如下语句：

```css
QLabel#label_titile{
color:rgb(220,220,220);
}
```

`#`后面是 ObjectName，在程序中将部件名使用`setObjectName("label_titile")`改成这个名称则这个部件会使用该样式。

