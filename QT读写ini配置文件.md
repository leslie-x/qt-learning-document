# QT读写ini配置文件

[Qt解析INI配置文件](https://blog.csdn.net/lu_embedded/article/details/54707697)

大部分的程序都会有相应的配置文件，如果一个程序没有任何配置文件，那么它对外界是完全封闭的，一旦程序需要修改一些参数必须要修改程序代码本身并重新编译。为了让程序出厂后还能根据需要进行必要的配置，即让程序更有弹性，所以要用配置文件。那么，配置文件就有很多种啦，比如 INI 配置文件、XML 配置文件、YML 配置文件、JSON 配置文件等等，还有就是可以使用系统注册表等。
　　INI 是英文 initialization 的头三个字母的缩写，当然，INI 文件的后缀名不一定是 .ini，你可能看到的是 .cfg、.conf，甚至是 .txt 等等的后缀名。也就是说，INI 文件不一定是 .ini 后缀的，它只表示一种由节、键、值组成的文件编写格式。
　　例如，我在 Windows 上随便搜索的一个 Tencent/QQPhoneAssistant/Settint.ini 文件内容如下：

```ini
[TabBarView_95467980]
RequestTime=1467798280
DefaultViewType=0
[WebGameCtrl]
ShowMutilAccountFlag=0
[QQAssist]
LastLoginQQ=95467980
[CookiesInfo]
last_uin=95467980
```

　　可以看出，INI 文件的格式很简单，一般包括三个基本要素：parameters，sections 和 comments。
　　
　　parameters（参数）
　　INI 所包含的最基本的“元素”就是 parameter；每一个 parameter 都有一个 name 和一个 value，如下所示：

"name = value"

　　sections（节）
　　所有的 parameters 都是以 sections 为单位结合在一起的。所有的 section 名称都是独占一行，并且 sections 名字都被方括号包围着（[ and ]）。在section声明后的所有parameters都是属于该section。对于一个section没有明显的结束标志符，一个section的开始就是上一个section的结束，或者是end of the file。Sections一般情况下不能被nested，当然特殊情况下也可以实现sections的嵌套。

section如下所示：

"[section]"

　　comments（注释）
　　在 INI 文件中注释语句是以分号“；”开始的。所有的所有的注释语句不管多长都是独占一行直到结束的。在分号和行结束符之间的所有内容都是被忽略的。

注释实例如下：

"；comments text"

　　当然，上面讲的都是最经典的 INI 文件格式，随着使用的需求 INI 文件的格式也出现了很多变种。

　　在 Qt 中使用 QSettings 可以很方便地实现 ini 配置文件读写，下面给出一个示例：

```c
#include <QCoreApplication>
#include <QSettings>
#include <QString>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QSettings *configIniWrite = new QSettings("test.ini", QSettings::IniFormat);
    configIniWrite->setValue("/ip/first", "192.168.0.1");
    configIniWrite->setValue("/ip/second", "127.0.0.1");
    configIniWrite->setValue("/port/open", "2222");
    delete configIniWrite;
    
    QSettings *configIniRead = new QSettings("test.ini", QSettings::IniFormat);
    QString ip1Result = configIniRead->value("/ip/first").toString();
    QString ip2Result = configIniRead->value("/ip/second").toString();
    QString portResult = configIniRead->value("/port/open").toString();
    QString testResult = configIniRead->value("/port/close", "2333").toString();
    
    qDebug() << "/ip/first  = " << ip1Result;
    qDebug() << "/ip/second = " << ip2Result;
    qDebug() << "/port/open = " << portResult;
    qDebug() << "/port/close = " << testResult;
    
    delete configIniRead;
    return a.exec();

}
```

　　示例中先由 configIniWrite 生成 ini 文件，再由 configIniRead 读取 ini 文件内容，执行结果如下图所示：

　　打开 test.ini，其内容如下：

```ini
[ip]
first=192.168.0.1
second=127.0.0.1

[port]
open=2222
```



　　因为 configIniWrite 并没有写入 /port/close 参数，所以 configIniRead 读取是预设的缺省值，即 value("/port/close", "2333") 的第二个参数。