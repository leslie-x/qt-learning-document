# Protocol

作用：

比json,XML体积小，序列化速度快，传输速度快。但是通用性差，二进制方式存储，需要通过`.proto`文件查看数据结构

语法：

[Protobuf3语法详解 - 望星辰大海 - 博客园 (cnblogs.com)](https://www.cnblogs.com/tohxyblog/p/8974763.html)

protocol例子：（通过 .proto文件查看）

```
syntax = "proto3";

package rbk.protocol;

import "google/protobuf/wrappers.proto";

message Message_Map {
    string map_directory = 1;
    Message_MapHeader header = 2;
    repeated Message_MapPos normal_pos_list = 3;              //普通点
    repeated Message_MapLine normal_line_list = 4;            //普通线
    repeated Message_MapPos normal_pos3d_list = 5;            //普通3d点云
    repeated Message_AdvancedPoint advanced_point_list = 6;   //站点
    repeated Message_AdvancedLine  advanced_line_list = 7;    //禁行线
    repeated Message_AdvancedCurve advanced_curve_list = 8;   //连接线
    repeated Message_AdvancedArea  advanced_area_list = 9;    //高级区域
    repeated Message_PatrolRoute patrol_route_list = 10;      //
    repeated Message_MapRSSIPos rssi_pos_list = 11;           //反光板点
    repeated Message_ReflectorPos reflector_pos_list = 12;    //反光板/反光柱
    repeated Message_tagPos tag_pos_list = 13;                //二维码
    repeated Message_Primitive primitive_list = 14;           //图元
    repeated Message_ExternalDevice external_device_list = 15; //外部设备
    repeated Message_BinLocations bin_locations_list = 16;     //库位

    repeated Message_MapProperty user_data = 100;              //支持地图中自定义一些属性
}

```

json转proto

```cpp
//json转proto
    Message_Map msgMap ;
    std::string str = fileByteArray.toStdString();
    google::protobuf::util::JsonParseOptions json_parse_option;
    json_parse_option.ignore_unknown_fields = true;
    google::protobuf::util::JsonStringToMessage(str, &msgMap, json_parse_option);

```

protobuf转json

```cpp

//protobuf转json
    google::protobuf::util::JsonOptions json_options;
    json_options.always_print_primitive_fields = false;
    std::string json_buffer;
    google::protobuf::util::MessageToJsonString(msgMap, &json_buffer, json_options);
```

## 生成protobuf文件

编译下载的protobuf.sln文件，生成文件中有protoc.exe程序，使用这个程序生成对应的.cc和.h。

具体：

1. 将.proto文件放到protoc.exe程序的文件夹下
2. 打开命令窗口输入以下命令：`protoc.exe filename.proto --cpp_out=./` 然后在文件夹中会生成对应 .cc和.h

