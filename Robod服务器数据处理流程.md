# RobodPro服务器数据处理流程

### 头部信息类

SCHeadData.h文件：

```c
class SeerHeader{
public:
    SeerHeader(){
        m_header = 0x5A;
        m_version = 0x01;
        m_number = 0;
        m_length = 0;
        m_type = 0;
        memset(m_reserved,0, 6);
    }
	//一共16字节
    uint8_t m_header;
    uint8_t m_version;
    uint16_t m_number;
    uint32_t m_length;
    uint16_t m_type;
    uint8_t m_reserved[6];
};


class SeerData{
public:
    SeerData(){}
    ~SeerData(){}
    int size(){
        return sizeof(SeerHeader) + m_header.m_length;
    }

    //对发送数据 进行格式整理
    int setData(uint16_t type, uint8_t* data, int size,uint16_t number){

        m_header.m_header = 0x5A;
        m_header.m_version = 0x01;
        qToBigEndian(type, (uint8_t*)&(m_header.m_type));
        qToBigEndian(number, (uint8_t*)&(m_header.m_number));

        uint16_t sentType = type-10000; //??返回应该加10000?
        qToBigEndian(sentType, (uint8_t*)&(sentType));
        //把uint16_t拆分2个uint8_t
        uint8_t u1= (uint8_t)(sentType & 0xFFu);
        uint8_t u2 = (uint8_t)((sentType >> 8u)  & 0xFFu);
        m_header.m_reserved[0] = u1;
        m_header.m_reserved[1] = u2;
        m_header.m_reserved[2] = 0;
        m_header.m_reserved[3] = 0;
        m_header.m_reserved[4] = 0;
        m_header.m_reserved[5] = 0;
        if (data) {
            memcpy(m_data, data, size);
        }
        qToBigEndian(size, (uint8_t*)&(m_header.m_length));
        return 16 + size;
    }
private:
    SeerHeader m_header;
    uint8_t m_data[1];
};
```



### 第一步：连接套接字

TcpSocket.cpp文件：

```c
TcpSocket::TcpSocket(QObject *parent) :
    QTcpSocket(parent)
{
    qRegisterMetaType<qintptr>("qintptr");
    qRegisterMetaType<QAbstractSocket::SocketError>("QAbstractSocket::SocketError");
    connect(this,SIGNAL(readyRead()),this,SLOT(slotReadyRead())/*,Qt::QueuedConnection*/);
    connect(this,SIGNAL(disconnected()),this,SLOT(slotDisconnected())/*,Qt::QueuedConnection*/);
    connect(this, SIGNAL(error(QAbstractSocket::SocketError)),this, SLOT(slotError(QAbstractSocket::SocketError)));
    connect(this,SIGNAL(sigWrite(QByteArray)),this,SLOT(slotWrite(QByteArray)));
}

```



TcpUdpInterfaceObject.cpp文件：

```c

    /** 开始监听
 * @brief TcpUdpInterfaceObject::startListenning
 * @return
 */
bool TcpUdpInterfaceObject::startTcpListenning()
{
    QSettings settings(M_ConfigIniFile,QSettings::IniFormat);
    quint16 port = settings.value(QString("Server%1/port").arg(_serverId)).toUInt();
    quint8 threadCount = settings.value(QString("Server%1/threadCount").arg(_serverId)).toUInt();
    quint16 payload =  settings.value(QString("Server%1/payload").arg(_serverId)).toUInt();
    return listenTcp(port,threadCount,payload);
    //m_pServer->listen(QHostAddress::AnyIPv4,nPort)
}

/*配置文件中的参数设置
[Server0]
payload=4096
port=19208
threadCount=1
*/

```

### 第二步：读取并解析头部和数据

TcpSocket.cpp文件：

读取数据

```c
void TcpSocket::slotReadyRead()
{
    resolveTcpReadData(_intptr,this->readAll());
}
```

解析头部和数据

```c

//第二步：解析头部和数据
bool TcpSocket::resolveTcpReadData(qintptr handle, const QByteArray &message)
{
    _lastMessage.append(message); //传入的是一个字节数组
    quint64 size = _lastMessage.size();
    quint64 total_size = 0;
    while(size > 0 && !isNeedStop()){

        if (_lastMessage.at(0) == 0x5A){//如果第一个字节为0x5A
//头部长度为16字节
            if ((total_size == 0 && size >= 16) || (total_size > 0 && size >= total_size)) {
                SeerHeader* header = new SeerHeader;
                memcpy(header, _lastMessage.data(), 16);
                quint64 remaining_size = size - 16;
                qToBigEndian(header->m_length,(uint8_t*)&(total_size)); //设置total_size数据长度

                    //将头部解析出来
                    uint16_t revCommand;
                    uint16_t number;
                    qToBigEndian(header->m_type, (uint8_t*)&(revCommand));
                    qToBigEndian(header->m_number, (uint8_t*)&(number));
                    delete header;
                    if(_lastMessage.size() == 16){//如果只有头部命令没有其他数据
                        setResolveClientData(handle,revCommand,number,QByteArray());
                    }else{//若有其他数据
                        QByteArray byteArray = _lastMessage.mid(16,total_size);
                        setResolveClientData(handle,revCommand,number,byteArray);
                    }
            }}}
 	//...
    return true;
}


```

### 第三步：处理数据并调用对应函数

TcpSocket.cpp文件：

```c
void TcpSocket::setResolveClientData(qintptr handle,
                                     quint16 type,quint16 number,
                                     const QByteArray &byteArray)
{
    //+10000作为返回
    quint16 rType = type ;
    rType += 10000;

    QString typeStr =  CDString(type); //根据端口号与指令int型转字符串,在sqlite数据库查找
    SCDebug<<QString(QStringLiteral("Client To Server: 函数:[%0] 报文类型:%1 (%2)序号: %3 (%4)数据区[size:%5]: %6"))
             .arg(typeStr)
             .arg(type)
             .arg(QString::number(type).toInt(0,16))
             .arg(number)
             .arg(QString::number(number).toInt(0,16))
             .arg(byteArray.size())
             .arg(byteArray.size()>1024 ? QStringLiteral("数据区过大不显示") : QString(byteArray));

    /*
     * 此处处理数据，具体处理函数为_relayObj决定，此项目中_relayObj==RobodProInterface
     * 第二个参数为要执行函数的函数名，Q_ARG为要执行的函数的参数，QueuedConnection表示异步调用函数
     */
    bool ok = QMetaObject::invokeMethod(_relayObj, "slotRevInterface",
                                        Qt::QueuedConnection,//异步
                                        Q_ARG(QString,typeStr),
                                        Q_ARG(qintptr,handle),
                                        Q_ARG(quint16,rType),
                                        Q_ARG(quint16,number),
                                        Q_ARG(QByteArray,byteArray));

    if(!ok){
        SCWarning<<QString(QStringLiteral("QMetaObject::invokeMethod:[%1] fail")).arg(typeStr);
    }else{
        SCDebug<<QString(QStringLiteral("QMetaObject::invokeMethod:[%1] success")).arg(typeStr);
    }
}
```

### 第四步：调用对应函数

RobodProInterface.h/cpp文件：

```c
class RobodProInterface : public QObject
{
    //...
      Q_INVOKABLE void slotRevInterface(const QString &typeStr, qintptr handle, quint16 type, quint16 number, const QByteArray &byteArray);
    //...
       Q_INVOKABLE void robot_core_end_req(qintptr handle, quint16 type, quint16 number, const QByteArray &byteArray);
    Q_INVOKABLE void robot_core_start_req(qintptr handle, quint16 type, quint16 number, const QByteArray &byteArray);
    //...
}

/**************************************************************
******指令函数************************************************
**************************************************************/
void RobodProInterface::slotRevInterface(const QString &typeStr,
                                         qintptr handle,
                                         quint16 type,quint16 number,
                                         const QByteArray &byteArray )
{
    //普通调用如下：
    //    if(typeStr == "robot_core_ntp_timezones_req"){
    //        robot_core_ntp_timezones_req(handle,type,number,byteArray);
    //        return;
    //    }else if(typeStr == "robot_core_upgrade_robot_req"){
    //        robot_core_upgrade_robot_req(handle,type,number,byteArray);
    //        return;
    //    }

    //使用invokeMethod调用更加简单
    //根据函数名执行相应函数
    bool ok = QMetaObject::invokeMethod(this, typeStr.toStdString().c_str(),
                                        Qt::QueuedConnection,
                                        Q_ARG(qintptr,handle),
                                        Q_ARG(quint16,type),
                                        Q_ARG(quint16,number),
                                        Q_ARG(QByteArray,byteArray));
    if(!ok){
        SCWarning<<QString(QStringLiteral("QMetaObject::invokeMethod:[slotRevInterface] fail"));
        SCWarning<<sendError(handle,QString("[%1]%2").arg(typeStr).arg(QStringLiteral("接口不存在")),type,number);
    }else{
        SCDebug<<QString(QStringLiteral("QMetaObject::invokeMethod:[slotRevInterface] success"));
    }
}
```

### 第五步：按需要调用线程函数

使用startRobodInterfaceThread

RobodProInterface.cpp文件：

```c
//线程函数调用
bool RobodProInterface::startRobodInterfaceThread(qintptr handle, quint16 type, quint16 number, const QByteArray &byteArray)
{
    //    RobodProInterfaceThread *pRobodProInterfaceThread {Q_NULLPTR};  //接口线程
    if(pRobodProInterfaceThread->isRunning()){
        SCWarning<<sendError(handle,QStringLiteral("线程繁忙,请稍候再试..."),type,number);
        return false;
    }
    pRobodProInterfaceThread->setNumber(number);
    pRobodProInterfaceThread->setHandle(handle);
    pRobodProInterfaceThread->setThreadType(type);
    pRobodProInterfaceThread->setVar(byteArray);
    pRobodProInterfaceThread->start(QThread::LowPriority);
    return true;
}

//例子
/** 备份当前机器人
 * @brief RobodProInterface::robot_core_backup_robot_req
 * @param handle
 * @param type
 * @param number
 * @param byteArray
 */
void RobodProInterface::robot_core_backup_robot_req(qintptr handle, quint16 type, quint16 number, const QByteArray &byteArray)
{
    startRobodInterfaceThread(handle,type,number,byteArray);
}
```

获取线程返回值，使用信号槽slotInterfacefinished()

```c
/** -------------需要使用线程类处理的返回值---------
 * @brief RobodProInterface::slotInterfacefinished
 */
void RobodProInterface::slotInterfacefinished()
{

    if(-1 == pRobodProInterfaceThread->result()){
        SCWarning<<sendError(pRobodProInterfaceThread->getHandle(),
                             pRobodProInterfaceThread->lastError(),
                             pRobodProInterfaceThread->threadType(),
                             pRobodProInterfaceThread->getNumber());
        //重启 robokit
        startListenApp();
        return ;
    }
    QByteArray b;
    QString sCD = CDString(pRobodProInterfaceThread->threadType() - 10000);
    //更新 robotkit
    if(sCD == "robot_core_upgrade_robot_req"
            || sCD == "robot_core_reduction_robot_req"){ //把指定机器人备份还原){
        pRobodProInterfaceThread->clearVar();
        b = sendData(pRobodProInterfaceThread->threadType(),
                     pRobodProInterfaceThread->getNumber(),
                     pRobodProInterfaceThread->getVar());

        sendDataToClient(pRobodProInterfaceThread->getHandle(),b);
        PublicClass::init()->waitSTime(3);

        //确定是否重启，重启操作系统还是程序
        qDebug("check reboot");
        if(pRobodProInterfaceThread->getUpgradeSelf()){
            pRobodProInterfaceThread->rebootSelf();
        }else if(pRobodProInterfaceThread->getUpgradeAfterReboot()){
            reboot();
        }else startListenApp();

        return;
    }
```



### 第六步：发送数据格式整理

RobodProInterface.cpp文件：

```c
/** 发送指令
 * @brief RoTcp::sendData
 * @param ip
 * @param port
 * @param var
 * @param commandVar
 * @return
 */
QByteArray  RobodProInterface::sendData(int type, int number,const QVariant& var)
{
    QByteArray data;
    int size = 0;
    if(!var.isNull()){
        switch (var.type()) {
        case QVariant::ByteArray:
            data = var.toByteArray();
            break;
        case QVariant::Map:
        case QVariant::List:
            data = QJsonDocument::fromVariant(var).toJson(QJsonDocument::Indented);
            break;
        case QVariant::String:
            data = var.toString().toLocal8Bit();
            break;
        default:
            break;
        }
    }

    uint8_t* buf = Q_NULLPTR;
    SeerData* seer_data = Q_NULLPTR;

    if(data.isEmpty()){
		//不带数据
        buf = new uint8_t[sizeof(SeerHeader)];
        seer_data = (SeerData*)buf;
        size = seer_data->setData(type,Q_NULLPTR,0,number);

    }else{
        //有数据
        std::string json_str = data.toStdString();
        buf = new uint8_t[sizeof(SeerHeader) + json_str.length()];
        seer_data = (SeerData*)buf;
        size = seer_data->setData(type, (uint8_t*)json_str.data(), json_str.length(),number);//按格式整理数据
    }

    QByteArray b((char*)seer_data,size); //深拷贝

    QString info = QString(QStringLiteral(
                               "Server To Client:  函数:[%0] 报文类型:%1 (0x%2)"
                               "序号: %3 (0x%4)"
                               "数据区[size:%5 (0x%6)]: %7"))
            .arg(CDString(type-10000))
            .arg(type)
            .arg(QString::number(type,16))
            .arg(number)
            .arg(QString::number(number,16))
            .arg(data.size())
            .arg(QString::number(data.size(),16))
            .arg(data.size()>1024 ? QStringLiteral("数据区过大不显示") : QString(data));
    delete[] buf; //释放buf
    SCDebug<<info;
    return  b;
}

```

### 第七步：发送数据

```c
/** server 返回的数据
 * @brief RobodProInterface::sendData
 * @param obj
 * @param b
 */
//1
void RobodProInterface::sendDataToClient(qintptr handle,const QByteArray &b)
{
    if(!pTcpUdpInterfaceObject){
        SCWarning<<QStringLiteral("init()函数尚未执行，pTcpUdpInterfaceObject未分配内存");
        return;
    }
    pTcpUdpInterfaceObject->sendDataToClient(handle,b);
}

//2
void TcpUdpInterfaceObject::sendDataToClient(qintptr handle,const QByteArray &array)
{
    m_pServer->sendDataToClient(handle,array);
}

//3
void HCTcpServer::sendDataToClient(qintptr handle,const QByteArray &array)
{
    if(_tcpSocketMap.value(handle)){ //    QMap<qintptr,TcpSocket*>_tcpSocketMap;
        _tcpSocketMap.value(handle)->write2Client(array);
    }else{
        SCWarning<<"!!!!!!!!!!!!!!!!!!!!!error: tcpsocket is null:"<<handle;
    }
}

//4
void TcpSocket::write2Client(const QByteArray &data)
{
    emit sigWrite(data);
    //    this->write(data);
}

//5
void TcpSocket::slotWrite(const QByteArray& message)
{
    this->write(message);
    this->flush();
}
```



概述
Qt元对象系统是Qt最核心的一个基础特性，元对象系统负责信号和插槽对象间通信机制、运行时类型信息和Qt属性系统。为应用程序中使用的每个QObject子类创建一个QMetaObject实例，此实例存储QObject子类的所有元信息。通过元对象系统，你可以查询QObject的某个派生类的类名、有哪些信号、槽、属性、可调用方法等信息，然后可以使用QMetaObject::invokeMethod()调用QObject的某个注册到元对象系统中的方法。而这里，主要就介绍改函数的使用方法，以及大致简介。

QMetaObject::invokeMethod()
QMetaObject的invokeMethod()方法用来调用一个对象的信号、槽、可调用的方法。这是一个静态方法，其函数原型如下：

```cpp
bool QMetaObject::invokeMethod(QObject *obj, const char *member, 
							Qt::ConnectionType type, 
							QGenericReturnArgument ret,
							QGenericArgument val0 = QGenericArgument(nullptr), 
							QGenericArgument val1 = QGenericArgument(), 
							QGenericArgument val2 = QGenericArgument(), 
						    QGenericArgument val3 = QGenericArgument(), 
							QGenericArgument val4 = QGenericArgument(),
							QGenericArgument val5 = QGenericArgument(),
							QGenericArgument val6 = QGenericArgument(), 
							QGenericArgument val7 = QGenericArgument(),
							QGenericArgument val8 = QGenericArgument(),
							QGenericArgument val9 = QGenericArgument())
```


在最新的Qt5.13中，QMetaObject中的invokeMethod函数一共有五个，除上面这个以外其他都是重载函数，就不一一介绍。

该函数就是调用obj对象中的member方法，如果调用成功则返回true，调用失败则返回false，失败的话要么就是没有这个方法要么就是参数传入不对。

参数介绍
第一个参数是被调用对象的指针；
第二个参数是方法的名字；
第三个参数是连接类型。可以指定连接类型，来决定是同步还是异步调用。
如果type是Qt :: DirectConnection，则会立即调用该成员。
如果type是Qt :: QueuedConnection，则会发送一个QEvent，并在应用程序进入主事件循环后立即调用该成员。
如果type是Qt :: BlockingQueuedConnection，则将以与Qt :: QueuedConnection相同的方式调用该方法，除了当前线程将阻塞直到事件被传递。使用此连接类型在同一线程中的对象之间进行通信将导致死锁。
如果type是Qt :: AutoConnection，则如果obj与调用者位于同一个线程中，则会同步调用该成员; 否则它将异步调用该成员。
第四个参数接收被调用函数的返回值；注意，如果调用是异步的，则无法计算返回值。
注意：传入的参数是有个数限制的，可以向成员函数传递最多十个参数（val0，val1，val2，val3，val4，val5，val6，val7，val8和val9）。

QGenericArgument和QGenericReturnArgument是内部帮助程序类。由于可以动态调用信号和槽，因此必须使用Q_ARG（）和Q_RETURN_ARG（）宏来封装参数。Q_ARG（）接受该类型的类型名称和const引用; Q_RETURN_ARG（）接受类型名称和非const引用。

注意：此功能是线程安全的。

```cpp
QString retVal;
QMetaObject::invokeMethod(obj, "compute", Qt::DirectConnection,
                          Q_RETURN_ARG(QString, retVal),
                          Q_ARG(QString, "sqrt"),
                          Q_ARG(int, 42),
                          Q_ARG(double, 9.7));

```

## 处理请求



发送请求

```cpp
 QVariantMap map;
    map.insert("taskId",0);
    map.insert("group",QString(PublicClass::init()->qVariantToByteArray(group)));
    RobotManager::init()->sendData(RobotInterface::E_StatusTcp,QVariant(),map,"robot_status_armtask_req","",RobotInterface::E_CN_RobomapCore);
```



连接请求

```cpp
connect(tcpManager,SIGNAL(sigStatusTcpReceiveAll(QString,QString,QVariant,int,bool)),
                this,SLOT(slotStatusTcpReceiveAll(QString,QString,QVariant,int,bool)),Qt::UniqueConnection);
```



接收请求

```cpp
void Calibration3110::slotStatusTcpReceiveAll(const QString &ip,const QString &sCD,const QVariant & jsonData ,int number,bool isError)
{
    if(number != RobotInterface::E_CN_CalibrationType) return;
    if(isError){
        UiClass::init()->showToastr(QString(QStringLiteral(" %1")).arg(jsonData.toString()),SCToastr::E_Warning,this);
        return;
    }
    if(sCD == "robot_status_calib_support_list_req"){
        QVariant var = PublicClass::init()->getJsonVariant(jsonData.toByteArray());
        relsoveVariant(var);
    }else if(sCD == "robot_status_calib_data_req"){
        showCalibDataDlg(QString::fromLocal8Bit(jsonData.toByteArray()),0);
    }else if(sCD == "robot_status_calib_camera_req"){//二维码相机标定
        UiClass::init()->releaseWaittingWidget(this);
        QVariant var = PublicClass::init()->getJsonVariant(jsonData.toByteArray());
        if(var.isNull() || var.toMap().isEmpty()){
            return;
        }
```

