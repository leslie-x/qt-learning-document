# HttpApi模板

```cpp
#include <QNetworkRequest> 
#include <QNetworkAccessManager>
#include <QNetworkReply>

QString x = "";
    QString url = QString("https://lic.seer-group.com/api/license?order_id= %1").arg(x);
    
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    QNetworkAccessManager naManager(this);
    QNetworkReply* reply = naManager.get(request);
    connect(reply, &QNetworkReply::finished, this, [this,reply](){
        if(reply->error() != QNetworkReply::NoError){
            reply->deleteLater();
            emit sigHttpRequestFinished();
            return;
        }
        qDebug()<<reply->readAll();
        emit sigHttpRequestFinished();
    });
    
    QEventLoop loop;
    connect(this,SIGNAL(sigHttpRequestFinished()),&loop,SLOT(quit()));
    loop.exec();
    
    delete reply;
```

自定义超时reply时间

```cpp
void PepperlFuchsIPConfigDlg::replyTimeout(int timeout)
{
    QTimer::singleShot(timeout,this,[this](){
       if(_reply) _reply->close();
    });
}
```



## 定义模板

```cpp
int main(int argc, char *argv[])
{
    //    QCoreApplication app(argc, argv);
    QtSingleCoreApplication app(QLatin1String(appNameC), argc, argv);
    //    qDebug()<<QStringLiteral("appVersion:")<<app.applicationName()<<app.applicationVersion();
    if (app.isRunning()) {
        qDebug() << app.applicationName()<<" is running~~~~";
        return 0;
    }
    QHttpServer httpserver;
    QString urlBase;
    QString sslUrlBase;
    int byteSize = 1024;

    //get 测试客户端到robod的网速
    httpserver.route("/api/getByte", QHttpServerRequest::Method::Get, [&byteSize] () {
        QByteArray data(byteSize,'a');
        qDebug()<<QString(data);
        return data;
    });
    httpserver.route("/api/getByte/<arg>", QHttpServerRequest::Method::Get, [] (int size) {
        QByteArray data(size,'a');
        qDebug()<<QString(data);
        return data;
    });

    //post 测试robod到指定url的网速
    httpserver.route("/api/toUrl", QHttpServerRequest::Method::POST, [] (const QHttpServerRequest &request) {
        QByteArray body = request.body();
        postUrl(body);
        qDebug()<<QString(body);
        return body;
    });

    quint16 port = httpserver.listen(QHostAddress::Any,7310);
    if (!port){
        qWarning() << QString("error:Http server listen [%1] failed").arg(port);
    }
    //当前IP
    urlBase = QStringLiteral("http://localhost:%1/api/").arg(port);
    qDebug()<<"urlBase:"<<urlBase;
    setAutoBootSoftware(false);
    return app.exec();
}
```



[QtHttpServer路由API](https://www.qt.io/zh-cn/blog/2019/05/28/qhttpserver-routing-api)
