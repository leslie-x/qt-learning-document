# opencv+qt

1.opencv显示图片

<img src="C:\Users\lesliex\AppData\Roaming\Typora\typora-user-images\image-20210929180028097.png" alt="image-20210929180028097" style="zoom: 67%;" />

hpp

三个主要函数

- imgInit()：负责加载图片，转换格式等操作
- imgProc()：负责对图片的处理和算法实现
- imgShow()：负责在窗口显示图片

```cpp

#include "opencv2/opencv.hpp"
using namespace cv;

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void imgInit();
    void imgProc(float contrast,int brightness);
    void imgShow();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    Mat myImg;
    QImage myQImg;
};
```

cpp

```cpp
void MainWindow::imgInit()
{
    Mat ImgData = imread("test.jpg");
    cvtColor(ImgData,myImg,COLOR_BGR2RGB);
    myQImg = QImage((const unsigned char*)(myImg.data),myImg.cols,myImg.rows,QImage::Format_RGB888);
    imgShow();
}

void MainWindow::imgProc(float con, int bri)
{
    Mat src = myImg;
    Mat dst = Mat::zeros(src.size(),src.type());
    src.convertTo(dst,-1,con,bri);
    myQImg = QImage((const unsigned char*)(dst.data),dst.cols,dst.rows,QImage::Format_RGB888);
    imgShow();
}


void MainWindow::imgShow()
{
    ui->label->setPixmap(QPixmap::fromImage(myQImg.scaled(ui->label->size())));//显示到Qlabel上
}


void MainWindow::on_pushButton_clicked()
{
    imgInit();
}
```

注意：

Mat默认的是BGR空间，而QImage是RGB，所以使用`cvtColor`函数预处理一下。

第二个函数`QImage(uchar *data, int width, int height, QImage::Format format)`



2.添加滑条处理图片

![image-20210929184809448](C:\Users\lesliex\AppData\Roaming\Typora\typora-user-images\image-20210929184809448.png)

```cpp
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    imgInit();
    imgShow();
    ui->verticalSlider->setSliderPosition(50);
    ui->verticalSlider_2->setSliderPosition(50);
}

void MainWindow::on_verticalSlider_sliderMoved(int position)
{
    imgProc(position/33.3, 0);
}


void MainWindow::on_verticalSlider_valueChanged(int value)
{
    imgProc(value/33.3, 0);
}


void MainWindow::on_verticalSlider_2_sliderMoved(int position)
{
    imgProc(1.0, position);
}


void MainWindow::on_verticalSlider_2_valueChanged(int value)
{
    imgProc(1.0, value);
}

```

