# 地图导入

smap->proto->地图导入

## 1.Protocol

先来看一个非常简单的例子。假设你想定义一个“搜索请求”的消息格式，每一个请求含有一个查询字符串、你感兴趣的查询结果所在的页数，以及每一页多少条查询结果。可以采用如下的方式来定义消息类型的.proto文件了：

```
syntax = ``"proto3"``;` `message SearchRequest {`` ``string query = ``1``;`` ``int32 page_number = ``2``;`` ``int32 result_per_page = ``3``;``}
```

　　

- 文件的第一行指定了你正在使用proto3语法：如果你没有指定这个，编译器会使用proto2。这个指定语法行必须是文件的非空非注释的第一个行。
- SearchRequest消息格式有3个字段，在消息中承载的数据分别对应于每一个字段。其中每个字段都有一个名字和一种类型。

### 指定字段类型

在上面的例子中，所有字段都是标量类型：两个整型（page_number和result_per_page），一个string类型（query）。当然，你也可以为字段指定其他的合成类型，包括枚举（enumerations）或其他消息类型。

### 分配标识号

正如你所见，在消息定义中，每个字段都有唯一的一个数字标识符。这些标识符是用来在消息的二进制格式中识别各个字段的，一旦开始使用就不能够再改变。注：[1,15]之内的标识号在编码的时候会占用一个字节。[16,2047]之内的标识号则占用2个字节。所以应该为那些频繁出现的消息元素保留 [1,15]之内的标识号。切记：要为将来有可能添加的、频繁出现的标识号预留一些标识号。

最小的标识号可以从1开始，最大到2^29 - 1, or 536,870,911。不可以使用其中的[19000－19999]（ (从FieldDescriptor::kFirstReservedNumber 到 FieldDescriptor::kLastReservedNumber)）的标识号， Protobuf协议实现中对这些进行了预留。如果非要在.proto文件中使用这些预留标识号，编译时就会报警。同样你也不能使用早期[保留](https://developers.google.com/protocol-buffers/docs/proto3?hl=zh-cn#reserved)的标识号。

### JSON 映射

Proto3 支持JSON的编码规范，使他更容易在不同系统之间共享数据，在下表中逐个描述类型。

如果JSON编码的数据丢失或者其本身就是`null`，这个数据会在解析成protocol buffer的时候被表示成默认值。如果一个字段在protocol buffer中表示为默认值，体会在转化成JSON的时候编码的时候忽略掉以节省空间。具体实现可以提供在JSON编码中可选的默认值。

| proto3                 | JSON          | JSON示例                                | 注意                                                         |
| ---------------------- | ------------- | --------------------------------------- | ------------------------------------------------------------ |
| message                | object        | {“fBar”: v, “g”: null, …}               | 产生JSON对象，消息字段名可以被映射成lowerCamelCase形式，并且成为JSON对象键，null被接受并成为对应字段的默认值 |
| enum                   | string        | “FOO_BAR”                               | 枚举值的名字在proto文件中被指定                              |
| map                    | object        | {“k”: v, …}                             | 所有的键都被转换成string                                     |
| repeated V             | array         | [v, …]                                  | null被视为空列表                                             |
| bool                   | true, false   | true, false                             |                                                              |
| string                 | string        | “Hello World!”                          |                                                              |
| bytes                  | base64 string | “YWJjMTIzIT8kKiYoKSctPUB+”              |                                                              |
| int32, fixed32, uint32 | number        | 1, -10, 0                               | JSON值会是一个十进制数，数值型或者string类型都会接受         |
| int64, fixed64, uint64 | string        | “1”, “-10”                              | JSON值会是一个十进制数，数值型或者string类型都会接受         |
| float, double          | number        | 1.1, -10.0, 0, “NaN”, “Infinity”        | JSON值会是一个数字或者一个指定的字符串如”NaN”,”infinity”或者”-Infinity”，数值型或者字符串都是可接受的，指数符号也可以接受 |
| Any                    | object        | {“@type”: “url”, “f”: v, … }            | 如果一个Any保留一个特上述的JSON映射，则它会转换成一个如下形式：`{"@type": xxx, "value": yyy}`否则，该值会被转换成一个JSON对象，`@type`字段会被插入所指定的确定的值 |
| Timestamp              | string        | “1972-01-01T10:00:20.021Z”              | 使用RFC 339，其中生成的输出将始终是Z-归一化啊的，并且使用0，3，6或者9位小数 |
| Duration               | string        | “1.000340012s”, “1s”                    | 生成的输出总是0，3，6或者9位小数，具体依赖于所需要的精度，接受所有可以转换为纳秒级的精度 |
| Struct                 | object        | { … }                                   | 任意的JSON对象，见struct.proto                               |
| Wrapper types          | various types | 2, “2”, “foo”, true, “true”, null, 0, … | 包装器在JSON中的表示方式类似于基本类型，但是允许nulll，并且在转换的过程中保留null |
| FieldMask              | string        | “f.fooBar,h”                            | 见fieldmask.proto                                            |
| ListValue              | array         | [foo, bar, …]                           |                                                              |
| Value                  | value         |                                         | 任意JSON值                                                   |
| NullValue              | null          |                                         | JSON null                                                    |

### Oneof

如果你的消息中有很多可选字段， 并且同时至多一个字段会被设置， 你可以加强这个行为，使用oneof特性节省内存. 相当于'union'
Oneof字段就像可选字段， 除了它们会共享内存， 至多一个字段会被设置。 设置其中一个字段会清除其它字段。 


###　　读取示例

```cpp
void MainWindow::on_actionmsgFile_triggered()
{
    QString curPath = QDir::currentPath();
    QString Title = "choose a file";
    QString filter = "smap(*.smap)";
    QString FilePath = QFileDialog::getOpenFileName(this,Title,curPath,filter);
    QFile file(FilePath);
    if(file.exists())
        file.open(QIODevice::ReadOnly);
    else return;

    QByteArray fileByteArray = file.readAll();
    file.close();

    //json转proto
    Message_Map msgMap ;
    std::string str = fileByteArray.toStdString();
    google::protobuf::util::JsonParseOptions json_parse_option;
    json_parse_option.ignore_unknown_fields = true;
    google::protobuf::util::JsonStringToMessage(str, &msgMap, json_parse_option);
    msgMap.mutable_header()->set_map_name("hello");

    qDebug()<<QString::fromStdString(msgMap.header().map_name());

    //放大倍率
    int mulriple = 40;
    {
        //获取场景大小信息

        qreal scenePosX = msgMap.header().min_pos().x() *mulriple;
        qreal scenePosY = msgMap.header().min_pos().y() *mulriple;
        qreal sceneEdgeX = msgMap.header().max_pos().x() *mulriple;
        qreal sceneEdgeY = msgMap.header().max_pos().y() *mulriple;

        qreal sceneWidth = sceneEdgeX - scenePosX;
        qreal sceneHight = sceneEdgeY - scenePosY;
        scene = new QGraphicsScene(scenePosX,scenePosY,sceneWidth,sceneHight);

        //添加场景
        ui->graphicsView->setScene(scene);
    }

```

### 导出保存

```cpp
void MainWindow::on_actionSave_triggered()
{
    Message_Map msgMap;

    //写入map
    for(auto& item:items_editable){
        qDebug()<<item->pos()<<" "<<item->scenePos();
        msgMap.add_normal_pos_list()->set_x(item.pos().x());
        msgMap.add_normal_pos_list()->set_y(item.pos().y());
        msgMap.add_normal_pos_list()->set_z(0);
    }


    //protobuf转json
    google::protobuf::util::JsonOptions json_options;
    json_options.always_print_primitive_fields = false;
    std::string json_buffer;
    google::protobuf::util::MessageToJsonString(msgMap, &json_buffer, json_options);

    QByteArray fileByteArray = QByteArray::fromStdString(json_buffer);

    QString f = QFileDialog::getSaveFileName(this);
    QFile fe(f);
    fe.open(QIODevice::WriteOnly);
    fe.write(fileByteArray);
    fe.close();
}
```



## 2.导入连线

```cpp
for(auto& curve:msgMap.advanced_curve_list()){
    QString type = QString::fromStdString(curve.class_name());
    //start pos
    qreal x1 = curve.start_pos().pos().x()*mulriple;
    qreal y1 = curve.start_pos().pos().y()*mulriple;
    //end pos
    qreal x2 = curve.end_pos().pos().x()*mulriple;
    qreal y2 = curve.end_pos().pos().y()*mulriple;
    //ctl pos1
    qreal cx1 = curve.control_pos1().x()*mulriple;
    qreal cy1 = curve.control_pos1().y()*mulriple;
    //ctl pos2
    qreal cx2 = curve.control_pos2().x()*mulriple;
    qreal cy2 = curve.control_pos2().y()*mulriple;

    //画曲线,先判断类型是什么，是曲线则需要添加控制点，是直线则直接添加直线
    //绘制贝塞斯曲线路径，初始化输入起始点，然后设置曲线的两个控制点和结束点
    //使用switch判断字符串的方法:
    QStringList typeList;
    typeList<<"BezierPath"<<"StraightPath";
    switch (typeList.indexOf(type)) {
        case 0:{//BezierPath
            QPainterPath CurPath(QPointF(x1,y1));
            CurPath.cubicTo(QPointF(cx1,cy1),QPointF(cx2,cy2),QPointF(x2,y2));
            scene->addPath(CurPath,QPen(QColor(Qt::darkGreen)));
        };break;
        case 1:{//StraightPath
            QLineF line(x1,y1,x2,y2);
            scene->addLine(line,QPen(QColor(Qt::darkBlue)));
        };break;
        default:break;
    }
}
```

![image-20211220121024847](smap地图导入.assets/image-20211220121024847.png)



```cpp
item_Apos->setRotation(AP.dir()*180/3.14); //AP的dir方向单位为rad
```

```cpp
//防止当前的路径rect和其他路径重叠，无法正确获取path，需要使用shape将选择区域变成路径上
QPainterPath ConnectLine::shape() const
{
    QPainterPathStroker pps;
    pps.setWidth(1);
    return pps.createStroke(path());
}

```

```cpp
void MainWindow::wheelEvent(QWheelEvent *event)
{
    if(QGuiApplication::keyboardModifiers()==Qt::KeyboardModifier::ControlModifier){
        //检测CTRL键按下
        if(event->delta()>0) on_actionzoomIn_triggered();
        else if(event->delta()<0) on_actionzoomOut_triggered();
    }
}
```



不能用qgraphicsitem_cast来判断重写的item类型，如果是userType会判断失效，如果继承同一个QGraphicsItem也会因为返回的type类型编号一样而出错。所以如果重写了，则自己重写一个type，然后转换都用static_cast比较稳

判断普通点落点

```cpp
qreal oldx=0, oldy=0;
        for(auto& NP:msgMap.normal_pos_list()){
            qreal x = NP.x()*_mulriple;
            qreal y = NP.y()*_mulriple;
            bool px = qFabs(x-oldx)>(100)?false:true;
            bool py = qFabs(y-oldy)>(100)?false:true;
            if(px && py)continue;
            else {
                QGraphicsEllipseItem *item1 = new QGraphicsEllipseItem(x,y,2,2,_normalPosGroup);
                if(item1->collidesWithItem(item,Qt::ItemSelectionMode::IntersectsItemShape))continue;
                oldx = x, oldy = y;
                item = item1;
            }
        }


    //普通点
    foreach(auto item, _normalPosGroup->childItems()){
        NP = msgMap.add_normal_pos_list();
        //pos()和screenPos()都显示为(0,0),只能使用sceneBoundingRect().topLeft()获取item的位置
        QPointF p(item->sceneBoundingRect().topLeft()/_mulriple);
            NP->set_x(p.x());
            NP->set_y(p.y());
            NP->set_z(0);
        }
```

四叉树优化

```cpp
 //绘制普通点
    {
        //绘制点 优化版
        QGraphicsRectItem *item=nullptr;
        QuadTree tree(_scene->sceneRect(),1,8,8);//最多个数，最小边界
        int in=0,all=0;

        for(auto& NP:msgMap.normal_pos_list()){
            QPointF* p = new QPointF(NP.x()*_mulriple,NP.y()*_mulriple);
            _nomPos<<p;++all;
            if(tree.Insert(*p)){
                ++in;
                item = new QGraphicsRectItem(p->x(),p->y(),1,1,_normalPosGroup);
                item->setBrush(Qt::black);
            }
        }
        qDebug()<<in<<" "<<all;
        _scene->addItem(_normalPosGroup);
    }


//清除
    foreach(auto p,_nomPos){
        delete p;
    }
    _nomPos.clear();
```

获取颜色

```cpp
            uint32_t rgb = area.attribute().color_brush();
            item->setBrush(QColor(rgb&0xFF,(rgb&0xFF00)>>8,(rgb&0xFF0000)>>16,(rgb&0xFF000000)>>24));

            uint32_t pen = area.attribute().color_pen();
            item->setPen(QColor((pen&0xFF),(pen&0xFF00)>>8,(pen&0xFF0000)>>16,(pen&0xFF000000)>>24));

```

曲线的方向部件，bug：开始显示时-90的方向有些显示不出来。线的控制点不在中心

bug细节：会不显示，需要拉大镜头才显示

bug解决：绘制出rect

```cpp
 //绘制方向
//    _direct->setPoint(path.elementAt(path.elementCount()/2));//与path.pointAtPercent(0.5)哪里不同？？
    {
        _dirForward->setPoint(path.pointAtPercent(0.5));
        _dirForward->setRotation(-path.angleAtPercent(0.5)-90);
        if(_dirBackward){
            _dirBackward->setPoint(path.pointAtPercent(0.5));
            _dirBackward->setRotation(-path.angleAtPercent(0.5)+90);
        }
    }
```

文字bug: 在当中的文字上下翻转，旋转时无法固定



保存颜色属性：

```cpp
Message_MapAttribute* attribute = new Message_MapAttribute();
        uint32_t rgb=0;
        rgb |= area->brush().color().alpha() << 24;
        rgb |= area->brush().color().blue() << 16;
        rgb |= area->brush().color().green() << 8;
        rgb |= area->brush().color().red() << 0;
        attribute->set_color_brush(rgb);
        marea->set_allocated_attribute(attribute);
```



直接绘制图片

```cpp
QImage image(QSize(this->width(),this->height()),QImage::Format_ARGB32);
    image.fill("white");
    QPainter *painter = new QPainter(&image);
    painter->save();
    QPen pen;
    pen.setWidth(2);
    pen.setColor(Qt::red);
    painter->setPen(pen);
    painter->drawEllipse(QPoint(width()/2,height()/2),50,50);
    painter->drawLine(QPointF(0,0),QPointF(width()/2,height()/2));
    painter->drawRect(QRect(40,40,150,160));
    painter->restore();
    painter->end();
    m_image = image;
```

点集转图片

头文件：

```cpp
#ifndef NORMALPOINTSIMAGE_H
#define NORMALPOINTSIMAGE_H

#include <QGraphicsPixmapItem>

class NormalPointsImage : public QGraphicsPixmapItem
{
public:
    NormalPointsImage(QGraphicsItem* parent = nullptr);

    QVector<QPointF>& getList(){return _plist;}
    void clearPoints(){_plist.clear();}
    void drawImage(QRectF rect);

private:
    QVector<QPointF> _plist;

};

#endif // NORMALPOINTSIMAGE_H

```

源文件

```cpp
#include "NormalPointsImage.h"

#include <QPainter>

NormalPointsImage::NormalPointsImage(QGraphicsItem* parent)
    :QGraphicsPixmapItem(parent)
{
}

void NormalPointsImage::drawImage(QRectF rect)
{
    qreal sceneWidth = rect.width(); qreal sceneHeight = rect.height();
    QImage image(sceneWidth*2,sceneHeight*2,QImage::Format_RGB16);
    image.fill("white");
    QPainter *painter = new QPainter(&image);
    painter->save();
    QPen pen;
    pen.setBrush(Qt::black);
    painter->setPen(pen);
    QPainterPath path;
    foreach(auto& p,_plist)
        path.addRect(p.x()+sceneWidth,p.y()+sceneHeight,1,1);

    painter->drawPath(path);
    painter->restore();
    painter->end();

    this->setPixmap(QPixmap::fromImage(image));
    this->setPos(-sceneWidth,-sceneHeight); //默认原点为scene原点，需要setPos移动坐标
    this->setZValue(-10);
}

```

或者：

```cpp
void NormalPointsImage::drawImage(QRectF rect)
{
    qreal sceneWidth = rect.width(); qreal sceneHeight = rect.height();
    qreal ratio=1;

    QImage image(sceneWidth*2*ratio,sceneHeight*2*ratio,QImage::Format_RGB16);
    image.fill("white");
    foreach(auto& p,_plist){
        image.setPixel((p.x()+sceneWidth)*ratio,(p.y()+sceneHeight)*ratio,qRgb(0,0,0));
}
    image.setDevicePixelRatio(ratio);

    qDebug()<<""<<image.devicePixelRatio()<<"x:"<<image.dotsPerMeterX()<<" y:"<<image.dotsPerMeterY()<<" width:"<<image.width()<<" height:"<<image.height();
    this->setPixmap(QPixmap::fromImage(image));
    this->setPos(-sceneWidth,-sceneHeight); //默认原点为scene原点，需要setPos移动坐标
    this->setZValue(-10);
}
```



绘制曲线，判断是直线时显示绿色

颜色：数值越小颜色越深，透明度数值越大越不透明

```cpp
//绘制曲线
    {
        QPen pathpen;
        pathpen.setWidth(2);
        if(_className=="BezierPath") {
           qreal pathLenth = path.length();
            qreal straightLength = QLineF(startpos,endpos).length();
            if(qFabs(pathLenth-straightLength)<0.01) //判断曲线与直线等长则为直线，两点之间直线最短,使用范围判断做冗余
                pathpen.setColor(QColor(0,100,0,128));
            else
            pathpen.setColor(QColor(181,107,34,128));
            }
        else if(_className=="StraightPath") pathpen.setColor(QColor(140,203,235,128));
        painter->setPen(pathpen);
        painter->drawPath(path);
    }
```

中键拖拽方法：

```cpp
void SGraphicsView::mousePressEvent(QMouseEvent *event)
{
    if(event->button()==Qt::MiddleButton){
        if(!this->scene())return;
        // 记录当前鼠标在view中的位置，用来在mouseMove事件中计算偏移
        posAnchor = event->pos();
        isMousePressed = true;
    }

    QGraphicsView::mousePressEvent(event);
}

void SGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    if(isMousePressed){
        //计算偏差
        QPointF offsetPos = (event->pos() - posAnchor)/20;
        QScrollBar* scroll{Q_NULLPTR};
        scroll = this->verticalScrollBar();
        scroll->setValue(scroll->value()-offsetPos.y());
        scroll = this->horizontalScrollBar();
        scroll->setValue(scroll->value()-offsetPos.x());
    }

    QGraphicsView::mouseMoveEvent(event);
}
```



崩溃问题：

![image-20211230114102543](smap地图导入.assets/image-20211230114102543.png)

崩溃点：

```
        //draw letter LM
//        _text->setPos(rect().center().x()-_text->boundingRect().center().x(),
//                      rect().center().y()+2/*_text->boundingRect().center().y()*/);
//        _text->setRotation(this->rotation());//字保持正面
```

解决：在线内画方向

```
void CableItem::drawArrowPolygon(QPainter *painter) const
{
    //这里不能用_path，需要获取
    int tArcDirection = _arcDirection;
    QPainterPath path = _path;
    qreal dlength = 7 ; //3.>path.length()?path.length()*0.5:3.;

    qreal vlength = dlength/1;
    double k = path.slopeAtPercent(0.5);
    //两条垂直相交直线的斜率相乘积为-1：k1*k2=-1.
    double k1 = -1/k;
    double agl1 = atan(k1);
    QPointF pos1 = path.pointAtPercent(0.5);
    if(type() == E_RoundLine){//圆弧线
        pos1 = _roundLineArrowPath.pointAtPercent(0.5);
        k = _roundLineArrowPath.slopeAtPercent(0.5);
        //两条垂直相交直线的斜率相乘积为-1：k1*k2=-1.
        k1 = -1/k;
        agl1 = atan(k1);
    }
    bool isBroken = false;
    if(!isBroken){ //线路未检修
        QPolygonF polyf;
        painter->setPen(Qt::NoPen);
        painter->setBrush(QColor(255, 0, 0,180));//QColor(255,0,0,188)
        //三角形上下2个顶点坐标
        QPointF ar1 = pos1+QPointF(vlength*cos(agl1),vlength*sin(agl1));
        QPointF ar2 = pos1-QPointF(vlength*cos(agl1),vlength*sin(agl1));

        polyf<<ar1<<ar2;
```

不能在paint中使用函数update



### 站点绘制名字

文字会随着站点旋转而旋转，所以为了保持文字方向水平一致，需要反方向旋转painter，并在中心点绘制。
思路：首先将painter的坐标原点平移(translate)至站点中心，然后旋转(rotate)站点的旋转角度(相互抵消)，最后在新的原点坐标系中选择位置绘制(draw)文字

```
painter->setPen(Qt::black);
        painter->setBrush(QColor(0,0,0,128));

        painter->translate(this->boundingRect().center());
        painter->scale(1,-1);
        painter->rotate(this->rotation());
        painter->drawText(0,0,_id);
```

