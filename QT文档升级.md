# QT文档升级

![2937362d39bd21b5e97d6c516086ee4](C:\Users\lesliex\AppData\Local\Temp\WeChat Files\2937362d39bd21b5e97d6c516086ee4.png)

首先写配置文件：

```ini
[update]
filename = "D:\\lesliex\\QTpro\\UpdatePro\\testupdatefile\\file";
checkname = "file";

```

代码

选择已更新的文件夹

```c
void MainWindow::on_pushButton_clicked()
{
   updateDir = QFileDialog::getExistingDirectory(this,"select Dir",QDir::currentPath());
    
   ui->plainTextEdit->appendPlainText("ready update...");
}
```

进行升级

```c
void MainWindow::on_pushButton_2_clicked()
{
    //读取配置文件
    QSettings setting("..\\setting.ini",QSettings::IniFormat,this);
    QString readyupdateDir = setting.value("/update/filename").toString();
    QString checkName = setting.value("/update/checkname").toString();
	
    //进行升级
    QFileInfo fileInfo(readyupdateDir);
    if(fileInfo.isDir()){
        if(fileInfo.fileName() == checkName){
            ui->plainTextEdit->appendPlainText("copying...");
            copyToDir(updateDir,readyupdateDir);
            ui->plainTextEdit->appendPlainText("update success!");
        }
        else
        ui->plainTextEdit->appendPlainText("update error!");
    }else
        ui->plainTextEdit->appendPlainText("update error!");

}
```

文件夹覆盖操作

```c
void MainWindow::copyToDir(QString FromDir, QString ToDir)
{
    //获取两个文件夹
    QDir sourceDir(FromDir);
    QDir targetDir(ToDir);

    //若目标文件不存在则创建
    if(!targetDir.exists()) targetDir.mkdir(targetDir.absolutePath());

    //获取源文件夹中文件信息
    QFileInfoList sourceFileInfos = sourceDir.entryInfoList();

    //逐个判别文件采取对应策略,注意QFileInfo类fileName和filePath的区别
    for(auto i : sourceFileInfos){
        //跳过"."".."文件
        if(i.fileName()=="." || i.fileName()=="..") continue;
        //如果是文件夹，则递归
        else if(i.isDir()){
            copyToDir(i.filePath(),targetDir.filePath(i.fileName()));
        }
        //如果是文件，如果文件存在则先删除，再复制
        else if(i.isFile()){
            if(targetDir.exists(i.fileName()))targetDir.remove(i.fileName());
            QFile::copy(i.filePath(),targetDir.filePath(i.fileName()));
        }
    }
}
```

后续可扩展：解压缩包升级，网络远程升级
后续待优化：进入线程执行
