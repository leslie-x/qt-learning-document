# smap地图解析

代码

json文件

```json
{
    //头部
    "header":
     {
          "mapType":"2D-Map",
          "mapName":"2",
          "minPos":{"x":-5.132,"y":-0.158},
          "maxPos":{"x":18.256,"y":10.86},
          "resolution":0.02,
          "version":"1.0.6"
     },
    //普通点集合
    "normalPosList":
    [
    	{"x":-0.006,"y":8.199},
		{"x":-0.006,"y":8.219},
		{"x":-0.007,"y":8.119},
		//...
	],
    //高级点集合
	"advancedPointList":
	[
        {
            "className":"LandMark",
            "instanceName":"LM1",
            "pos":{"x":16.344,"y":6.621},
            "property":	
            [
                {"key":"spin","type":"bool","value":"ZmFsc2U=","boolValue":false}
            ]
        },
        {
            "className":"ChargePoint",
            "instanceName":"CP3",
            "pos":{"x":2.105,"y":6.621},
            "dir":-3.1415926535897931,
            "property":
            [            
                {"key":"prepoint","type":"string","value":"TE0y","stringValue":"LM2"},
                {"key":"spin","type":"bool","value":"ZmFsc2U=","boolValue":false}
            ]
        }
    ],
    //曲线集合
    "advancedCurveList":
    [
        {
            "className":"BezierPath",
            "instanceName":"LM1-LM2",
            "startPos":
            {
                "instanceName":"LM1",
                "pos":{"x":16.344,"y":6.621}
            },
            "endPos":
            {
                "instanceName":"LM2",
                "pos":{"x":3.693,"y":6.621}
            },
            "controlPos1":{"x":12.127,"y":6.621},
            "controlPos2":{"x":7.91,"y":6.621},
            "property":
            [
                {"key":"direction","type":"int","value":"MA==","int32Value":0},
                {"key":"movestyle","type":"int","value":"MA==","int32Value":0}
            ]
        }
    ]
}
    
```



头文件

```c
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsItem>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void handleJsonFile(QString FilePath);

private slots:
    void on_btnFile_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    QVector<QGraphicsItem*> items_editable;
};
#endif // MAINWINDOW_H

```

源文件

```c
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFile>
#include <QFileDialog>
#include <QDebug>

#include <QByteArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_btnFile_clicked()
{
    QString curPath = QDir::currentPath();
    QString Title = "choose a file";
    QString filter = "smap(*.smap)";
    QString FilePath = QFileDialog::getOpenFileName(this,Title,curPath,filter);
    handleJsonFile(FilePath);
}

void MainWindow::on_pushButton_clicked()
{
    if(items_editable.isEmpty())return;
    for(auto i : items_editable){
        i->setFlags(QGraphicsItem::ItemIsFocusable|
                    QGraphicsItem::ItemIsSelectable|
                    QGraphicsItem::ItemIsMovable);
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    if(items_editable.isEmpty())return;
    for(auto i : items_editable){
        i->setFlags(!QGraphicsItem::ItemIsFocusable|
                    !QGraphicsItem::ItemIsSelectable|
                    !QGraphicsItem::ItemIsMovable);
    }
}

void MainWindow::handleJsonFile(QString FilePath)
{
    //打开读取文件内容
    QFile file(FilePath);
    if(file.exists())
        file.open(QIODevice::ReadOnly);
    else return;

    QByteArray data = file.readAll();
    file.close();

    //json文件转换成对象
    QJsonDocument jsonDoc(QJsonDocument::fromJson(data));
    QJsonObject jsonObject = jsonDoc.object();

    //列出json里的所有key
    QStringList keys = jsonObject.keys();
    for(int i=0;i<keys.size();++i)
        qDebug()<<"key "<<i<<" is "<<keys.at(i);

    //放大倍率
    int mulriple = 40;

    //根据key进行相应操作
    if(keys.contains("header"))
    {
        //获取属性信息
        QJsonObject JOB_header = jsonObject["header"].toObject();
        QString mapType = JOB_header["mapType"].toString();
        QString mapName = JOB_header["mapName"].toString();
        QString mapVersion = JOB_header["version"].toString();
        double resolution = JOB_header["resolution"].toDouble();

        //获取场景大小信息
        QJsonObject JOB_minPos = JOB_header["minPos"].toObject();
        QJsonObject JOB_maxPos = JOB_header["maxPos"].toObject();

        qreal scenePosX = JOB_minPos["x"].toDouble()*mulriple;
        qreal scenePosY = JOB_minPos["y"].toDouble()*mulriple;
        qreal sceneEdgeX = JOB_maxPos["x"].toDouble()*mulriple;
        qreal sceneEdgeY = JOB_maxPos["y"].toDouble()*mulriple;

        qreal sceneWidth = sceneEdgeX - scenePosX;
        qreal sceneHight = sceneEdgeY - scenePosY;
        scene = new QGraphicsScene(scenePosX,scenePosY,sceneWidth,sceneHight);

        //添加场景，并显示矩形框
        ui->graphicsView->setScene(scene);
        scene->addRect(scenePosX,scenePosY,scene->width(),scene->height());
    }

    //显示普通点
    //检测碰撞(重叠)部分不绘制
    if(keys.contains("normalPosList"))
    {
        QJsonArray posArray = jsonObject["normalPosList"].toArray();

        //绘制点 优化版
        QGraphicsEllipseItem *item=nullptr;
        qreal oldx=0, oldy=0;
        for(auto i : posArray){
            QJsonObject JOB_pos = i.toObject();
            qreal nx = JOB_pos["x"].toDouble()*mulriple;
            qreal ny = JOB_pos["y"].toDouble()*mulriple;
            bool px = qFabs(nx-oldx)>(100)?false:true;
            bool py = qFabs(ny-oldy)>(100)?false:true;
            if(px && py)continue;
            else {
                QGraphicsEllipseItem *item1 = new QGraphicsEllipseItem(nx,ny,1,1);
                if(item1->collidesWithItem(item,Qt::ItemSelectionMode::IntersectsItemShape))continue;
                oldx = nx, oldy = ny;
                item = item1;
                scene->addItem(item);
            }
        }

//        //绘制点
//        for(auto i : posArray){
//            QJsonObject JOB_pos = i.toObject();
//            qreal x = JOB_pos["x"].toDouble()*mulriple;
//            qreal y = JOB_pos["y"].toDouble()*mulriple;
//            scene->addEllipse(x,y,1,1);
//        }
    }

    //显示工作站
    if(keys.contains("advancedPointList"))
    {
        QJsonArray APosArray = jsonObject["advancedPointList"].toArray();

        int AposCount = APosArray.count();
        ui->statusbar->addWidget(lab5 = new QLabel(QString("LandMark Num: %1").arg(AposCount)));

        //绘制高级点
        for(auto i : APosArray){
            QJsonObject JOB_Apos = i.toObject();
            QString className = JOB_Apos["className"].toString();
            QString instanceName = JOB_Apos["instanceName"].toString();

            QJsonObject JOB_pos = JOB_Apos["pos"].toObject();
            qreal x = JOB_pos["x"].toDouble()*mulriple;
            qreal y = JOB_pos["y"].toDouble()*mulriple;

            QGraphicsItem* item_Apos = nullptr;
            if(className == "LandMark")
            item_Apos = scene->addRect(x-10,y-10,20,20,QPen(QColor(Qt::yellow)),QBrush(QColor(Qt::blue)));

            else if(className == "ChargePoint")
            item_Apos = scene->addRect(x-10,y-10,20,20,QPen(QColor(Qt::yellow)),QBrush(QColor(Qt::green)));

            else item_Apos = scene->addRect(x-10,y-10,20,20,QPen(QColor(Qt::yellow)),QBrush(QColor(Qt::black)));
            //将他加入可编辑图元列表里
            this->items_editable.append(item_Apos);

            //若标明方向，则绘制方向
            if(JOB_Apos.keys().contains("dir")){
                qreal angle = JOB_Apos["dir"].toDouble()*180/3.14;
                QVector<QPointF> Tri = {{x,y-10},{x+10,y},{x,y+10}};
                QGraphicsItem* item_pos_dir = scene->addPolygon(QPolygonF(Tri),QPen(QColor(Qt::red)),QBrush(QColor(Qt::red)));
                //设置旋转中心
                item_pos_dir->setTransformOriginPoint(x,y);
                item_pos_dir->setRotation(angle);
                item_pos_dir->setParentItem(item_Apos);
            }
        }
    }

    //显示曲线
    if(keys.contains("advancedCurveList"))
    {
        QJsonArray CurArray = jsonObject["advancedCurveList"].toArray();
        for(auto i : CurArray){
            QJsonObject JOB_Cur = i.toObject();

            QJsonObject JOB_Cur_startPos = JOB_Cur["startPos"].toObject();
            QJsonObject JOB_Cur_endPos = JOB_Cur["endPos"].toObject();

            QJsonObject startPos = JOB_Cur_startPos["pos"].toObject();
            qreal x1 = startPos["x"].toDouble()*mulriple;
            qreal y1 = startPos["y"].toDouble()*mulriple;

            QJsonObject endPos = JOB_Cur_endPos["pos"].toObject();
            qreal x2 = endPos["x"].toDouble()*mulriple;
            qreal y2 = endPos["y"].toDouble()*mulriple;

            QJsonObject controlPos1 = JOB_Cur["controlPos1"].toObject();
            qreal cx1 = controlPos1["x"].toDouble()*mulriple;
            qreal cy1 = controlPos1["y"].toDouble()*mulriple;

            QJsonObject controlPos2 = JOB_Cur["controlPos2"].toObject();
            qreal cx2 = controlPos2["x"].toDouble()*mulriple;
            qreal cy2 = controlPos2["y"].toDouble()*mulriple;

            //画贝塞斯曲线
            //绘制贝塞斯曲线路径，初始化输入起始点，然后设置曲线的两个控制点和结束点
            QPainterPath CurPath(QPointF(x1,y1));
            CurPath.cubicTo(QPointF(cx1,cy1),QPointF(cx2,cy2),QPointF(x2,y2));
            scene->addPath(CurPath,QPen(QColor(Qt::darkGreen)));

//            scene->addLine(x1,y1,x2,y2,QPen(QColor(Qt::blue)));
        }
    }
}



```

