# QT单例

添加如下：

single.h

```cpp
#include <QSharedPointer>
#include <QMutex>

class SingleTon:
{
    public:
    static VideoPlayerDialog *init();
    
    private:
    explicit VideoPlayerDialog(QWidget *parent = 0);
    static QSharedPointer<VideoPlayerDialog >_instance;
    static QMutex _mutex;
}
```

single.cpp

```cpp
//init static variable
QMutex VideoPlayerDialog::_mutex;
QSharedPointer<VideoPlayerDialog> VideoPlayerDialog::_instance;

VideoPlayerDialog *VideoPlayerDialog::init()
{
    //初始化决定了它所在的线程
    if (_instance.isNull()){
        QMutexLocker mutexLocker(&_mutex);
        if (_instance.isNull())
            _instance = QSharedPointer<VideoPlayerDialog>(new VideoPlayerDialog());
    }
    return _instance.data();
}
```

