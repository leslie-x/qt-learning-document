# Menu菜单

```cpp
void Widget::MenuRequested(QPoint p)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("resize",this,[=](){
        ui->cw->rescaleAxes();
        ui->cw->replot();
    });
    menu->addAction("zoom",this,[=](){
        ui->cw->setSelectionRectMode(QCP::SelectionRectMode::srmZoom);
    });
    menu->addAction("select",this,[=](){
        ui->cw->setSelectionRectMode(QCP::SelectionRectMode::srmSelect);
    });
    menu->addAction("None",this,[=](){
        ui->cw->setSelectionRectMode(QCP::SelectionRectMode::srmNone);
    });
    menu->popup(ui->cw->mapToGlobal(p));//必须要，否则不显示菜单
}
```

button中添加menu

```cpp
//按键显示菜单进行操作
QMenu* menu = new QMenu;
menu->addAction(tr("Rename"),gw,&RobotGroupWidget::on_pushButton_modifyGroup_clicked);
menu->addAction(tr("Delete"),gw,&RobotGroupWidget::on_pushButton_removeOrAnd_clicked);

QPushButton* button_modify = new QPushButton(w);
button_modify->setObjectName("pushButton_setting");
button_modify->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
button_modify->setMenu(menu);
button_modify->setStyleSheet("QPushButton::menu-indicator{image:none;}");//不显示下拉图片
layout->addWidget(button_modify);
```

