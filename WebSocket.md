# WebSocket

## QWebSocketServer：

创建：
```cpp
QWebSocketServer(const QString &serverName, QWebSocketServer::SslMode secureMode, QObject *parent = nullptr)
```
监听：
```cpp
bool QWebSocketServer::listen(const QHostAddress &address = QHostAddress::Any, quint16 port = 0)
```
signals:

```cpp
void closed()
 void newConnection()
```



## QWebSocket

连接服务器：url一般为：`ws://localhost:1234`

```cpp
[slot] void QWebSocket::open(const QUrl &url)
```


发送：
```cpp
qint64 QWebSocket::sendTextMessage(const QString &message)
qint64 QWebSocket::sendBinaryMessage(const QByteArray &data)
```
返回连接：
```cpp
[virtual] QWebSocket *QWebSocketServer::nextPendingConnection()
```

signals:

```cpp
 void binaryMessageReceived(const QByteArray &message)
  void textMessageReceived(const QString &message)
 void stateChanged(QAbstractSocket::SocketState state)
  void connected()
 void disconnected()
```

