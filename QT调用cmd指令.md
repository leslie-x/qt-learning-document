# QT调用Windows的cmd指令

### 知识点
- QProcess进程的使用
	- process.start( 程序，参数指令 )
	- process.waitForStarted()
	- process.waitForFinished()
	- process.close()
- cmd指令的使用
	- cmd /c 指令

### 开启程序

```c
void MainWindow::on_pushButton_2_clicked()
{
    QString file = QFileDialog::getOpenFileName(this,"select exe",QDir::homePath(),"(*.exe)");
    qDebug()<<file;
    QStringList arguments;
    arguments<< "/c" << file;
    QProcess cmd(this);
    cmd.start("cmd.exe",arguments);
    cmd.waitForStarted();
    cmd.waitForFinished();
}
```

### 获取pid

```c
void MainWindow::on_byngetpid_clicked()
{
    //获取pid
    QProcess cmd(this);
    QStringList arguments;
    arguments<< "/c" << "tasklist|findstr" << ui->labexe->text();

    cmd.start("cmd.exe",arguments);//这种方式最好，使用cmd程序，运行命令
    cmd.waitForStarted();//必须加waitForStarted
    cmd.waitForFinished();

    //    QString temp=QString::fromLocal8Bit(cmd.readAllStandardOutput());//fromLocal8Bit()函数可以设置编码。处理中文等字符问题
    QString temp = cmd.readAll();
    qDebug()<<temp;

    QStringList templist = temp.split(QRegExp("\\s+"));
    //正则表达式'\s+'表示查询除去字符间所有空白后的单词，'\s'只除去单个空白
    //    for(auto i:templist)    qDebug()<<i;

    QString PID = templist.at(1);
    ui->labpid->setText(PID);
}

```

### 杀死进程

```c
void MainWindow::on_pushButton_clicked()
{
    QStringList arguments;
    arguments<< "/c" << "taskkill" << "/PID" << ui->labpid->text();
    
    QProcess cmd(this);
    cmd.start("cmd.exe",arguments);
    cmd.waitForStarted();
    cmd.waitForFinished();

}

```

杀死进程两种方式：

- PID：taskkill    /PID    pid
- 名称：taskkill    /IM    a.exe

### 界面：

![85a78bf71bc1ce60683da1feaee8da4](C:\Users\lesliex\AppData\Local\Temp\WeChat Files\85a78bf71bc1ce60683da1feaee8da4.png)

![f3dd9ee0dcd3336966511927e5d60fd](C:\Users\lesliex\AppData\Local\Temp\WeChat Files\f3dd9ee0dcd3336966511927e5d60fd.png)

### 整理后代码：

```c
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QProcess>
#include <QDebug>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QStringList MainWindow::ExeCmd(QStringList arguments,bool returnflag)
{
    QProcess cmd(this);

    cmd.start("cmd.exe",arguments);//这种方式最好，使用cmd程序，运行命令
    cmd.waitForStarted();//必须加waitForStarted
    cmd.waitForFinished();

    if(returnflag == false) {cmd.close();return QStringList();}

    //    QString temp=QString::fromLocal8Bit(cmd.readAllStandardOutput());//fromLocal8Bit()函数可以设置编码。处理中文等字符问题
    QString temp = cmd.readAll();
    QStringList templist = temp.split(QRegExp("\\s+"));//正则表达式'\s+'表示查询除去字符间所有空白后的单词，'\s'只除去单个空白

    cmd.close();
    return templist;
}


void MainWindow::on_byngetpid_clicked()
{
    //获取pid
    QStringList arguments;
    arguments<< "/c" << "tasklist|findstr" << ui->labexe->text();

    QStringList templist = ExeCmd(arguments,true);
    //    for(auto i:templist)    qDebug()<<i; //"geany.exe" "1100" "Console" "1" "34,968" "K" ""
    QString PID = templist.at(1);
    ui->labpid->setText(PID);
}


void MainWindow::on_pushButton_clicked()
{
    QStringList arguments;
    arguments<< "/c" << "taskkill" << "/PID" << ui->labpid->text();
    ExeCmd(arguments);

    arguments.clear();
    arguments<< "/c" << "tasklist|findstr" << ui->labexe->text();
    QStringList templist = ExeCmd(arguments,true);
    //templist=="",内容长度为 1
    if(templist.size()==1)qDebug("kill pid success");
}


void MainWindow::on_pushButton_2_clicked()
{
    QString file = QFileDialog::getOpenFileName(this,"select exe",QDir::homePath(),"(*.exe)");
    //截取执行的程序名称，找到最后一个'/'，得到带.exe的程序名，这里使用section来分段
    QString exename = file.section('/',-1,-1);
    ui->labexe->setText(exename);
    
    QStringList arguments;
    arguments<< "/c" << file;
    ExeCmd(arguments);
}

```

