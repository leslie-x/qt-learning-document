# win10 ubuntu子系统+VcXsrv+xfce4

## 下载

windows商店下载 ubuntu18.04LTS

win10 下载 VcXsrv

ubuntu中下载 xfce4

## 桌面配置

xfce4桌面
Ubuntu中执行以下操作：

```shell
sudo apt-get install xfce4-terminal
sudo apt-get install xfce4
```

打开配置文件`sudo  vim ~/.bashrc`在最下方加入一下命令：

```shell
export DISPLAY=:0.0
export LIBGL_ALWAYS_INDIRECT=1
```

win10打开 XLaunch

注意使用 one large window 然后保存配置在桌面上，下次点击这个保存的配置即可

然后在ubuntu中输入命令：

`sudo startxfce4`

## 语言支持

如果没有语言支持

`sudo apt-get install language-selector-gnome`

安装中文语言包

`sudo apt-get install  language-pack-zh-han*`

中文语言包:

language-pack-zh-hans 简体中文

language-pack-zh-hans-base



## 共享文件夹

命令 `mount` 查看挂载的文件夹

一般D盘在 /mnt/d/ 



## 编译支持

下载geany编辑器

下载编译工具 `sudo apt-get install build-essential`