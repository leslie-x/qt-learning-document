# QListView使用

1. 创建一个QStringList

2. 创建一个QStringListModel

   ```c
   QStringList str;
   QStringListModel strMod;
   strMod.setStringList(str);
   ```

3. 设置QListView

   ```c
   QListView view;
   view.setModel(strMod);
   ```

4. QListView的增删
   增：

   ```c
   //列表末尾添加项
   strMod.insertRow(strMod.rowCount()); //在末尾添加空行
   QModelIndex index = strMod.index(strMod.rowCount()-1,0); // 为新的项生成新的模型索引
   strMod.setData(index,"new item",Qt::DisplayRole);
   ui->view->setCurrentIndex(index);
   
   //当前行前面插入行
   QModelIndex index = ui->view->currentIndex();
   strMod.insertRow(index.row());
   strMod.setData(index,"inserted item",Qt::DisplayRole);
   ui->view->setCurrentIndex(index);
   
   ```

   删：

   ```c
   //删除当前项
   QModelIndex index = ui->view->currentIndex();
   strMod.removeRow(index.row());
   
   //删除列表
   strMod.removeRows(0,strMod.rowCount());

5. QListView的点击事件

```c

void CameraNetworkDlg::on_listViewcamera_clicked(const QModelIndex &index)
{
    //点击获取当前索引index的内容
    QString name = strmod->stringList().at(index.row());
}

```

