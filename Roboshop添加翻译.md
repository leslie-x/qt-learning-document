# 添加翻译

步骤：

1. 首先屏蔽(注释掉)CLibs\RoPclVtk中的 Boost.pri 和 Eigen.pri，防止加载过多的头文件
2. 点击菜单 "工具"->"外部"->"语言家"->"更新翻译" 等待更新 Ch.ts文件
3. 使用工具 Qt -> Linguist 软件(注意版本)，打开 appInfo\setting\Ch\Ch.ts 文件
4. 点击"下一个"找到未check的文字进行编辑，此时会寻找未翻译的带有`tr()`的项 和 ui上的文字
5. 编辑完点击"保存"
6. 保存后再回到Qtcreator，点击 "工具"->"外部"->"语言家"->"发布翻译"
7. 将 Boost.pri 和 Eigen.pri 返回原来样子 保存

