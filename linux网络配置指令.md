# seer网络

nmcli使用方法非常类似linux ip命令、cisco交换机命令，并且支持tab补全，也可在命令最后通过-h、--help、help查看帮助。在nmcli中有2个命令最为常用：

nmcli语法：
nmcli [ OPTIONS ] OBJECT { COMMAND | help }
   OBJECT和COMMAND可以用全称也可以用简称，最少可以只用一个字母，建议用头三个字母。OBJECT里面我们平时用的最多的就是connection和device，还有其他的选项在里暂时不介绍，这里需要简单区分一下connection和device

详细的介绍请看这篇文章：RHEL/CentOS系列发行版nmcli命令概述

这里主要介绍命令的使用

1、查看网络接口信息
--------------------------------------------------------------
nmcli　　　　　　　　　　##查看ip（类似于ifconfig、ip addr）

nmcli device status　　　   ##所有接口的简略信息

nmcli device show 　　　   ##所有接口的详细信息

nmcli device show interface-name 　　　　##特定接口的详细信息
--------------------------------------------------------------

2、查看连接信息
--------------------------------------------------------------
nmcli connection show 　　　　　　　　##所有连接的简略信息

nmcli connection show --active　　　　  ##显示激活的连接

nmcli connection show inteface-name　　 ##某个接口的详细连接信息
--------------------------------------------------------------

3、激活连接与取消激活链接
--------------------------------------------------------------
#激活连接
nmcli connection up connection-name
nmcli device connect interface-name

#取消激活链接
nmcli connection down connection-name　　　　##这个操作当取消一个激活后，如果有其它连接会自动激活其它连接
nmcli device disconnect interface-name 　　　　##这个操作会取消接口上的激活，如果有其它连接也不会自动激活其它连接
--------------------------------------------------------------
建议使用 nmcli device disconnect(connect) interface-name，因为连接断开可将该接口放到“手动”模式，这样做用户让 NetworkManager 启动某个连接前，或发生外部事件（比如载波变化、休眠或睡眠）前，不会启动任何自动连接。


4、创建动态获取ip地址的连接
--------------------------------------------------------------
nmcli connection add type ethernet con-name connection-name ifname interface-name

add表示添加连接,type后面是指定创建连接时候必须指定类型，类型有很多，可以通过nmcli c add type -h看到，这里指定为ethernet。con-name后面是指定创建连接的名字，ifname后面是指定物理设备，网络接口

例子：nmcli connection add type ethernet con-name dhcp-ens33 ifname ens33
--------------------------------------------------------------

5、创建静态ip地址连接
--------------------------------------------------------------
nmcli connection add type ethernet con-name connection-name ifname interface-name ipv4.method manual ipv4.addresses address ipv4.gateway address

ipv4.addresses后面指定网卡ipv4的地址，ipv4.gateway后面指定网卡的ipv4网关

例子：nmcli connection add type ethernet con-name static-enp0s3 ifname enp0s3 ipv4.method manual ipv4.addresses 192.168.1.115/24 ipv4.gateway 192.168.1.1
--------------------------------------------------------------
注意：创建连接后，NetworkManager 自动将 connection.autoconnect 设定为 yes。还会将设置保存到 /etc/sysconfig/network-scripts/connection-name 文件中，且自动将 ONBOOT 参数设定为 yes。

6、常用参数和网卡配置文件参数的对应关系这个只使用RHEL系列的发行版，不适合Debian系列发行版
--------------------------------------------------------------


7、修改连接配置

--------------------------------------------------------------
#添加一个ip地址
nmcli connection modify connection-name ipv4.addresses 192.168.0.58 　　　　##如果已经存在ip会更改现有ip

#给eth0添加一个子网掩码（NETMASK）
nmcli connection modify connection-name ipv4.addresses 192.168.0.58/24

#获取方式设置成手动（BOOTPROTO=static/none）

nmcli connection modify connection-name ipv4.method manual

#获取方式设置成自动（BOOTPROTO=dhcp）

nmcli connection modify connection-name ipv4.method auto

#添加DNS

nmcli connection modify connection-name ipv4.dns 114.114.114.114

#删除DNS

nmcli connection modify connection-name -ipv4.dns 114.114.114.114 （注意这里的减号）

#添加一个网关（GATEWAY）

nmcli connection modify connection-name ipv4.gateway 192.168.0.2

#可一块写入：

nmcli connection modify connection-name ipv4.dns 114.114.114.114 ipv4.gateway 192.168.0.2

#修改连接是否随开机激活
nmcli connection modify connection-name connection.autoconnect no/on

#配置静态路由，重启系统依然生效

nmcli connection modify connection-name +ipv4.routes "192.168.12.0/24 10.10.10.1"

这样会将 192.168.122.0/24 子网的流量指向位于 10.10.10.1 的网关,同时在 /etc/sysconfig/network-scripts/目录下生产一个route-connection-name的文件，这里记录了这个连接的路由信息

--------------------------------------------------------------

8、重载connection
--------------------------------------------------------------
#重载所有ifcfg到connection（不会立即生效，在通过配置文件更改后需要做这个操作让NM知道你做了更改，重新激活连接或重启NM服务后生效）
nmcli connection reload
--------------------------------------------------------------
#重载指定ifcfg到connection（不会立即生效，重新激活连接或重启NM服务后生效）
nmcli connection load /etc/sysconfig/network-scripts/ifcfg-connection-name
nmcli connection load /etc/sysconfig/network-scripts/route-connection-name
--------------------------------------------------------------

9、删除connection
--------------------------------------------------------------
nmcli connection delete connection-name
--------------------------------------------------------------

10、设置主机名
--------------------------------------------------------------
#查询当前主机名
nmcli general hostname

#修改主机名
nmcli general hostname new-hostname

#重启hostname(主机名)服务
systemctl restart systemd-hostnamed
--------------------------------------------------------------
注意：CentOS7 / Redhat7 下的主机名管理是基于系统服务systemd-hostnamed，服务自身提供了hostnamectl命令用于修改主机名，推荐这种方式进行修改；
使用nmcli命令更改主机名时，systemd-hostnamed服务并不知晓 /etc/hostname 文件被修改，因此需要重启服务去读取配置；
