# QT图形视图——进阶

### 需求

在item图元的边框加上以下点

- 中心点：可以整体拖动移动器件
- 边缘点：可以拖动改变图元的大小等
- 特殊点：其他功能

### 解决思路

创建几个图元派生类实现功能

- BPointItem类：绘制中心点或边缘点或特殊点
- BGraphicsItem类：重写获得焦点函数，增加获取中心点、边缘点、类型的函数
- BRectangle类：绘制矩形

### 代码

鼠标拖拽目标点功能：

```c
#include "bpointitem.h"
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QCursor>
#include <QtMath>
#include <QDebug>
#include "bgraphicsitem.h"

BPointItem::BPointItem(QAbstractGraphicsShapeItem* parent, QPointF p, PointType type)
    : QAbstractGraphicsShapeItem(parent)
    , m_point(p)
    , m_type(type)
{
    this->setPos(m_point);
    this->setFlags(QGraphicsItem::ItemIsSelectable |
                   QGraphicsItem::ItemIsMovable |
                   QGraphicsItem::ItemIsFocusable);

    switch (type) {
    case Center:
        this->setCursor(Qt::OpenHandCursor);
        break;
    case Edge:
        this->setCursor(Qt::SizeFDiagCursor);
        break;
    case Special:
        this->setCursor(Qt::PointingHandCursor);
        break;
    default: break;
    }
}

QRectF BPointItem::boundingRect() const
{
    return QRectF(-4, -4, 20, 20);
}

void BPointItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    painter->setPen(this->pen());
    painter->setBrush(this->brush());
    this->setPos(m_point);

    //在轮廓上绘制点
    switch (m_type) {
    case Center:
        painter->drawEllipse(-4, -4, 8, 8);
        break;
    case Edge:
        painter->drawRect(QRectF(-4, -4, 15, 15));
        break;
    case Special:
        painter->drawRect(QRectF(-4, -4, 20, 20));
        break;
    default: break;
    }
}

void BPointItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    //获取鼠标在场景上的坐标
    qreal nx = event->scenePos().x();
    qreal ny = event->scenePos().y();

    if ( event->buttons() == Qt::LeftButton ) {

        //获取父图对象
        BGraphicsItem* item = static_cast<BGraphicsItem *>(this->parentItem());
        BGraphicsItem::ItemType itemType = item->getType();

        //计算当前鼠标位置与上次动作时鼠标位置的距离
        qreal dx = nx - event->lastScenePos().x();
        qreal dy = ny - event->lastScenePos().y();

        /* 获取x,y用于计算角度旋转
         * 需要将图元item的中心点转换为场景坐标，再与鼠标场景坐标计算
         */
        qreal ddx = nx - item->mapToScene(item->getCenter()).x();
        qreal ddy = ny - item->mapToScene(item->getCenter()).y();

        /* 问题：边缘点或特殊点如何随鼠标点走动？ 解决如下：
         * 将 m_point映射成父图的坐标，这样可以绑定点与父图
         * 相当于 this->m_point = this->mapToParent(event->pos());
         */
        if(m_type!=Center)
        this->setPoint(this->mapToParent( event->pos() ));

        switch (m_type) {
        case Center: {//中心点，移动
            item->moveBy(dx, dy); //moveBy(dx,dy) = setPos(pos+QPointF(dx,dy))
        } break;

        case Edge: {//边缘点，大小
            item->setEdge(m_point);
        } break;

        case Special: {//特殊点，旋转
            qreal angle = qAtan2(ddy,ddx)*180/3.14; //qAtan2()输出为reg圆周率数，不是度数
            item->setRotation(angle);
        } break;

        default: break;
        }
        //每次操作后更新触发重画
        this->scene()->update();
    }
}


```

创建中心点，边缘点，特殊点

```cpp
#include "bpointitem.h"
#include "bqgraphicsitem.h"

BEllipse::BEllipse(qreal x, qreal y, qreal width, qreal height, ItemType type)
    : BGraphicsItem(QPointF(x,y), QPointF(x+width/2,y+height/2), type)
{
    BPointItem *point = new BPointItem(this, m_edge, BPointItem::Edge);
    point->setParentItem(this);
    m_pointList.append(point);
    m_pointList.append(new BPointItem(this, m_center, BPointItem::Center));
    m_pointList.setRandColor();
}

```



先绑定shape再绑定boundingrect

```cpp
QRectF BaseControlPosGraphicsitem::boundingRect() const
{
    return shape().boundingRect();//QRectF(-radius_,-radius_,radius_*2,radius_*2);
}

QPainterPath BaseControlPosGraphicsitem::shape() const
{
    QPainterPath path;
    path.addRoundRect(QRectF(-radius_,-radius_,radius_*2,radius_*2),50,50);
    return path;
}
```

