# Qt工程文件配置

## pro文件

```
TEMPLATE = subdirs
CONFIG += ordered

DEFINES += QT_NO_WARNING_OUTPUT \
           QT_NO_DEBUG_OUTPUT

SUBDIRS += \
    RoboCore

SUBDIRS += httpserver \
          RoMainWindow


```



- `TEMPLATE=`
  - app : 表示该工程建立一个应用程序的makefile。这是默认值，所以如果模板没有被指定，这个将被使用
  - lib : 表示该工程建立一个库的makefile
  - subdirs : 用于配置子项目,只负责协调子项目之间的编译，不包含具体的编译事宜

- `DESTDIR = ` : 生成目录——指定生成的可执行程序的目录，可以绝对和相对路径

- `TARGET =` ：生成的应用程序名

- `CONFIG =` ： 编译选项——增添或者移除某个模块，比如增添C++11和console支持，Qt的各模块也在这里面添加

```
CONFIG(debug, debug|release){
    QJSON_NAMESPACE = RoQJsond
}else{
    QJSON_NAMESPACE = RoQJson
}
```



- `OBJECTS_DIR =`：目标文件目录——obj文件存放的目录，可以绝对或相对路径

- `HEADERS += .hpp \ SOURCES += .cpp`

- `RESOURCES += .qrc`

- `INCLUDEPATH +=`

- `LIBS +=` ：库文件

- `DEFINES +=`

## pri文件

```
INCLUDEPATH += $$PWD

include(p/p.pri)

HEADERS += p.hpp
SOURCES += p.cpp
```



## prf文件

 定位输出文件夹

```
#TOP_BUILD_DIR=$$shadowed($$PWD) #编译路径.
TOP_SRC_DIR=$$PWD

BIN_DIR
LIBS_DIR

CONFIG += c++11

RO_NAMESPACE = Ro

DEFINES += RO_NAMESPACE

CONFIG(debug, debug|release){
    win32 {
    contains(QT_ARCH, i386) {
        win32-g++{
            BIN_DIR =  $${TOP_SRC_DIR}/bin/debug/mingw_x86
        }else{
            BIN_DIR =  $${TOP_SRC_DIR}/bin/debug/msvc_x86
        }
    }else {
        win32-g++{
            BIN_DIR =  $${TOP_SRC_DIR}/bin/debug/mingw_x64
        }else{
            BIN_DIR =  $${TOP_SRC_DIR}/bin/debug/msvc_x64
        }
      }
    }
    unix {
        BIN_DIR =  $${TOP_SRC_DIR}/bin/debug/gcc_x64
        contains(DEFINES,YH_IS_AARCH64){
            BIN_DIR =  $${TOP_SRC_DIR}/bin/debug/aarch_x64
        }

    }
}else{
    #release编译环境
    #指定 release 编译输出文件路径
    win32 {
    contains(QT_ARCH, i386) {
        win32-g++{
            BIN_DIR =  $${TOP_SRC_DIR}/bin/release/mingw_x86
        }else{
            BIN_DIR =  $${TOP_SRC_DIR}/bin/release/msvc_x86
        }
    }else {
        win32-g++{
            BIN_DIR =  $${TOP_SRC_DIR}/bin/release/mingw_x64
        }else{
            BIN_DIR =  $${TOP_SRC_DIR}/bin/release/msvc_x64
        }
      }

        #这句话的目的是Release版也将生成“.pdb”后缀的调试信息文件
        QMAKE_LFLAGS_RELEASE += /MAP
        QMAKE_CFLAGS_RELEASE += /Zi
        QMAKE_LFLAGS_RELEASE += /debug  /opt:ref
    }
    unix {
        BIN_DIR =  $${TOP_SRC_DIR}/bin/release/gcc_x64
        contains(DEFINES,YH_IS_AARCH64){
            BIN_DIR =  $${TOP_SRC_DIR}/bin/release/aarch_x64
        }
    }
}

message($${BIN_DIR})

LIBS_DIR=$${BIN_DIR}
DESTDIR = $${BIN_DIR}
LIBS += -L$${LIBS_DIR}
```

