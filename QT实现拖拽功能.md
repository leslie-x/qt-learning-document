# QT实现拖拽功能

### 重写两个虚函数

```c
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
```

例子

```c
void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasUrls()){
        //若是文件路径则允许拖拽
        event->acceptProposedAction();
    }
}

void MainWindow::dropEvent(QDropEvent *event)
{
    QList<QUrl> urls = event->mimeData()->urls();
    if(urls.count()>0){
       QString fileName = urls.first().toLocalFile();
       handleLogFile(fileName);
    }
}
```

### 拖拽属性的开启和关闭

可以设置属性来开启关闭，也可以使用函数setAcceptDrops设置

```c
    //关闭文本框的拖拽属性，开启窗口的拖拽属性
    ui->plainTextEdit->setAttribute(Qt::WA_AcceptDrops,false);
    this->setAcceptDrops(true);
```

