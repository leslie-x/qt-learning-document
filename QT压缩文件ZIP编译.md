# QT压缩文件ZIP编译

### 环境准备

系统变量里加入：

```
C:\Windows\System32
D:\Program Files (x86)\Microsoft Visual Studio 14.0\VC
```

### 准备工具

- zlib 源码下载：http://www.zlib.net/
- quazip 源码下载：https://sourceforge.net/projects/quazip/
- quazip编译博客链接：[ QT使用quazip解压zip压缩文件](https://blog.csdn.net/mml5211314/article/details/90754947?spm=1001.2014.3001.5506)
- zlib编译博客链接：[win10下Visual Studio 2015，C++ x64编译zlib](https://www.cnblogs.com/MrOuqs/p/5751485.html)
- zlib的32位和64位编译说明：https://blog.csdn.net/yxy244/article/details/105024989

### 编译zlib

1. 打开VS2015 x64本地命令行工具（在开始栏VS2015文件夹中）
2. cd 到解压的zlib文件目录
3. 输入

```
nmake -f win32/Makefile.msc AS=ml64 LOC="-DASMV -DASMINF -I." OBJA="inffasx64.obj gvmat64.obj inffas8664.obj"
```

4. 若出错，则试着将VC\bin\amd64\ml64.exe文件拷贝到zlib文件目录下再进行编译
5. 编译后生成文件 zdll.lib，zlib1.dll等等

### 编译quaZip

1. 在quazip文件建立include和lib两个文件夹
2. 在include文件夹放入zlib1211/contrib/minizip 里的所有.h文件以及zlib目录的zconf.h和zlib.h文件，lib文件里放入zlib编译生成的zdll.lib文件。
3. 用QT打开quazip.pro，进入，点击quazip.pro，去掉quaziptest相关
4. 在内部quazip项目中添加依赖的zlib库，项目右击，选择添加库，添加外部库，点击下一步。选择lib文件夹下的zlib.dll文件，点击下一步，完成即可，在quazip.pro下会自动添加代码。
5. 对项目右键先执行qmake，再进行构建，构建完成，就可以在Debug\release模式下生成我们需要的dll和lib文件，分别在对应debug/release文件夹中
6. 编译完成

### 测试

1. 新建QT项目，在项目文件下，新建include文件夹和lib文件夹，将quazip下的quazip文件里的所有.h文件以及zconf.h和zlib.h文件复制到include文件中。
2. 将quazip编译生成的两个版本的 lib和dll 以及 zlib编译生成的zdll.lib复制到lib文件夹下
3. 按上面方法添加这两个lib依赖库

```
win32: LIBS += -L$$PWD/quazipPack/lib/ -lzdll

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/quazipPack/lib/ -lquazip
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/quazipPack/lib/ -lquazipd

INCLUDEPATH += $$PWD/quazipPack/include
DEPENDPATH += $$PWD/quazipPack/include

```



4. 在项目的头文件加入:#include <JlCompress.h>   进行验证，运行成功，则可以进行文件的压缩和解压。

### 库使用方法

QuaZIP类描述：

| 类                |         描述          |
| :---------------- | :-------------------: |
| JlCompress        |    典型操作工具类     |
| QuaAdler32        |   Adler32算法校验和   |
| QuaChecksum32     |      校验和接口       |
| QuaCrc32          |      CRC32校验和      |
| QuaGzipFile       |     GZIP文件操作      |
| QuaZIODevice      |  压缩/解压QIODevice   |
| QuaZip            |        ZIP文件        |
| QuaZipDir         |   ZIP文件内目录导航   |
| QuaZipFile        |    ZIP文件内的文件    |
| QuaZipFileInfo    | ZIP压缩包内的文件信息 |
| QuaZipFilePrivate |     QuaZip的接口      |
| QuaZipNewInfo     |   被创建的文件信息    |
| QuaZipPrivate     |     QuaZIP内部类      |

主要用QuaZip中的JlCompress类来压缩和解压缩文件

1. 静态方法压缩文件

```c

static bool compressDir(QString fileCompressed, QString dir=QString(), bool recursive = true)
    
```

参数：

- 第一个参数fileCompressed表示压缩后的文件
- 第二个参数dir表示待压缩的目录
- 第三个参数recursive表示是否递归

2. 解压缩用静态方法：extractDir

```c

static QStringList extractDir(QString fileCompressed, QString dir=QString())
    
```

参数：

- 第一个参数fileCompressed表示待解压缩的文件
- 第二个参数表示解压缩存放的目录

例子

```c
if(false == JlCompress::compressDir("D:\\lesliex\\QTpro\\QuaZIP_Learn\\quazipTest\\1.zip",
                                        "D:\\lesliex\\QTpro\\QuaZIP_Learn\\quazipTest\\1"))
    {
        qDebug("false");
    }
    else{
        qDebug("yes");
    }
```

