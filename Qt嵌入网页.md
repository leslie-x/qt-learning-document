# Qt嵌入网页

## 1.QWebEngineView使用

pro文件：`Qt += webenginewidgets`

头文件：`#include <QWebEngineView>`、`#include <QWebEngineSettings>`

使用：

```cpp
void Widget::initWebview()
{
    //创建QWebEngineView,会有一个独立窗口
    QWebEngineView* web = new QWebEngineView();
    //打开网页插件支持
    web->settings()->setAttribute(QWebEngineSettings::PluginsEnabled, true);
	//加载页面，可用setHtml,setUrl,load等
    web->load(QUrl("http://localhost:7310/"));
	//显示页面
    web->show();
	//绑定加载页面结束后的操作
    connect(web,&QWebEngineView::loadFinished,this,[this](bool ok){
      //操作，可以调用js函数
    });

}
```



## 2.调用 JavaScript

函数：

- 执行js：`void QWebEnginePage::runJavaScript(const QString &scriptSource)`
- 带返回值的执行js：`void QWebEnginePage::runJavaScript(const QString &scriptSource, const QWebEngineCallback<const QVariant &> &resultCallback)`

```cpp
web->page()->runJavaScript("showVersion(true)");
//第二个为匿名函数，参数为js返回值
web->page()->runJavaScript("getVersion()",[](const QVariant &v){qDebug()<<v.toString();});
```



## 3.本地跨域访问——搭建服务器

首先需要有 qhttpserver 库 例子：https://gitee.com/leslie-x/qt-web-engine.git

头文件：`#include "httpserver/qhttpserver.h"`

```cpp
void Widget::initHttpServer()
{
    server = new QHttpServer();
    QString urlBase;
    QString sslUrlBase;

    //get返回本地页面
    server->route("/", QHttpServerRequest::Method::Get, [] () {
          return  QHttpServerResponse::fromFile("index/index.html");
      });
    
    //添加js使用的本地资源路径
      server->route("/", [] (const QString &file,const QHttpServerRequest &req) {
          qDebug()<<"url:"<<req.url();
          return QHttpServerResponse::fromFile("index/" + file);
      });
      server->route("/assets/", [] (const QString &file,const QHttpServerRequest &req) {
          qDebug()<<"url:"<<req.url();
          return QHttpServerResponse::fromFile("index/assets/" + file);
      });

    //打开监听
      quint16 port = server->listen(QHostAddress::Any,7310);
      if (!port){
          qWarning() << QString("error:Http server listen [%1] failed").arg(port);
      }
    
      urlBase = QStringLiteral("http://localhost:%1/").arg(port);
      qDebug()<<"urlBase:"<<urlBase;
}
```

输出：

```
urlBase: "http://localhost:7310/"
url: QUrl("http://localhost:7310/assets/index.370ea674.css")
url: QUrl("http://localhost:7310/assets/index.be6d5d0a.js")
url: QUrl("http://localhost:7310/hdri.hdr")
```

