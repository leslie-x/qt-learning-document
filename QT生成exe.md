# QT release版本

### 生成exe文件

在QT中生成时选择release版本而非debug版本

将要打包的exe文件单独放到一个文件夹exefile

### 编译

打开MinGW，输入指令

```
> cd /d exefile路径

> windeployqt 名称.exe
```

但是这样依旧无法打开，则需要添加QT库路径到系统变量

在系统变量添加msvc版本的编译库路径：

```
D:\qt\5.12\msvc2017_64\bin
```



### 使用Enigma Virtual Box打包



