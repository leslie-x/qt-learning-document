# QT曲线路径绘制

头文件

```cpp
#ifndef CONNECTLINE_H
#define CONNECTLINE_H

#include <QGraphicsPathItem>
#include "AdvancedPoint.h"
class ContrlPoint;

//===================================================//画路径方向箭头
class LineDirectItem : public QGraphicsPolygonItem
{
public:
    LineDirectItem(){}
    LineDirectItem(QGraphicsItem* parent=nullptr)
        :QGraphicsPolygonItem(parent){}
    void setPoint(QPointF p){_p = p;this->setTransformOriginPoint(_p);}

private:
    QPointF _p;

    // QGraphicsItem interface
public:
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    virtual QRectF boundingRect() const;
};

//===================================================//曲线路径
class ConnectLine : public QGraphicsPathItem
{
public:
    ConnectLine();
    ConnectLine(AdvancedPoint* startitem, AdvancedPoint* enditem);
    ~ConnectLine();

    void setCtlPoints(QList<QPointF> ctlPosList);
    QList<QPointF> getCtlPoints();

    void setClassName(QString classname){_className = classname;}
    void setId(QString id){_id = id;}

    void setReverse(bool flag){
        if(flag) _dirBackward = new LineDirectItem(this);
        else if(_dirBackward) {delete _dirBackward;_dirBackward=nullptr;}
    }

private:
    AdvancedPoint* _startItem;
    AdvancedPoint* _endItem;
    QList<ContrlPoint*> _ctlPoints;

    LineDirectItem* _dirForward{Q_NULLPTR};
    LineDirectItem* _dirBackward{Q_NULLPTR};

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    virtual QPainterPath shape() const;
};

#endif // CONNECTLINE_H

```

源文件

```cpp

#include "ConnectLine.h"
#include <QPainter>
#include "ContrlPoint.h"
#include <QDebug>

static void createNBezierCurve(const QVector<QPointF> &src, QVector<QPointF> &dest, qreal precision)
{}


ConnectLine::ConnectLine()
{}

ConnectLine::ConnectLine(AdvancedPoint *startitem, AdvancedPoint *enditem)
{
    _startItem = startitem;
    _endItem = enditem;
    this->setFlags(QGraphicsLineItem::ItemIsSelectable | QGraphicsLineItem::ItemIsFocusable);

    _dirForward = new LineDirectItem(this);

}

ConnectLine::~ConnectLine()
{
}

void ConnectLine::setCtlPoints(QList<QPointF> ctlPosList)
{
    foreach(auto pos,ctlPosList){
        _ctlPoints.append(new ContrlPoint(this,pos,ContrlPoint::NORMAL));
    }
}

QRectF ConnectLine::boundingRect() const
{
    QRectF r = this->path().controlPointRect();
    return r;
}

void ConnectLine::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPointF startpos = _startItem->sceneBoundingRect().center();
    QPointF endpos = _endItem->sceneBoundingRect().center();

    //起始点控制点
    QVector<QPointF> src;
    {
        foreach(auto i,_ctlPoints)
            src.append(i->pos());
        src.insert(0,startpos);
        src.append(endpos);
    }
    //计算曲线点
    QVector<QPointF> dest;
    {
        createNBezierCurve(src,dest,0.01);
        dest.insert(0,startpos);
        dest.append(endpos);
    }
    //曲线路径
    QPainterPath path;
    {
        path.moveTo(startpos);
        foreach(auto i,dest)path.lineTo(i);

        //在QGraphicsPathItem中实体为setPath, 在QGraphicsLineItem中实体为setLine,这样点击到路径就能被选中
        this->setPath(path);
    }

    //绘制曲线
    {
        QPen pathpen;
        pathpen.setWidth(2);
        if(_className=="BezierPath") {
            if(path.length()==QLineF(startpos,endpos).length()) //判断曲线与直线等长则为直线，两点之间直线最短
                pathpen.setColor(QColor(0,100,0,128));else

            pathpen.setColor(QColor(181,107,34,128));
            }
        else if(_className=="StraightPath") pathpen.setColor(QColor(140,203,235,128));
        painter->setPen(pathpen);
        painter->drawPath(path);
    }

    //绘制方向
//    _direct->setPoint(path.elementAt(path.elementCount()/2));//与path.pointAtPercent(0.5)哪里不同？？
    {
        _dirForward->setPoint(path.pointAtPercent(0.5));
        _dirForward->setRotation(-path.angleAtPercent(0.5)+90);
        if(_dirBackward){
            _dirBackward->setPoint(path.pointAtPercent(0.5));
            _dirBackward->setRotation(-path.angleAtPercent(0.5)-90);//会不显示，需要拉大镜头才显示
        }
    }

    //绘制辅助线
    if(this->isSelected()){
        //连接辅助点
        QPainterPath pathAssist;
        {
            pathAssist.moveTo(startpos);
            foreach(auto i,src)pathAssist.lineTo(i);
        }

        QPen penAssist(QColor(131,131,252),1,Qt::DashLine);
        painter->setPen(penAssist);
        painter->drawPath(pathAssist);

        foreach(auto pos,_ctlPoints)pos->show();
    }else{
        foreach(auto pos,_ctlPoints){if(pos->isSelected())return;}
        foreach(auto& p, _ctlPoints)p->hide();
    }

    this->update(this->boundingRect());//可以只刷新当前图元
}

//防止当前的路径rect和其他路径重叠，无法正确获取path，需要使用shape将选择区域变成路径上
QPainterPath ConnectLine::shape() const
{
    QPainterPathStroker pps;
    pps.setWidth(3);
    return pps.createStroke(path());
}


//=============================================//方向箭头
void LineDirectItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    //从底边中点画一个正三角形,p4为顶点
    QPointF p1 = _p + QPointF(4,0);
    QPointF p2 = _p + QPointF(-4,0);
    QPointF p3 = _p + QPointF(0,4*sqrt(3));
    QPointF p4 = _p + QPointF(0,-4*sqrt(3));

    QPolygonF poly;
    poly<<p1<<p4<<p2;
    setPolygon(poly);
    painter->setBrush(QColor(255,100,0,128));
    painter->setPen(Qt::NoPen);
    painter->drawPolygon(poly);

    this->update(this->boundingRect());
}

QRectF LineDirectItem::boundingRect() const{
    return QRectF(_p+QPointF(-2,-4*sqrt(3)),QSize(4,4*sqrt(3)));
}

```

