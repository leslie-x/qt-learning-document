# VLD内存泄漏检测工具

一、安装下载链接
			[Visual Leak Detector | Enhanced Memory Leak Detection for Visual C++ (kinddragon.github.io)](https://kinddragon.github.io/vld/)

二、使用：

- 在安装目录下找到vld.lib文件。有32位和64位的注意位数。复制到qt安装目录的lib文件夹中，也要复制到你的项目的debug文件夹中
- 复制vld安装目录下include文件夹中的文件vld.h和vld_def.h到qt安装目录的include文件夹中
- 建一个工程。在main.cpp中添加头文件。#include<vld.h>。在所需要检测的文件里加上这个头文件

# QT中单例的创建与销毁

使用qt自带的全局静态变量宏 `Q_GLOBAL_STATIC(TypeName,instanceName)`

头文件

```cpp
class SingleTon : public QObject
{
public:
    static SingleTon* getInstance(void);
};
```

源文件

```cpp
#include <QGlobalStatic>

Q_GLOBAL_STATIC(SingleTon, _self) 
SingleTon *SingleTon::instance() 
{ 
    return _self; 
}
```

