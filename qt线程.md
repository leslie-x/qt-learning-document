# QT 线程

### QtConCurrent 和 QThread

```cpp
//1.调用外部函数func1,并得到返回值
    QFuture<QString> f1 =QtConcurrent::run(func1,QString("aa"));
    f1.waitForFinished();
    qDebug()<<f1.result();
    //2.调用Lambda函数，实际同外部函数方式
    QThreadPool pool;
    QFuture < void > future =  QtConcurrent::run([=](){
        qDebug() << __FUNCTION__  << QThread::currentThreadId() << QThread::currentThread();
    });
    QFuture < void > future2 = QtConcurrent::run([=](){
        qDebug() << __FUNCTION__  << QThread::currentThreadId() << QThread::currentThread();
    });
    //3.调用成员函数
    QFuture<void> future3 = QtConcurrent::run(this, &Dialog::memberFun1);

    //4.常量成员函数QByteArray::split()
    QByteArray bytearray = "hello,world";
    QFuture<QList<QByteArray>> future4 = QtConcurrent::run(bytearray, &QByteArray::split, ',');
    QList<QByteArray> result = future4.result();
    qDebug()<<"result:"<<result;

```

```
func1 "aa" 0xacc QThreadPoolThread(0x3d421d0, name = "Thread (pooled)")
"func1 return"
operator() 0xacc QThreadPoolThread(0x3d421d0, name = "Thread (pooled)")
operator() 0x70ac QThreadPoolThread(0x3d42090, name = "Thread (pooled)")
memberFun1 0x8a88 QThreadPoolThread(0x3d421b0, name = "Thread (pooled)")
result: ("hello", "world")

```



生命周期|开发场景|解决方案
-|-|:-:
单次调用|在其他的线程中运行一个方法，当方法运行结束后退出线程。|（1）编写一个函数，然后利用 QtConcurrent::run()运行它；（2）从QRunnable 派生一个类，并利用全局线程池QThreadPool::globalInstance()->start()来运行它。（3） 从QThread派生一个类, 重载QThread::run() 方法并使用QThread::start()来运行它
单次调用|一个耗时的操作必须放到另一个线程中运行。在这期间，状态信息必须发送到GUI线程中。|使用 QThread,，重载run方法并根据情况发送信号。.使用queued信号/槽连接来连接信号与GUI线程的槽。
常驻|有一对象位于另一个线程中，将让其根据不同的请求执行不同的操作。这意味与工作者线程之间的通信是必须的。|从QObject 派生一个类并实现必要的槽和信号，将对象移到一个具有事件循环的线程中，并通过queued信号/槽连接与对象进行通信。



 总结和QThread的区别
与外界通信方式不同。由于QThread是继承于QObject的，但QRunnable不是，所以在QThread线程中，可以直接将线程中执行的结果通过信号的方式发到主程序，而QRunnable线程不能用信号槽；
启动线程方式不同。QThread线程可以直接调用start()函数启动，而QRunnable线程需要借助QThreadPool进行启动；
资源管理不同。QThread线程对象需要手动去管理删除和释放，而QRunnable则会在QThreadPool调用完成后自动释放。

moveToThread线程的关闭：

```cpp
void MiniNetworkThread::thExit()
{//先退出，然后等待退出完毕，再释放
    _th.quit();
    _th.wait();
    _th.deleteLater();
}
```

自动销毁

```cpp
connect(&thread_do,&QThread::finished,&worker_do,&class_do::deleteLater);
```



### Q_INVOKABLE 和 QMetaObject::invokeMethod

Invoke函数可以相当于考虑线程安全的 信号与槽 ，代替emit，对处于不同线程的信号和槽进行资源分配

可以使用在进程间的通信

1. 为什么需要用invokeMethod而不是emit呢？

一般来说，我们使用invokeMethod是在子线程需要调度UI操作的时候。主要有一下原因：

  a. 使用 QMetaObject :: invokeMethod 的原因是收件人对象可能在另一个线程中，如果试图直接在另一个线程中的对象上调用一个槽访问或修改非线程安全数据，则会导致损坏或更糟。

b. 我们可以自己定义连接方式。但是需要注意具体问题具体分析。


QGenericArgument和QGenericReturnArgument是内部帮助程序类。由于可以动态调用信号和插槽，因此必须使用q_arg（）和q_return_arg（）宏将参数括起来。q_arg（）接受类型名和该类型的常量引用；q_return_arg（）接受类型名和非常量引用。


   对于Qt来说，UI线程是主线程，对于同一UI线程中对象的通信可以通过connect进行信号与槽关联来实现，但是当UI中对象A中的子线程B需要和另外UI对象C进行通信的时候，如果这个时候使用connect来进行通信的话,需要B对象和A对象进行关联将信号发送到主线程中，然后A对象和C对象再建立联系，这样处理起来会比较繁琐。Qt提供了invokeMethod反射机制，就可以实现任何线程中的数据之间传输，使用invokeMethod的前提条件是1）对象继承QObject; 2)定义的类中使用Q_OBJECT(可以使用信号与槽)，



QtConcurrent + QThreadPool

把多个线程放进QThreadPool，`QtConcurrent::run(&pool,func)`，然后使用新的另一个pool的线程来监听前一个线程池的结束 `QThreadPool.waitforDone`
