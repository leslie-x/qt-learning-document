# MVC框架

<img src="MVC框架.assets/image-20211125101958315.png" alt="image-20211125101958315" style="zoom:67%;" />

```cpp

#include <QAbstractItemView>
#include <QAbstractItemModel>
#include <QDirModel>
#include <QTreeView>
#include <QListView>
#include <QTableView>
#include <QSplitter>

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //1.创建模型
    QDirModel model;
    //2.创建视图
    QTreeView tree;
    QListView list;
    QTableView table;
    //3.设置视图模型
    tree.setModel(&model);
    list.setModel(&model);
    table.setModel(&model);
    //4.设置选择模式
    tree.setSelectionMode(QAbstractItemView::SingleSelection);
    list.setSelectionMode(tree.selectionMode());
    table.setSelectionMode(tree.selectionMode());
    //5.连接信号,当双击tree中的文件夹时，列表显示文件夹下的文件
    QObject::connect(&tree,&QTreeView::clicked,&list,&QListView::setRootIndex);
    QObject::connect(&tree,&QTreeView::clicked,&table,&QTableView::setRootIndex);

    //6.编辑界面
    QSplitter* splitter = new QSplitter;
    splitter->addWidget(&tree);
    splitter->addWidget(&list);
    splitter->addWidget(&table);
    splitter->show();

    return a.exec();
}

```



二、

<img src="MVC框架.assets/image-20211125105708047.png" alt="image-20211125105708047" style="zoom: 67%;" />

数据

```
Tom,1977-01-05,worker,1500
Jack,1978-12-23,doctor,3000
Alice,1980-04-06,army,2500
John,1983-09-25,loaryer,5000

```



```cpp
#include <QApplication>
#include <QStandardItemModel>
#include <QTableView>
#include <QFile>
#include <QTextStream>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //设置模型
    QStandardItemModel model(4,4);
    QTableView tableView;
    tableView.setModel(&model);
    //插入表头
    model.setHeaderData(0,Qt::Horizontal,QObject::tr(u8"姓名"));
    model.setHeaderData(1,Qt::Horizontal,QObject::tr(u8"生日"));
    model.setHeaderData(2,Qt::Horizontal,QObject::tr(u8"职业"));
    model.setHeaderData(3,Qt::Horizontal,QObject::tr(u8"收入"));
    //从文件插入数据内容
    QFile file("test.txt");
    if(file.open(QFile::ReadOnly|QFile::Text)){
        QTextStream stream(&file);
        QString line;
        model.removeRows(0,model.rowCount(QModelIndex()));
        int row=0;
        do{
            line = stream.readLine();
            if(!line.isEmpty()){
                model.insertRow(row);
                QStringList pieces = line.split(",",QString::SkipEmptyParts);
                model.setData(model.index(row,0),pieces.value(0));
                model.setData(model.index(row,1),pieces.value(1));
                model.setData(model.index(row,2),pieces.value(2));
                model.setData(model.index(row,3),pieces.value(3));
                row++;
            }
        }while(!line.isEmpty());
        file.close();
    }
    tableView.show();

    return a.exec();
}

```

3.delegate

```cpp
#ifndef DATEDELEGATE_H
#define DATEDELEGATE_H

#include <QItemDelegate>

class DateDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    DateDelegate(QObject*parent=0);

    // QAbstractItemDelegate interface
public:
    /** 完成创建控件的工作
     * @brief createEditor
     * @param parent
     * @param option
     * @param index
     * @return
     */
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    /** 设置控件显示的数据
     * @brief setEditorData
     * @param editor
     * @param index
     */
    void setEditorData(QWidget *editor, const QModelIndex &index) const;

    /** 将Delegate中对数据的改变更新至model中
     * @brief setModelData
     * @param editor
     * @param model
     * @param index
     */
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

    /** 更新控件区的显示
     * @brief updateEditorGeometry
     * @param editor
     * @param option
     * @param index
     */
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // DATEDELEGATE_H

```

```cpp
#include "DateDelegate.h"
#include <QDateTimeEdit>

DateDelegate::DateDelegate(QObject*parent)
{

}

QWidget *DateDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    //创建一个控件
    QDateTimeEdit *editor = new QDateTimeEdit(parent);
    editor->setDisplayFormat("yyyy-MM-dd");
    editor->setCalendarPopup(true);
    editor->installEventFilter(const_cast<DateDelegate*>(this));

    return editor;
}

void DateDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    //设置控件的显示数据
    QString dateStr = index.model()->data(index).toString();//获取指定index数据项的数据
    QDate date = QDate::fromString(dateStr,Qt::ISODate);
    QDateTimeEdit *edit = static_cast<QDateTimeEdit*>(editor);
    edit->setDate(date);
}

void DateDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    //将数据修改更新到Model中
    QDateTimeEdit *edit = static_cast<QDateTimeEdit*>(editor);
    QDate date = edit->date();
    model->setData(index,QVariant(date.toString(Qt::ISODate)));
}

void DateDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry(option.rect);
}

```

改变main

```cpp
    tableView.setModel(&model);
    //设置代理
    DateDelegate dateDelegate;
    tableView.setItemDelegateForColumn(1,&dateDelegate);//双击时可以进入编辑
```

combobox控件：

```cpp
#include "ComboDelegate.h"
#include <QComboBox>

ComboDelegate::ComboDelegate(QObject *parent) : QItemDelegate(parent)
{

}

QWidget *ComboDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    //创建显示控件
    QComboBox *editor = new QComboBox(parent);
    editor->addItem("worker");
    editor->addItem("doctor");
    editor->addItem("lawyer");
    editor->addItem("soldier");
//    editor->installEventFilter(const_cast<ComboDelegate*>(this));

    return editor;
}

void ComboDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    //根据当前的字符串设置控件的显示
    QString str = index.model()->data(index).toString();
    QComboBox *edit = static_cast<QComboBox*>(editor);
    int idx = edit->findText(str);
    edit->setCurrentIndex(idx);
}

void ComboDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    //根据控件的反馈设置模型的数据
    QComboBox *edit = static_cast<QComboBox*>(editor);
    QString str = edit->currentText();
    model->setData(index,str);
}

void ComboDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry(option.rect);
}

```



树形视图搜索框

```cpp
//搜索框
void MainWindowWidget::on_lineEdit_search_textEdited(const QString &arg1)
{
    ui->treeView->collapseAll();
    if(arg1.isEmpty())return;

    auto model = _modelThread->standardItemModel();
    //模糊查找
    auto list = model->findItems(arg1,Qt::MatchContains|Qt::MatchRecursive);
    foreach(auto item ,list){
        //滚轮到查找item位置并将折叠的节点展开确保看到寻找的item
        ui->treeView->scrollTo(model->indexFromItem(item),QAbstractItemView::PositionAtTop);
    }

}
```

