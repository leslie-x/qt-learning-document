# 新项目 报警界面

## 主页面

<img src="报警界面项目.assets/image-20211011150517701.png" alt="image-20211011150517701" style="zoom: 80%;" />

1.添加两个类：

- RobotCellWidget：继承自QFrame，机器人单位部件，显示单个机器人状态
- RobotsListWidget：继承自QWidget，机器人集合，将各个单位部件排列显示

2.设置样式

```cpp
//设置样式
this->setStyleSheet("\
                    QFrame {border-width:10px; border-color:red; border-style:solid;} \
                    QLabel {border:1px solid black;}\
                    ");
//取消样式
this->setStyleSheet("");

//高级
    this->setStyleSheet("\
                        QFrame#RobotCellWidget:hover {border: 10px solid red;}\
                        ");//此处'#'代表特指，限制样式作用范围，hover为鼠标悬在上方的动作
```



```cpp
class RobotCellWidget : public QFrame
{
    Q_OBJECT

public:
    explicit RobotCellWidget(QWidget *parent = nullptr);
    ~RobotCellWidget();

private:
    Ui::RobotCellWidget *ui;
    RoCorrectionDlg* _rcd{Q_NULLPTR};
    bool _selected{false};

    // QWidget interface
protected:
    virtual void mousePressEvent(QMouseEvent *event) override;
};



RobotCellWidget::RobotCellWidget(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::RobotCellWidget)
{
    ui->setupUi(this);
    this->setFixedSize(200,150);
    this->setFrameShape(QFrame::Box);
    this->setFrameShadow(QFrame::Plain);
    this->setLineWidth(1);
    this->setStyleSheet("\
                        QFrame {border-width:10px; border-color:red; border-style:solid;} \
                        QLabel {border:1px solid black;} \
                        ");
}

RobotCellWidget::~RobotCellWidget()
{
    delete ui;
}

void RobotCellWidget::mousePressEvent(QMouseEvent *event)
{
    if(_rcd==nullptr) {
        this->setStyleSheet("");
        _rcd = new RoCorrectionDlg(this);
        _rcd->setWindowFlags(Qt::Window);

    }
    _rcd->show();
}



```

```cpp
class RobotsListWidget : public QWidget
{
    Q_OBJECT
public:
    explicit RobotsListWidget(QWidget *parent = nullptr);

    void addCell(int row, int col);
    void addCell();
private:
    QGridLayout* _grid{Q_NULLPTR};
};


RobotsListWidget::RobotsListWidget(QWidget *parent) : QWidget(parent)
{
    _grid = new QGridLayout(this);
    addCell(0,0);
    addCell(0,1);
    addCell(0,2);
    addCell();
    addCell();
    addCell();
    addCell();
    addCell();
}

void RobotsListWidget::addCell(int row, int col)
{
    RobotCellWidget* cellWidget = new RobotCellWidget(this);
    _grid->addWidget(cellWidget,row,col);
}

void RobotsListWidget::addCell()
{
    RobotCellWidget* cellWidget = new RobotCellWidget(this);
    _grid->addWidget(cellWidget);
}

```



## 报警处理界面

需要包含fatal，error，warning的信息，并实时显示。每个按钮对应报警码进行不同的操作。

<img src="C:\Users\lesliex\AppData\Roaming\Typora\typora-user-images\image-20211009170847355.png" alt="image-20211009170847355" style="zoom:80%;" />

```cpp
void RoCorrectionDlg::createItemsARow(int rowNo, QString code, QString info)
{
    QTableWidgetItem *item;

    item = new QTableWidgetItem(code,0);
    ui->tableWidget->setItem(rowNo,0,item);

    item = new QTableWidgetItem(info,0);
    ui->tableWidget->setItem(rowNo,1,item);

    //创建按钮
    QPushButton *pBtn = new QPushButton(tr("deal"),this);
    connect(pBtn,&QPushButton::clicked,this,&RoCorrectionDlg::slotTableBtnClicked);
    ui->tableWidget->setCellWidget(rowNo,2,pBtn);
}

void RoCorrectionDlg::slotTableBtnClicked()
{
    //获取当前被点击的按钮位置
    QPushButton *pSenderBtn = qobject_cast<QPushButton*>(sender());
    if(pSenderBtn==nullptr)return;

    QModelIndex index = ui->tableWidget->indexAt(pSenderBtn->pos());
    QTableWidgetItem *item = ui->tableWidget->item(index.row(),0);
    QString code = item->data(item->type()).toString();
    QMessageBox::information(this,"ret",code);
}

void RoCorrectionDlg::on_pushButton_clicked()
{
    QStringList headText;
    headText<<"code"<<"info"<<"correction";
    ui->tableWidget->setColumnCount(headText.count());
    ui->tableWidget->setHorizontalHeaderLabels(headText);
}


void RoCorrectionDlg::on_pushButton_2_clicked()
{
    int curRow = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(curRow);
    createItemsARow(curRow,"11500","error");
}

```



## 踢出界面

![image-20211009174829599](报警界面项目.assets/image-20211009174829599.png)



获取时间

```cpp
qDebug()<<QDateTime::fromSecsSinceEpoch(jsObj.value(jsObj.keys().at(0)).toInt()).toString("yyyy/MM/dd hh:mm:ss");
```

```cpp
ui->tableWidget->setWordWrap(true);//让文字可以自动分行
```



```cpp
class RobotStateInfo{
    public:
//warning
    QVariantList fatals;                                  //告警码	Fatal的数组,	所有出现的	Fatal	告警都会出现在数组中
    QVariantList errors;                                  //告警码	Error的数组,	所有出现的	Error	告警都会出现在数组中
    QVariantList warnings;                            //告警码	Warning的数组,	所有出现的	Warning	告警都会出现在 数组中
    QVariantList notices;                                //noticess数组
}
```

```
URL是计算机专业书籍，英语UniformResourceLocator的缩写，意为统一资源定位符，是对可以从互联网上得到的资源的位置和访问方法的一种简洁的表示，俗称网址。每一个网页都有只属于自己的URL，它具有全球唯一性。

URL由以下几部分组成：协议，服务器主机地址，端口，路径和参数。

1、协议：常用的有http协议，https（http+ssh）协议。

2、服务器主机地址：可以是域名，也可以是主机名，或IP地址。

3、端口：服务器设定的端口。URL地址里一般无端口，因为服务器使用了协议的默认端口，用户通过url访问服务器时，可以省略。

4、路径：访问的资源在服务器下的相对路径，是服务器上的一个目录或者文件地址。

5、参数：查询搜索的部分，通过问号？连接到路径后面，有时候也归类到路径中。
一个标准的URL语法组成是下面这样的：

scheme://login:password@address:port/path_to_resource?query_string#fragment
简化下上面的组成，就可以把URL分成下面四部分：

传输协议 + 域名或IP地址 + [端口(端口为80时可省略)] + 资源路径 + 查询字符串
如果要指定访问端口时需要用“：”来隔开，例如(http://www.baidu.com:80/index.php)

http
http是一种超文本传输协议，除了没有用户名和密码之外，与通用的URL格式相符。如果省略了端口，就默认为80。
基本格式：http://<host>:<port>/<path>?<query>#<frag>
示例：http://www.baidu.com/index.html 或 http://www.baidu.com:80/index.html

4、查询字符串
很多资源，比如数据库服务，都是可以通过査询来缩小所请求资源类型范围的。假设数据库中维护着一个未售货物的清单，并可以对淸单进行査询，以判断产品是否有货，那就可以用下列URL来査询Web数据库网关，看看id为12731、颜色为blue、尺寸为large的条目是否有货：
http://www.test.com/query?id=12731&color=blue&size=large

分析下上面的URL，发现问号(?)右边的内容是前面没有出现的，这部分可以称为查询(query)组件，通常是以键值对的形式出现，多个键值对之间用&连接。此外，对于查询字符串除了有些不合规则的字符（比如空格等）还需要转码处理。

GET - 通常是从指定的服务器中获取数据，查询字符串（键值对）被附加在URL地址后面一起发送到服务器，如下面这样的：http://localhost:8090/api/query?id=3

POST - 通常是提交数据给指定的服务器处理，当然也可以从服务器获取数据。使用POST方法时，查询字符串或发送的数据在POST信息中单独存在，和请求URL一起发送到服务器，而不是像GET方法一样直接放在URL中

    ４、最后简要总结一下，使用PUT时，必须明确知道要操作的对象，如果对象不存在，创建对象；如果对象存在，则全部替换目标对象。同样POST既可以创建对象，也可以修改对象。但用POST创建对象时，之前并不知道要操作的对象，由HTTP服务器为新创建的对象生成一个唯一的URI；使用POST修改已存在的对象时，一般只是修改目标对象的部分内容。

经过了以上４条的分析之后，对PUT和POST的区别应该很清楚了吧？再强调一遍，PUT是“idempotent”（幂等），意味着相同的PUT请求不管执行多少次，结果都是一样的。但POST则不是。就类似于"x=1"这条语句是幂等的，因为无论执行多少次，变量x的值都是１；但"x++"就不是幂等的，因为每执行一次，变量x的值都不一样。

当然，这里讲的都是规范，都是最佳实践（best practise）。如果你在实际开发中，不按这个来，没有人能管得了你；但是当你的REST API要开放给别人使用时，就会和大家所接受的“普世价值”违背，很可能就会发生各种问题。
```

```cpp
bool HttpClientClass::postUrl(const QString & filePath,const QVariant &property)
    filePath???

```

测试用

```cpp
void slotUpdate( QVariantList fatalMap,  QVariantList errorMap,  QVariantList warningMap);

void RoCorrectionDlg::slotUpdate( QVariantList fatalMap,  QVariantList errorMap,  QVariantList warningMap)    

    //test
        QVariantMap var = {{"50000",1497698400},{"desc","xxxxx"},{"times",1}};
        QVariantMap var2 = {{"52000",1497698400},{"desc","xxxxx"},{"times",1}};
        fatalMap.insert(0,var);
        errorMap.insert(0,var2);
```

直接显示markdown内容,路径

```
QFile file(QString("%1/appInfo/setting/Ch/alarms/54000.md").arg(qApp->applicationDirPath()));

QFile file(fileName);
    if(file.exists()){
        file.open(QIODevice::ReadOnly);
    }else return;

    QTextStream stream(&file);
    stream.setCodec("UTF-8");

    this->appendPlainText(stream.readAll());
    file.close();
```

```cpp
1、52961 识别不到货架
 1.1 回收控制权
 1.2 提醒客户移动一下货架
 1.3 下发清错指令
 1.4 释放控制权 
    
    bool RobotManager::sendData(int port, const QVariant &jsonVar, const QVariant &byteVar, const QVariant &commandVar,QString ip,int number)
    
    RobotManager::init()->sendData(RobotInterface::E_ConfigTcp,QVariant(),json_map,"robot_config_clearallerrors_req",ip,RobotInterface::E_CN_Synchronize
```

```cpp
//显示markdown大文档内容
QString path = ALARMSPATH+level+"/"+code+".md";
        QFile file(path);
        if(file.exists()){
            file.open(QIODevice::ReadOnly);
        }else return;

        QTextStream stream(&file);
        stream.setCodec("UTF-8");
        ui->plainTextEdit->appendPlainText(stream.readAll());
        ui->plainTextEdit->moveCursor(QTextCursor::Start);
        file.close();
```

robot_config_clearwarning_req

 robot_config_clearerror_req

 robot_config_clearallerrors_req

http传输

```cpp
void RoRemoveDlg::on_pushButton_deal_clicked()
{
    if(ui->lineEdit_name->text().isEmpty()||ui->lineEdit_site->text().isEmpty())return;
    
    QSettings setting(QCoreApplication::applicationDirPath() + "/" + "AppInfo/robotInfo.ini", QSettings::IniFormat);
    QString strIp = setting.value("rds/ip").toString();
    QString strPort = setting.value("rds/port").toString();
    QString url = QString("%1:%2/api/ext/maintain/task-failed-and-retry").arg(strIp,strPort);
    
    QJsonObject obj;
    obj.insert("agvName", ui->lineEdit_name->text());
    obj.insert("siteId", ui->lineEdit_site->text());
    QJsonDocument doc(obj);
    
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    QNetworkAccessManager* naManager = new QNetworkAccessManager(this);
    connect(naManager, &QNetworkAccessManager::finished, this, &RoRemoveDlg::requestFinished);
    QNetworkReply* reply = naManager->post(request, doc.toBinaryData());
    
}

void RoRemoveDlg::requestFinished(QNetworkReply *reply)
{
    QByteArray data = reply->readAll();
    QJsonObject obj = QJsonDocument::fromBinaryData(data).object();
    if(!obj["success"].toBool()) qDebug()<<obj["message"].toString();
    
//    // 获取http状态码
//        QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
//        if(statusCode.isValid())
//            qDebug() << "status code=" << statusCode.toInt();
    
//        QVariant reason = reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
//        if(reason.isValid())
//            qDebug() << "reason=" << reason.toString();
    
//        QNetworkReply::NetworkError err = reply->error();
//        if(err != QNetworkReply::NoError) {
//            qDebug() << "Failed: " << reply->errorString();
//        }
//        else {
//            // 获取返回内容
//            qDebug() << reply->readAll();
//        }
}


    QNetworkRequest request;
    request.setUrl(QUrl(url));
    QNetworkAccessManager naManager(this);
    connect(&naManager, &QNetworkAccessManager::finished, this,[](){});
    QNetworkReply* reply = naManager.post(request, doc.toBinaryData());
    
    connect(reply, &QNetworkReply::finished, this, [this,reply](){
        if(reply->error() != QNetworkReply::NoError){
            reply->deleteLater();
            emit sigHttpRequestFinished();
            return;
        }
        int status =  reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).value<int>();
        QByteArray result =  reply->readAll();
        switch (status) {
        case 200:
        {//下载的都是文件
            
            break;
        }
        default:
            break;
        }

        reply->deleteLater();
        emit sigHttpRequestFinished();
    });

    QEventLoop *loop = new QEventLoop;
    connect(this,SIGNAL(sigHttpRequestFinished()),loop,SLOT(quit()));
    loop->exec();
    delete loop;
        
```

```cpp
报错：
[debug][ ..\..\..\RoboshopMini\RoboshopMiniCore\RoboshopMiniNetwork\RoboshopMiniClient.cpp ] [ 76 ][ RoboshopMiniClient::sendData ] "b.isEmpty()"
[debug][ ..\..\..\RoboshopMini\RoboshopMiniCore\RoboshopMiniNetwork\RoboshopMiniClient.cpp ] [ 77 ][ RoboshopMiniClient::sendData ] "Network operation timed out"
```

