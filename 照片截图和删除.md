# 照片查看，截图和删除



```cpp
#include "CameraShotDlg.h"
#include "ui_CameraShotDlg.h"

#include "UiClass.h"
#include "RoLog.h"

#include <QFile>
#include "DeleteShotDlg.h"

#define PICPATH "D:\\lesliex\\pic\\"

CameraShotDlg::CameraShotDlg(QWidget *parent)
    : CustomChildBaseDialog(parent)
    ,ui(new Ui::CameraShotDlg)
{
    ui->setupUi(this);
    this->resize(500,300);
    ui->label->resize(400,300);
    ui->label->sizeHint();
    _pic = new QPixmap(PICPATH"test.jpg");
//    ui->label->setScaledContents(true);//只能缩小放大已经在label中的图片

}

CameraShotDlg::~CameraShotDlg()
{
    delete ui;
}

void CameraShotDlg::resizeEvent(QResizeEvent *event)
{
    ui->label->resize(this->width()-ui->listWidget->width(),this->height());
    ui->label->setPixmap(_pic->scaledToWidth(ui->label->width()));
}


void CameraShotDlg::on_pushButton_shot_clicked()
{
    QString date = QDateTime::currentDateTime().toString("yyyyMMdd-hhmmsszzz");
    QString jpg = QString(PICPATH"%1.jpg").arg(date);
    ui->label->pixmap()->save(jpg,"JPG");
    ui->listWidget->addItem(date);
}


void CameraShotDlg::on_pushButton_del_clicked()
{
    if(!ui->listWidget->currentItem())return;
    if(!_dsd) _dsd = new DeleteShotDlg(this);
    _dsd->setPic(ui->listWidget->currentItem()->text());
    _dsd->show();
    connect(_dsd,&DeleteShotDlg::delDone,this,[this](const QString& date){
        for (int i=0; i < ui->listWidget->model()->rowCount() ;i++ ) {
            if(ui->listWidget->item(i)->text()==date){
            QFile::remove(QString(PICPATH"%1.jpg").arg(date));
            ui->listWidget->model()->removeRow(i);
            }
        }
    });
}


```

实现放大缩小

```cpp
#ifndef MYLABEL_H
#define MYLABEL_H

#include <QLabel>
#include <QObject>
#include <QImage>

class MyLabel : public QLabel
{
    Q_OBJECT
public:
    explicit MyLabel(QWidget *parent = 0);

protected:
    void contextMenuEvent(QContextMenuEvent *event);   //右键菜单

    void wheelEvent(QWheelEvent *event);               //鼠标滚轮滚动

    QImage pixmapScale(const QImage& image, const double & index);

public:
    void setLabelImage(const QString &filepath);
    void ZoomOutOrIn();

signals:
    void zoom(qreal);

public slots:
    void OnSelectImage();       //选择打开图片
    void OnZoomInImage();       //图片放大
    void OnZoomOutImage();      //图片缩小
    void OnPresetImage();       //图片还原

private:
    QImage Image;           //显示的图片
    qreal ZoomValue;  //鼠标缩放值
    QString LocalFileName;  //图片名
    QImage *m_NewImage;

    
    // QWidget interface
protected:
    virtual void paintEvent(QPaintEvent *event) override;
};

#endif // MYLABEL_H

```

```cpp
#include "MyLabel.h"
#include <QPainter>
#include <QDebug>
#include <QWheelEvent>
#include <QMenu>
#include <QFileDialog>


MyLabel::MyLabel(QWidget *parent) :
    QLabel(parent)
{
    ZoomValue = 1.0;  //鼠标缩放值

    m_NewImage = new QImage();
}

void MyLabel::ZoomOutOrIn()
{
    if (LocalFileName.isNull())
        return;

    Image.load(LocalFileName);

    *m_NewImage = this->pixmapScale(Image,ZoomValue);
    this->setPixmap(QPixmap::fromImage(*m_NewImage));
    this->resize(m_NewImage->width(), m_NewImage->height());
    emit zoom(ZoomValue);
}

void MyLabel::paintEvent(QPaintEvent *event)
{
  auto image =  m_NewImage->scaled(this->size(),Qt::KeepAspectRatio);
  QRect rect(this->rect().topLeft()+QPoint(50,50),this->rect().bottomRight()-QPoint(50,50));

  QPainter painter(this);
  painter.drawImage(rect,image,image.rect());
}



//图片无失真缩放
QImage MyLabel::pixmapScale(const QImage& image, const double & index)
{
    QImage r_image;
    r_image = image.scaled(image.width()*index, image.height()*index, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    return r_image;
}

//替换图片
void MyLabel::setLabelImage(const QString &filepath)
{
    this->LocalFileName = filepath;
    OnPresetImage();
}

//鼠标滚轮滚动
void MyLabel::wheelEvent(QWheelEvent *event)
{
    event->ignore();
    int value = event->delta();
    if (value > 0)  //放大
        OnZoomInImage();
    else            //缩小
        OnZoomOutImage();
}

//选择打开图片
void MyLabel::OnSelectImage()
{
    LocalFileName = QFileDialog::getOpenFileName(this, "Open Image", "./", tr("Images (*.png *.xpm *.jpg)"));

    QFile file(LocalFileName);
    if (!file.exists())
        return;
    ZoomValue = 1.0;
}
//图片放大
void MyLabel::OnZoomInImage()
{
    ZoomValue += 0.05;
    this->ZoomOutOrIn();
}
//图片缩小
void MyLabel::OnZoomOutImage()
{
    ZoomValue -= 0.05;
    if (ZoomValue <= 0)
    {
        ZoomValue = 0.05;
        return;
    }
    this->ZoomOutOrIn();
}
//图片还原
void MyLabel::OnPresetImage()
{
    ZoomValue = 1.0;
    this->ZoomOutOrIn();
}

//右键菜单
void MyLabel::contextMenuEvent(QContextMenuEvent *event)
{
    QPoint pos = event->pos();
    pos = this->mapToGlobal(pos);
    QMenu *menu = new QMenu(this);

    QAction *loadImage = new QAction(this);
    loadImage->setText("选择图片");
    //connect(loadImage, &QAction::triggered, this, &MyLabel::OnSelectImage);
    connect(loadImage,SIGNAL(triggered()),this,SLOT(OnSelectImage()));
    menu->addAction(loadImage);
    menu->addSeparator();

    QAction *zoomInAction = new QAction(this);
    zoomInAction->setText("放大");
    //connect(zoomInAction, &QAction::triggered, this, &MyLabel::OnZoomInImage);
    connect(zoomInAction,SIGNAL(triggered()),this,SLOT(OnZoomInImage()));
    menu->addAction(zoomInAction);

    QAction *zoomOutAction = new QAction(this);
    zoomOutAction->setText("缩小");
    //connect(zoomOutAction, &QAction::triggered, this, &MyLabel::OnZoomOutImage);
    connect(zoomOutAction,SIGNAL(triggered()),this,SLOT(OnZoomOutImage()));
    menu->addAction(zoomOutAction);

    QAction *presetAction = new QAction(this);
    presetAction->setText("还原");
    //connect(presetAction, &QAction::triggered, this, &MyLabel::OnPresetImage);
    connect(presetAction,SIGNAL(triggered()),this,SLOT(OnPresetImage()));
    menu->addAction(presetAction);

    menu->exec(pos);
}

```



```
QToolTip::showText(w->mapToGlobal(w->pos()),"123",w);
```

<img src="照片截图和删除.assets/image-20211110155504132.png" alt="image-20211110155504132" style="zoom:67%;" />

```cpp
#include "CameraShotDlg.h"
#include "ui_CameraShotDlg.h"

#include "UiClass.h"
#include "RoLog.h"

#include <QFile>
#include "DeleteShotDlg.h"
#include <QToolTip>
#include <QDesktopServices>

#define PICPATH "D:\\lesliex\\pic\\"

CameraShotDlg::CameraShotDlg(QWidget *parent)
    : CustomChildBaseDialog(parent)
    ,ui(new Ui::CameraShotDlg)
{
    ui->setupUi(this);
    this->resize(1200,600);
    _img = new QImage(PICPATH"test.jpg");

    ui->pushButton_muldel->setObjectName("pushButton_delete_total");
    ui->pushButton_shot->setObjectName("blueButton");

    ui->listWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
    connect(ui->listWidget,&QListWidget::itemDoubleClicked,this,[this](QListWidgetItem* item){
        QString date =  _map[ui->listWidget->itemWidget(item)];
        DeleteShotDlg* _dsd = new DeleteShotDlg(this);
        _dsd->setWindowTitle(date);
        _dsd->setPic(PICPATH+date);
        _dsd->previewMode();
        _dsd->show();
        connect(_dsd,&DeleteShotDlg::delDone,this,[_dsd](bool i){ _dsd->deleteLater(); });
    });
}

CameraShotDlg::~CameraShotDlg()
{
    foreach(auto w, _map.keys()){if(!w)w->deleteLater();}
    delete _img;
    delete ui;
}

void CameraShotDlg::on_pushButton_shot_clicked()
{
    //以日期格式保存
    QString date = QDateTime::currentDateTime().toString("yyyyMMdd-hhmmss-zzz");
    QString jpg = QString(PICPATH"%1.jpg").arg(date);
    ui->label->pixmap()->save(jpg,"JPG");

    //创建listItem绑定窗口
    QWidget *w = new QWidget(this);
    QHBoxLayout* layout = new QHBoxLayout(w);
    QPushButton* btn = new QPushButton(w);
    QLabel* lab = new QLabel(date,w);
    btn->setObjectName("pushButton_delete");
    layout->addWidget(lab);
    layout->addWidget(btn);
    w->setLayout(layout);

    _map.insert(w,date);

    QListWidgetItem* item = new QListWidgetItem(ui->listWidget);
    item->setSizeHint(QSize(80,80));//没有这句布局不能显示内容
    ui->listWidget->addItem(item);
    ui->listWidget->setItemWidget(item,w);

    connect(btn,&QPushButton::clicked,this,[=](){
        btn->setDisabled(true);
        DeleteShotDlg* _dsd = new DeleteShotDlg();
        _dsd->setWindowTitle(date);
        _dsd->setPic(jpg);
        _dsd->show();

        connect(_dsd,&DeleteShotDlg::delDone,this,[=](bool done){
            btn->setDisabled(false);
            if(!done)_dsd->deleteLater();
            else{
                _map.remove(w);
                int row = ui->listWidget->row(item);
                ui->listWidget->itemWidget(item)->deleteLater();
                ui->listWidget->removeItemWidget(item);
                ui->listWidget->model()->removeRow(row);
                if(QFile::exists(jpg))QFile::remove(jpg);
                _dsd->deleteLater();
            }
        });
    });
}


void CameraShotDlg::resizeEvent(QResizeEvent *event)
{
    auto image =  _img->scaled(ui->label->size(),Qt::KeepAspectRatio);
    ui->label->setPixmap(QPixmap::fromImage(image));
}


void CameraShotDlg::on_pushButton_muldel_clicked()
{
    if(ui->listWidget->selectedItems().isEmpty()) {
        UiClass::init()->showToastr(tr("No pictures selected"),SCToastr::E_Warning,this);
        return;
    }
    if(UiClass::init()->showDialog(tr("Delete selected pictures"),"Delete Pictures",0)!=0) return;
    foreach(auto item, ui->listWidget->selectedItems()){
        QFile::remove(PICPATH+_map[ui->listWidget->itemWidget(item)]+".jpg");
        _map.remove(ui->listWidget->itemWidget(item));
        int row = ui->listWidget->row(item);
        ui->listWidget->itemWidget(item)->deleteLater();
        ui->listWidget->removeItemWidget(item);
        ui->listWidget->model()->removeRow(row);
    }
}


void CameraShotDlg::on_pushButton_opendir_clicked()
{
    QDesktopServices::openUrl(QUrl("file:///D:\\lesliex\\pic\\", QUrl::TolerantMode));
}


void CameraShotDlg::on_pushButton_clicked()
{
    static bool expand = true;
    if(expand==true){
        ui->pushButton_muldel->setVisible(false);
        ui->pushButton_opendir->setVisible(false);
        ui->pushButton_shot->setVisible(false);
        ui->listWidget->setVisible(false);
        ui->pushButton_shot->setVisible(false);
        ui->pushButton->setText("<");
        expand=false;
    }else{
        ui->pushButton_muldel->setVisible(true);
        ui->pushButton_opendir->setVisible(true);
        ui->pushButton_shot->setVisible(true);
        ui->listWidget->setVisible(true);
        ui->pushButton_shot->setVisible(true);
        ui->pushButton->setText(">");
        expand=true;
    }
    this->resize(this->width()-1,this->height()-1);
}


```

