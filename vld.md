vld

1.

```
WARNING: Visual Leak Detector detected memory leaks!
---------- Block 152 at 0x0000000095515780: 16 bytes ----------
  Leak Hash: 0x5ED9EFE4, Count: 1, Total 16 bytes
  Call Stack (TID 18128):
    ucrtbased.dll!malloc()
    f:\dd\vctools\crt\vcstartup\src\heap\new_scalar.cpp (19): RoboshopMini.exe!operator new() + 0xA bytes
    d:\lesliex\coding\nettool\roboshopmini\roboshopminiwindow\showmessagedlg.cpp (6): RoboshopMini.exe!ShowMessageDlg::ShowMessageDlg() + 0xA bytes
    d:\lesliex\coding\nettool\roboshopmini\roboshopminiwindow\showmessagedlg.cpp (28): RoboshopMini.exe!ShowMessageDlg::init() + 0x25 bytes
    d:\lesliex\coding\nettool\roboshopmini\roboshopminiwindow\rocorrectiondealdlg.cpp (117): RoboshopMini.exe!RoCorrectionDealDlg::showFirstPage() + 0x34 bytes
    d:\lesliex\coding\nettool\roboshopmini\roboshopminiwindow\rocorrectiondlg.cpp (104): RoboshopMini.exe!RoCorrectionDlg::slotTableBtnClicked() + 0x21 bytes
```

2.

```
---------- Block 259 at 0x000000002CAC5A50: 20 bytes ----------
  Leak Hash: 0x2A967DC6, Count: 1, Total 20 bytes
  Call Stack (TID 13952):
    ucrtbased.dll!malloc()
    f:\dd\vctools\crt\vcstartup\src\heap\new_array.cpp (16): RoboshopMiniNetworkd.dll!operator new[]()
    d:\lesliex\coding\nettool\roboshopmini\roboshopminicore\roboshopmininetwork\roboshopminiclient.cpp (46): RoboshopMiniNetworkd.dll!RoboshopMiniClient::sendData() + 0x19 bytes
    d:\lesliex\coding\nettool\roboshopmini\roboshopminicore\roboshopmininetwork\roboshopmininetwork.cpp (249): RoboshopMiniNetworkd.dll!RoboshopMiniNetwork::acquireError() + 0x1E bytes
    d:\lesliex\coding\nettool\roboshopmini\roboshopminicore\roboshopmininetwork\roboshopmininetwork.cpp (224): RoboshopMiniNetworkd.dll!RoboshopMiniNetwork::flushRobots2() + 0x3F bytes
    d:\lesliex\coding\nettool\build-roboshopmini-desktop_qt_5_12_11_msvc2015_64bit-debug\roboshopminicore\roboshopmininetwork\debug\moc_roboshopmininetwork.cpp (118): RoboshopMiniNetworkd.dll!RoboshopMiniNetwork::qt_static_metacall() + 0xA bytes
    Qt5Cored.dll!QPersistentModelIndex::parent() + 0x47CBED bytes
    Qt5Cored.dll!QPersistentModelIndex::parent() + 0x47B760 bytes
    Qt5Cored.dll!QPersistentModelIndex::parent() + 0x50E55 bytes
    d:\lesliex\coding\nettool\roboshopmini\roboshopminiwindow\minihomewidget.cpp (42): RoboshopMini.exe!<lambda_3a3e166d601adcbd1bdf03b7a641e68f>::operator()()
    d:\qt\qt5.12.11\5.12.11\msvc2015_64\include\qtconcurrent\qtconcurrentstoredfunctioncall.h (70): RoboshopMini.exe!QtConcurrent::StoredFunctorCall0<void,<lambda_3a3e166d601adcbd1bdf03b7a641e68f> >::runFunctor() + 0x1A bytes
    d:\qt\qt5.12.11\5.12.11\msvc2015_64\include\qtconcurrent\qtconcurrentrunbase.h (136): RoboshopMini.exe!QtConcurrent::RunFunctionTask<void>::run() + 0x22 bytes
    Qt5Cored.dll!QPersistentModelIndex::parent() + 0xA61A7 bytes
    Qt5Cored.dll!QPersistentModelIndex::parent() + 0x934F7 bytes
    KERNEL32.DLL!BaseThreadInitThunk() + 0x14 bytes
    ntdll.dll!RtlUserThreadStart() + 0x21 bytes
  Data:
    5A 01 04 1A    00 00 00 04    04 1A DD 0A    00 00 00 00     Z....... ........
    7B 0A 7D 0A                                                  {.}..... ........


Visual Leak Detector detected 83 memory leaks (6576 bytes).
Largest number used: 22619 bytes.
Total allocations: 26679 bytes.
Visual Leak Detector is now exiting.
```

```
d 66 memory leaks (5380 bytes).
Largest number used: 20759 bytes.
Total allocations: 23635 bytes.
Visual Leak Detector is now exiting.
```

没有删除生成的 tcpSocket，client中的缓存buf没有删除

```
Visual Leak Detector detected 55 memory leaks (4444 bytes).
Largest number used: 14883 bytes.
Total allocations: 23675 bytes.
Visual Leak Detector is now exiting.

Visual Leak Detector detected 112 memory leaks (8320 bytes).
Largest number used: 18759 bytes.
Total allocations: 42755 bytes.
Visual Leak Detector is now exiting.

Visual Leak Detector detected 20 memory leaks (2152 bytes).
Largest number used: 24835 bytes.
Total allocations: 38971 bytes.
Visual Leak Detector is now exiting.
```

```
bool RoboshopMiniNetwork::connectRobot(const QString &id, int port)
{
    MiniRobot* pRobot = _pRobotManager->getRobot(id);
    if(!pRobot){
        return false;
    }

    QTcpSocket* pSocket = pRobot->getSocket(port);
    if(!pSocket){
        pSocket = new QTcpSocket();
        pSocket->setProxy(QNetworkProxy::NoProxy);
    }

    if(pSocket->state() != QAbstractSocket::ConnectedState){
        pSocket->connectToHost(pRobot->getIp(), port);
        if(!pSocket->waitForConnected(100)){
            pSocket->deleteLater();
            SCDebug<<"connect socket failed";
            pRobot->setStatus(MiniRobot::UnconnectedState);
            return false;
        }
//        SCDebug<<"connect socket success";
    }
    pRobot->setSocket(port, pSocket);
    return true;
}
```

```cpp

private:
// This is important
    class GC // 垃圾回收类
    {
    public:
        GC()
        {
            qDebug()<<"GC construction"<<endl;
        }
        ~GC()
        {
            qDebug()<<"GC destruction"<<endl;
            // We can destory all the resouce here, eg:db connector, file handle and so on
            if (_this != NULL)
            {
                delete _this;
                _this = NULL;
                qDebug()<<"Singleton destruction"<<endl;
            }
        }
    };
    static GC gc;  //垃圾回收类的静态成员
```

