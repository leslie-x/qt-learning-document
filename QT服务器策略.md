### QT服务器策略

### 1.handle策略

- 根据操作数来决定调用哪个handle方法类，工厂中有一个方法类的列表
- 结合工厂模式、策略模式、命令模式 
- 缺点是类比较多，浪费资源
- 优点是策略比较明确

### 2.invokeMethod使用

- 根据函数名来决定调用那个函数，函数名和编号对应存储在一个数据库中
- 使用invokeMethod来调用函数，这些函数都在一个obj对象中
- 优点是类很少，直接调用函数，可以跨线程
- 适合大型项目

```c
[static] bool QMetaObject::invokeMethod(QObject *obj, const char *member, Qt::ConnectionType type, QGenericReturnArgument ret, QGenericArgument val0 = QGenericArgument(nullptr)//最多10个
)
```

