# 插件编写规范

### 文字规范

1. 应用界面上不要出现中文

2. 注释最好都在在头文件中撰写

3. 函数的说明格式为：

   ```c
       /**
        * @brief getDevsInfo
        * @param iface
        * @return 
        */
   ```

4. 注意命名规则，变量名使用小驼峰，类名使用大驼峰，最多一个下划线'_'

5. 写好程序后，按 Ctl+a 然后 Ctl+i 将页面对齐

6. Message语句都用英文，可以用中文在谷歌翻译输出英文再粘贴。注意标点都是英文字符，注意大小写，注意末尾不要出现英文句号，末尾无符号。

### 宏

使用 SCDebug 代替 qDebug()

### 类

1.消息框的更改

```c
QMessageBox::information(this,"info","Update Success");
//改编成
UiClass::init()->showToastr(tr("Update Success"),SCToastr::E_Info,this);
```

2.窗口类名

注意自己写的插件都为widget窗口，不要用mainwindow

```c++
QWidget#CustomChildBaseDialog{
background: rgba(249, 249, 249,255);
}
//******************************************************8
#include "CustomBaseDialog/CustomChildBaseDialog.h"

class LogAnalyzerDlg : public CustomChildBaseDialog
{}
```



### 元件对象

窗口部件的对象名称，只能修改_后面的部分，还有一些特殊的比如delete update，会自动变成图标

注意：如果有两个图标是一样的比如两个删除键，则可以使用

```c++
ui->pushButton_1->setObjectName("pushButton_delete");
```



### 添加部件：

使用widget创建界面

头文件：

```cpp
#include "CustomBaseDialog/CustomChildBaseDialog.h"
QT_BEGIN_NAMESPACE
namespace Ui { class SmapPreview; }
QT_END_NAMESPACE
class SmapPreview : public CustomChildBaseDialog
```

源文件：

```cpp
SmapPreview::SmapPreview(QWidget *parent)
    : CustomChildBaseDialog(parent)
    , ui(new Ui::SmapPreview)
```

