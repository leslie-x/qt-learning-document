# QT实现文本高亮和查找

### 概念

文档的光标主要基于QTextCursor类，文档的框架主要基于QTextDocument类。
一个富文本的文档结构主要分为几种元素：框架（QTextFrame）、文本块（QTextBlock）、表格（QTextTable）、和列表（QTextList）。
每种元素的格式有相应的format类表示：框架格式（QTextFrameFormat）、文本块格式（QTextBlockFormat）、表格格式（QTextTableFormat）、列表格式（QTextListFormat）。这些格式通常配合QTextCursor类使用。
QTextEdit类就是一个富文本编辑器，在构建QTextEdit类对象时就已经构建了一个QTextDocument类对象和一个QTextCursor类对象。只需调用他们进行相应的操作即可。

### 文本高亮

高亮文本文件颜色预设

```
$$keyword     r      g     b
auto         212    12    204
short	     244    98    32
int          244    98    32
long	     244    98    32
float	     244    98    32
double	     244    98    32
char         122    98    32
struct	     122    98    32
union	     122    98    204
enum         122    98    204
typedef	     233    98    204
const        124    150   243
unsigned     191    198   32
signed       191    198   32
extern       191    198   32
register     191    198   32
static       122    98    204
volatile     233    124   204
void         233    164   204
if	     190    32    190
else	     190    32    190
switch	     190    32    190
case	     190    32    190
for	     190    32    190
do	     190    32    190
while	     190    32    190
goto	     122    98    204
continue     190    32    190
break        190    32    190
default      122    98    204
sizeof       22     134   152
return       46     159   170
#include     0      255   0

```



头文件

```c
#ifndef MYTEXTEDITOR_H
#define MYTEXTEDITOR_H

#include <QtWidgets>

//高亮文件预设文件结构
typedef struct SyntaxHight {
    QString keyWord;
    QColor   highlightColor;
}SyntaxHight_T;

//自定义高亮规则类
class MySyntaxHighlighterEditor : public QSyntaxHighlighter {

    Q_OBJECT

public:
    MySyntaxHighlighterEditor(QTextDocument *document = 0);
    void readSyntaxHighter(const QString &fileName);
    QMap<QString, QColor> syntaxHightMap; //保存语法高亮信息

protected:
    void highlightBlock(const QString &text);

private:
    QRegularExpression matchReExpression;

};

//声明类
class MyGCodeTextEdit;
class LineNumberArea;

//自定义QPlainTextEdit文本框类
class MyGCodeTextEdit : public QPlainTextEdit{

    Q_OBJECT

public:
    MyGCodeTextEdit(QWidget *parent  = 0);
    //void setCompleter(QCompleter *completer);
    QString wordUnderCursor() const; //Cursor为光标
    int lineNumberAreaWidth();
    void lineNumberAreaPaintEvent(QPaintEvent *event);

protected:
    void resizeEvent(QResizeEvent *event) override;
    void keyPressEvent(QKeyEvent *e);

private slots:
    void updateLineNumberAreaWidth(int newBlockCount);
    void highlightCurrentLine();//高亮当前行
    void updateLineNumberArea(const QRect &, int);

public slots:
    void onCompleterActivated(const QString &completion);
    void onCurosPosChange(void);

private:
    MySyntaxHighlighterEditor *gCodeHighlighter;
    QCompleter *keyWordsComplter; //单词自动补全的类
    QStringList keyWordsList;
    QTextCursor curTextCursor;
    QRect curTextCursorRect;
    QString completerPrefix;

    //显示行号
    QWidget *lineNumberArea;

};

class LineNumberArea : public QWidget
{

public:
    explicit LineNumberArea(MyGCodeTextEdit *editor): QWidget(editor)
    {
        gCodeTextEdit = editor;
        //setVisible(true);
    }

    QSize sizeHint() const override
    {
        return QSize(gCodeTextEdit->lineNumberAreaWidth(), 0);
    }

protected:
    void paintEvent(QPaintEvent *event) override
    {
        gCodeTextEdit->lineNumberAreaPaintEvent(event);
        //qDebug() << "gCodeTextEdit:" << __FUNCTION__;
    }

private:
    MyGCodeTextEdit *gCodeTextEdit;
};

#endif // MYTEXTEDITOR_H

```

源文件

```c
#include "mytexteditor.h"

/**************MySyntaxHighlighterEditor******************/
MySyntaxHighlighterEditor::MySyntaxHighlighterEditor(QTextDocument *document):QSyntaxHighlighter(document)
{
}


void MySyntaxHighlighterEditor::readSyntaxHighter(const QString &fileName)
{
    QFile file(fileName);
    if (false == file.open(QIODevice::ReadOnly))
    {
        qDebug() << "Open file " << fileName << "error" << __FUNCTION__;
    }

    QTextStream readFileStream(&file);
    QString keyWord;
    QString readLineStr;
    QStringList lineWordList;
    QString matchReString;
    QRegularExpression re("[ ]+");
    int r, g, b;

    while(!readFileStream.atEnd())
    {
        readLineStr = readFileStream.readLine();
        if (readLineStr.startsWith("$$")) //comment line
        {
            continue;
        }
        readLineStr.replace("\t", " ");
        lineWordList = readLineStr.split(re);
        if (lineWordList.size() != 4)
        {
            continue;
        }
        keyWord = lineWordList.at(0);
        matchReString.append(keyWord);
        matchReString.append("|");
        r = lineWordList.at(1).toInt();
        g = lineWordList.at(2).toInt();
        b = lineWordList.at(3).toInt();
        syntaxHightMap.insert(keyWord, QColor(r, g, b));
    }
    matchReString.trimmed();
    matchReExpression.setPattern(matchReString);
    qDebug() << "matchReString:" << matchReString;

}

void MySyntaxHighlighterEditor::highlightBlock(const QString &text)
{
    QTextCharFormat myClassFormat;
    myClassFormat.setFontWeight(QFont::Bold);
    QRegularExpressionMatchIterator i = matchReExpression.globalMatch(text);
    while (i.hasNext())
    {
        QRegularExpressionMatch match = i.next();
        myClassFormat.setForeground(QBrush(syntaxHightMap.value(match.captured())));
        setFormat(match.capturedStart(), match.capturedLength(), myClassFormat);
    }
}

/**************MyGCodeTextEdit******************/
//public functions
MyGCodeTextEdit::MyGCodeTextEdit(QWidget *parent):QPlainTextEdit(parent)
{
    gCodeHighlighter = new MySyntaxHighlighterEditor(this->document());
    gCodeHighlighter->readSyntaxHighter(QString(":/SynatxHight/C.txt")); //设置语法高亮文件

   // 补全列表设置
    QMap<QString, QColor>::iterator iter;
    for (iter = gCodeHighlighter->syntaxHightMap.begin(); iter != gCodeHighlighter->syntaxHightMap.end(); ++iter)
    {
        keyWordsList.append(iter.key());
    }
    qDebug() << "keyWordsList" << keyWordsList;

    keyWordsComplter = new QCompleter(keyWordsList);
    keyWordsComplter->setWidget(this);
    keyWordsComplter->setCaseSensitivity(Qt::CaseInsensitive);
    keyWordsComplter->setCompletionMode(QCompleter::PopupCompletion);
    keyWordsComplter->setMaxVisibleItems(6);

    lineNumberArea = new LineNumberArea(this);
    lineNumberArea->setVisible(true);

    connect(keyWordsComplter, SIGNAL(activated(QString)), this, SLOT(onCompleterActivated(QString)));

    connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateLineNumberAreaWidth(int)));
    connect(this, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateLineNumberArea(QRect,int)));

    connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(highlightCurrentLine()));
    connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(onCurosPosChange()));

    setBaseSize(1200, 800);

    updateLineNumberAreaWidth(0);
    highlightCurrentLine();

}
#include <QByteArray>

QString MyGCodeTextEdit::wordUnderCursor() const
{
    //不断向左移动cursor，并选中字符，并查看选中的单词中是否含有空格——空格作为单词的分隔符
    QTextCursor curTextCursor = textCursor();
    QString selectedString;
    while (curTextCursor.movePosition(QTextCursor::Left, QTextCursor::KeepAnchor, 1))
    {
        selectedString = curTextCursor.selectedText();
        if (selectedString.startsWith(QString(" ")) || selectedString.startsWith(QChar(0x422029)))
        {
            break;
        }

    }
    if (selectedString.startsWith(QChar(0x422029))) //0x422029为')'
    {
        selectedString.replace(0, 1, QChar(' '));
    }
    return selectedString.trimmed();

}

int MyGCodeTextEdit::lineNumberAreaWidth()
{
    int digits = 1;
    int max = qMax(1, blockCount());
    while (max >= 10)
    {
        max /= 10;
        ++digits;
    }

    int space = 3 + fontMetrics().width(QLatin1Char('9')) * digits;

    return space;
}

//protected Events
void MyGCodeTextEdit::keyPressEvent(QKeyEvent *e)
{
    return;//用作查看，暂且过滤
    if (keyWordsComplter)
    {
        if (keyWordsComplter->popup()->isVisible())
        {
            switch(e->key())
            {
                case Qt::Key_Escape:
                case Qt::Key_Enter:
                case Qt::Key_Return:
                case Qt::Key_Tab:
                    e->ignore();
                    return;
                default:
                    break;
            }
        }
        QPlainTextEdit::keyPressEvent(e);
        completerPrefix = this->wordUnderCursor();
        keyWordsComplter->setCompletionPrefix(completerPrefix); // 通过设置QCompleter的前缀，来让Completer寻找关键词
        curTextCursorRect = cursorRect();
        if (completerPrefix == "")
        {
            return;
        }
        //qDebug() << "completerPrefix:" << completerPrefix << " match_count:" << keyWordsComplter->completionCount() << " completionColumn:"<<keyWordsComplter->completionColumn();
        if (keyWordsComplter->completionCount() > 0)
        {
            keyWordsComplter->complete(QRect(curTextCursorRect.left(), curTextCursorRect.top(), 60, 15));
        }
    }
}

void MyGCodeTextEdit::resizeEvent(QResizeEvent *e)
{
    QPlainTextEdit::resizeEvent(e);
    QRect cr = contentsRect();
    lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}

void MyGCodeTextEdit::lineNumberAreaPaintEvent(QPaintEvent *event)
{
    QPainter painter(lineNumberArea);
    painter.fillRect(event->rect(), Qt::lightGray);

    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
    int bottom = top + (int) blockBoundingRect(block).height();

    while (block.isValid() && top <= event->rect().bottom())
    {
        if (block.isVisible() && bottom >= event->rect().top())
        {
            QString number = QString::number(blockNumber + 1);
            painter.setPen(Qt::black);
            painter.drawText(0, top, lineNumberArea->width(), fontMetrics().height(),
                             Qt::AlignRight, number);
        }

        block = block.next();
        top = bottom;
        bottom = top + (int) blockBoundingRect(block).height();
        ++blockNumber;
    }
}

//public slots
void MyGCodeTextEdit::onCompleterActivated(const QString &completion)
{
    QString completionPrefix = wordUnderCursor(),
            shouldInertText = completion;
    curTextCursor = textCursor();
    if (!completion.contains(completionPrefix))
    {
        // delete the previously typed.
        curTextCursor.movePosition(QTextCursor::Left, QTextCursor::KeepAnchor, completionPrefix.size());
        curTextCursor.clearSelection();
    }
    else
    {
        // 补全相应的字符
        shouldInertText = shouldInertText.replace(
            shouldInertText.indexOf(completionPrefix), completionPrefix.size(), "");
    }
    curTextCursor.insertText(shouldInertText);
}

void MyGCodeTextEdit::onCurosPosChange()
{
    QString completionPrefix = wordUnderCursor();
    if (completionPrefix == "")
    {
        keyWordsComplter->setCompletionPrefix("----");
        keyWordsComplter->complete(QRect(curTextCursorRect.left(), curTextCursorRect.top(), 60, 15));
    }
    highlightCurrentLine();

}

void MyGCodeTextEdit::updateLineNumberAreaWidth(int /* newBlockCount */)
{
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}

void MyGCodeTextEdit::highlightCurrentLine()
{
    QList<QTextEdit::ExtraSelection> extraSelections;

    if (!isReadOnly())
    {
        QTextEdit::ExtraSelection selection;

        QColor lineColor = QColor(Qt::yellow).lighter(160);

        selection.format.setBackground(lineColor);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }

    setExtraSelections(extraSelections);
}

void MyGCodeTextEdit::updateLineNumberArea(const QRect & rect, int dy)
{
    if (dy)
        lineNumberArea->scroll(0, dy);
    else
        lineNumberArea->update(0, rect.y(), lineNumberArea->width(), rect.height());

    if (rect.contains(viewport()->rect()))
        updateLineNumberAreaWidth(0);
}

```



### 查找

查找并强调显示

使用find()函数，第二个参数可以规定是否向后查找、是否完整词查找

```c
//查找关键字
void MainWindow::on_pushButton_clicked()
{
    QString str = ui->FindText->toPlainText();
    //向后查找
    if(ui->plainTextEdit->find(str)==true){
        //查找后强调颜色
        QPalette palette = ui->plainTextEdit->palette();
        palette.setColor(QPalette::Highlight,palette.color(QPalette::Active,QPalette::Highlight));
        ui->plainTextEdit->setPalette(palette);

    }else {
        //若无法继续查找则跳出对话框
      QMessageBox::warning(this, tr("Find word"),
                            tr("no find."),
                            QMessageBox::Cancel
                           );
    }
}


void MainWindow::on_pushButton_2_clicked()
{
    QString str = ui->FindText->toPlainText();
    //向前查找
    if(ui->plainTextEdit->find(str,QTextDocument::FindFlag::FindBackward)==true){

        QPalette palette = ui->plainTextEdit->palette();
        palette.setColor(QPalette::Highlight,palette.color(QPalette::Active,QPalette::Highlight));
        ui->plainTextEdit->setPalette(palette);

    }else {
        QMessageBox::warning(this, tr("Find word"),
                            tr("no find."),
                            QMessageBox::Cancel
                             );
    }
}
```



### 处理日志

```c
//处理log文件
void MainWindow::handleLogFile(QString fileName)
{
    QFile file(fileName);
    if(file.exists()){
        file.open(QIODevice::ReadOnly);
    }else return;

    QByteArray data = file.readAll();
    file.close();

    ui->plainTextEdit->clear();
    ui->plainTextEdit->appendPlainText(data);
    ui->plainTextEdit->moveCursor(QTextCursor::MoveOperation::Start);//移动光标到最开始

}
```

