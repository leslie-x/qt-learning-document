# 机器码自动获取授权

需求：

Roboshop 上，增加 【在线激活】和 【导入授权文件】

【在线激活】通过 https://lic.seer-group.com/#/license 的 http api 进行查询和下载授权文件，然后激活
【导入授权文件】即为原来的 【激活设备】

如果 core （通用的 feature）或者 rbk_diff（小车上的基础 feature） 功能模块已激活，则隐藏 【在线激活】 的按钮



具体内容：

```
查询

https://lic.seer-group.com/api/search_by_mcode?mcode=240829f0-0bdb7847-9d172edf-ea61073d&page_no=1&page_size=1&prod_name=RDS Core
//这里的mccode等是什么？Machine code
返回
{"code":200,"message":"Success","total":1,"data":[{"order_id":"1464429456658665472","act_time":"2021-11-27 11:02:45","prod_name":"RDS Core","opt_user":"liulong","mcode":"240829f0-0bdb7847-9d172edf-ea61073d","desc":"毛傲的笔记本，用于core日常测试","remark":"","is_active":true,"features":[{"feat_name":"core","expired_time":"永久"}]}]}

下载 //需要用到上面的order_id

https://lic.seer-group.com/api/license?order_id=1464429456658665472
返回
-----BEGIN LICENSE-----
REUgZDIwVDQxWXZFaGVTRndXckFFK1RiZmlXcm5kZFF4R0VBUTd5OTFDPXJkNFc0YU
tbRzYwdjIKckVuNmZ3M1k3XTBqY29pIG85K2RkUm1IdHMyaGwzCnpFNXlkN1JvVHE9
NTBMVG5RYi91ODdyMzFTYzBBNC9lbWd6WDAtNk42ZyBDYU9yUlJDUnAyMG4KWTMwSW
5vN0N4VW12IHNYS21hMHBLUCsKQml1Sm89S2xJZWJxSVl1aXB1dVMyUkxsZjktdkg3
ZyBTVG9SbzdNdFQ3MjBNYyBoYVRvOHZMWGw2LWU1eWFpPWlGRW1PeE5LdTg3aQo=
-----END LICENSE-----
```



界面

![image-20211201114527608](C:\Users\lesliex\AppData\Roaming\Typora\typora-user-images\image-20211201114527608.png)