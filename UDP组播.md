# UDP组播

本地管理组播地址：`239.0.0.0 ~ 239.255.255.255`

先绑定端口，

`udpSocket->bind(QHostAddress::AnyIPv4,groupPort,QUdpSocket::SareAddress)`

然后使用函数加入、退出组播

`bool QUdpSocket::joinMulticastGroup(const QHostAddress &groupAddress)`

`bool QUdpSocket::leaveMulticastGroup(const QHostAddress &groupAddress)`

发送和接受组播数据报

`writeDatagram(const QByteArray &datagram, const QHostAddress &host, quint16 port)`

`qint64 QUdpSocket::readDatagram(char *data, qint64 maxSize, QHostAddress *address = nullptr, quint16 *port = nullptr)`

` QNetworkDatagram receiveDatagram(qint64 maxSize = -1)`获取QNetworkDatagram 的senderAddress和senderPort和data

```cpp
void PepperlFuchsIPConfigDlg::SSDPsearch()
{
    //SSDP寻找设备
    QString searchRequest = "M-SEARCH * HTTP/1.1\r\n"
                             "H0ST: 239.255.255.250:1900\r\n"
                             "ST: urn:pepperl-fuchs-com:device:R2000:1\r\n"
                             "MAN: \"ssdp:discover\"\r\n"
                             "MX: 1\r\n";
    //使用UDP组播
    quint16 port = 1900;
    QString hostAddress = "239.255.255.250";
    QHostAddress groupAddress(hostAddress);
    _udpSocket = new QUdpSocket(this);
    _udpSocket->setSocketOption(QAbstractSocket::MulticastTtlOption,1);

    //绑定ip，加入组播，发送消息
    _udpSocket->bind(QHostAddress::AnyIPv4,port,QUdpSocket::ShareAddress);
    _udpSocket->joinMulticastGroup(groupAddress);
    _udpSocket->writeDatagram(searchRequest.toUtf8(),groupAddress,port);

    //获取udp消息
    connect(_udpSocket,&QUdpSocket::readyRead,this,[this](){
        QNetworkDatagram datagram = _udpSocket->receiveDatagram();
        qDebug()<<"peer address:"<<datagram.senderAddress();
        qDebug()<<"peer port:"<<datagram.senderPort();
        qDebug()<<"peer data:"<<datagram.data();
    });

}

void PepperlFuchsIPConfigDlg::SSDPsearchOff()
{
    //退出组播，断开绑定
    _udpSocket->leaveMulticastGroup(_groupAddress);
    _udpSocket->abort();
    _udpSocket->deleteLater();
    _udpSocket = Q_NULLPTR;
}
```

