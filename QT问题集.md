# QT问题集

1. LNK2019：无法解析的外部符号：
   - 原因1：函数在.h中声明但却没有实现
   - 原因2：添加新的.ui窗体文件时编译器没有为它生成新的.obj文件而报错，需要重新执行qmake，而不是重新构建项目
   
2. Debug版本运行正常，Release版本崩溃

   - 原因：没有区分动态库的版本，

     ```makefile
     #错误：win32: LIBS += -L$$PWD/quazipPack/lib/ -lquazipd
     
     #正确：
     win32:CONFIG(release, debug|release): LIBS += -L$$PWD/quazipPack/lib/ -lquazip
     else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/quazipPack/lib/ -lquazipd
     ```

     

3. 同时存在minGW和MSVC编译器，发生冲突

   - 原因：没有正确分辨是使用何种编译器编译

   - 解决：

     - 在不同平台对应的kits中自定义一个宏：
       比如用minGW64编译，则在qmake详情中，找到Addition arguments，写入DEFINES+=PLATFORM_MIGW64，而MSVC为默认不写

     - 在Pro文件使用contains关键字进行区分:

       ```cmake
       if(contains(DEFINES,PLATFORM_MINGW64)){
       
           win32:CONFIG(release, debug|release): LIBS += -L$$PWD/quazipPack/lib/minGW/ -lquazip
           else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/quazipPack/lib/minGW/ -lquazipd
       
           INCLUDEPATH += $$PWD/quazipPack/lib/minGW
           DEPENDPATH += $$PWD/quazipPack/lib/minGW
       }
       else{
       
           win32:CONFIG(release, debug|release): LIBS += -L$$PWD/quazipPack/lib/VC2015/ -lquazip
           else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/quazipPack/lib/VC2015/ -lquazipd
       
           INCLUDEPATH += $$PWD/quazipPack/lib/VC2015
           DEPENDPATH += $$PWD/quazipPack/lib/VC2015
       }
       ```

       

     - 这样每次编译或运行不同平台的时候，Pro文件就会自动去链接不同路径下的库，非常方便