# Robod 相机配置

首先需要在Roboshop.db数据库中添加接口，倍加福可以只有一个接口因为只需要传送URL，图漾相机使用SDK所以需要不同的请求执行不同的命令

![image-20211213181332084](Robod相机配置.assets/image-20211213181332084.png)

Roboshop和Robod通过端口19208通信

Roboshop通过RoCoreTcp给Robod发送和接收消息

```cpp
//_coreTcp初始化
_coreTcp = new RoCoreTcp(this);
_coreTcp->setPayloadSize(3145728);
connect(_coreTcp,SIGNAL(sigTcpReadData(const QVariant& ,const QByteArray&)),
            this,SLOT(slotCoreTcpReadData(const QVariant&, const QByteArray&)));
_coreTcp->connectHost("127.0.0.1",RobotInterface::E_CoreTcp);

_coreTcp->sendData("127.0.0.1",RobotInterface::E_CoreTcp,QVariant(),map,"robot_core_PFlaser_config_req",RobotInterface::E_CN_UserRole +21);
```



Robod接收后通过请求名称运行该名称的函数，通过下面的函数发送给Roboshop

```cpp
void RobodProInterface::robot_core_PFlaser_config_req(qintptr handle, quint16 type, quint16 number, const QByteArray &byteArray){
    QString url = QJsonDocument::fromJson(byteArray).object()["data"].toString();
    QByteArray b = reply->readAll();
    QByteArray bb = sendData(type, number, b);//整合信息
	sendDataToClient(handle, bb);//发送给Roboshop
}
```

配置静态网络部分
一开始需要配置与激光同一网段，使用命令行配置有线网络IP
在Robod中使用函数 `bool SystemNetwork::setAlxIPconfig(const QVariantMap &map)`

其中linux使用nmcli指令

```
修改IP地址等属性：

 # nmcli connection modify IFACE [+|-]setting.propertyvalue
     setting.property:
         ipv4.addresse        ipv4.gateway
         ipv4.dns1           ipv4.methodmanual | auto

修改配置文件执行生效：systemctl restart network 或 nmcli con reload

nmcli命令生效：nmclicon down eth0 ; nmclicon up eth0
```

```
#配置静态路由，重启系统依然生效

nmcli connection modify connection-name +ipv4.routes "192.168.12.0/24 10.10.10.1"

这样会将 192.168.122.0/24 子网的流量指向位于 10.10.10.1 的网关,同时在 /etc/sysconfig/network-scripts/目录下生产一个route-connection-name的文件，这里记录了这个连接的路由信息

```

发送

```
QVariantMap oneMap;
    oneMap.insert("ip",map.value("ip").toString());
    oneMap.insert("gateway",map.value("gateway").toString());
    oneMap.insert("mask",map.value("mask").toString());
    oneMap.insert("dhcp",map.value("dhcp").toBool());
    this->sendData(oneMap,"robot_core_alxsetip_req",AdvancedCommon::E_CN_Advanced);
```

```
#ifdef Q_OS_WIN
    QString command;
    //dhcp
    if(map.value("dhcp").toBool()){
        command = QString("netsh interface ipv4 set address name = \"%1\" "
                          " source = dhcp ")
                .arg(_currentHumanReadableName);
    }else{
        if(map.value("gateway").toString().simplified()!="..."){
            command = QString("netsh interface ipv4 set address name = \"%1\" "
                              "source = static address = %2 mask = %3 "
                              "gateway = %4  gwmetric = 0") //  gwmetric = 0
                    .arg(_currentHumanReadableName)
                    .arg(ip)
                    .arg(map.value("mask").toString())
                    .arg(map.value("gateway").toString());
        }else {
            command = QString("netsh interface ipv4 set address name = \"%1\" "
                              "source = static address = %2 mask = %3 "
                              "gwmetric = 0") //  gwmetric = 0
                    .arg(_currentHumanReadableName)
                    .arg(ip)
                    .arg(map.value("mask").toString());
        }
    }
    QString info;
    if(!PublicClass::init()->processCmd(command,info)){
        return false;
    }
    //.contains(QStringLiteral("对象已存在")) || info.contains(QStringLiteral("语法不正确"))
    if(info.size() > 10){
        setLastError(info);
        return false;
    }
#endif
```

