# Roboshop发布测试版本

1. 将 `Roboshop3\bin\release\msvc_x64` 文件夹复制出来
2. 将其中的`_libs`删除
3. 运行`Roboshop3\bin\release\msvc_x64\appInfo\setting`中的`批量修改文件名.bat`
4. 将修改后的文件夹打包



## 设置Roboshop模式

1. 调试模式：修改appInfo\setting\Config.ini文件，将`robotDebugMode`设置为1，将`logMode`设置为-1。