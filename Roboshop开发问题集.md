# Roboshop Pro

## 1.修改ip地址

右击机器人，打开机器人文件目录，打开robot_info.json，修改"ip"项

## 2.修改监听端口

进入Robod项目文件目录，进入 Robod-Pro\bin\release\msvc_x64\appInfo\setting，打开Config.ini，修改"port"一项

## 3.连接网络后进行在线升级

针对要更新的程序进行配置，在RobodMake.ini中配置

```
[log]

appPaths=   eudic dic:dic | eudic dat:dat     ;//公开app所在可下载文件目录，可暴露文件，相对路径,格式——文件名:相对路径

[debug]

appPaths=config:0;//一键下载调试文件: 0表示全部下载，-1表示特殊处理； 大于0表示仅提取近期几个文件
resourcePathsdateTimePaths=  ;//app所在目录，安时间下载文件
dateTimeResourcePaths= ;//资源文件所在目录，安时间下载文件


[app]
name = "eudic.exe";//资源管理中名称，exe文件
isListen = true; //是否需要robod监听守护
dirPath = "D://Program Files (x86)//eudic"          ;//程序主目录，包含exe文件的目录
start = ;//启动
stop = ;//停止

[upgrade]
name = "eudic" ;//安装包名称（RObokit-xxxx）用于升级时判断是否为有效的升级包，要升级的文件夹
after="restart" ;//reboot:升级后重启系统(如果为空，则表示重装系统);     restart:升级后重启程序(不重启操作系统)

```

然后将程序项目目录打包成zip，注意要有顶层文件夹即如eudic的主文件夹

将zip拖入升级槽中进行升级

这里需要在程序中修改，变成只需要修改ini文件中的文件夹名即可升级相应程序。

```c
 
#include <QSettings>
//解析ini配置文件
        QString iniPath = "..\\Robod-Pro\\bin\\release\\robodMakeTest\\RobodMake.ini";
        QSettings iniRead(iniPath,QSettings::IniFormat,this);
        QString iniDirName = iniRead.value("/upgrade/name").toString();
```



## 4.服务器响应策略

## 5.获取线程结束返回值进行判断操作

```c
/** -------------需要使用线程类处理的返回值---------
 * @brief RobodProInterface::slotInterfacefinished
 */
void RobodProInterface::slotInterfacefinished()
{

    if(-1 == pRobodProInterfaceThread->result()){
        SCWarning<<sendError(pRobodProInterfaceThread->getHandle(),
                             pRobodProInterfaceThread->lastError(),
                             pRobodProInterfaceThread->threadType(),
                             pRobodProInterfaceThread->getNumber());
        //重启 robokit
        startListenApp();
        return ;
    }
    QByteArray b;
    QString sCD = CDString(pRobodProInterfaceThread->threadType() - 10000);
    //更新 robotkit
    if(sCD == "robot_core_upgrade_robot_req"
            || sCD == "robot_core_reduction_robot_req"){ //把指定机器人备份还原){
        pRobodProInterfaceThread->clearVar();
        b = sendData(pRobodProInterfaceThread->threadType(),
                     pRobodProInterfaceThread->getNumber(),
                     pRobodProInterfaceThread->getVar());

        sendDataToClient(pRobodProInterfaceThread->getHandle(),b);
        PublicClass::init()->waitSTime(3);

        //确定是否重启，重启操作系统还是程序
        qDebug("check reboot");
        if(pRobodProInterfaceThread->getUpgradeSelf()){
            pRobodProInterfaceThread->rebootSelf();
        }else if(pRobodProInterfaceThread->getUpgradeAfterReboot()){
            reboot();
        }else startListenApp();

        return;
    }
```



## 6.WINDOWS的查看和杀死进程

![92cb7692c9778f06f881883175a4c34](C:\Users\lesliex\AppData\Local\Temp\WeChat Files\92cb7692c9778f06f881883175a4c34.png)

```c
int PublicClass::getPid(const QString &appName)
{
#ifdef Q_OS_LINUX
      QString cmd = QString("pgrep %1 -x").arg(appName);
      
#endif
#ifdef Q_OS_WIN
      QString cmd = QString("tasklist | findstr %1").arg(appName);
#endif
    QString info;
    if(!processCmd(cmd,info)){
        SCWarning<<getLastError();
        return 0;
    }
    //去除字符前后空格
    QString pidStr = info.trimmed();
    if(pidStr.contains(" ")){
        SCWarning<<"!!!!!!!!!!!more pidStr:"<<pidStr;
        pidStr = pidStr.split(" ").first();
    }
    return pidStr.toInt();
}

bool PublicClass::killPid(int pid)
{
    if(pid <= 0){
        SCWarning<<QString("pid <=0 ");
        return false;
    }
    QProcess process;
#ifdef Q_OS_LINUX
    int ret = process.execute(QString("kill -9 %1").arg(pid)); //kill -9
#endif
#ifdef Q_OS_WIN
    int ret = process.execute(QString("taskkill /PID %1 -t -f").arg(pid)); 
#endif
    if(0 !=ret){
        SCWarning<<QString("kill pid failed：%1 execute ret: %2 ").arg(pid).arg(ret);
        process.close();
        return false;
    }
    SCDebug<<QString("kill pid sucess:%1 execute ret: %2").arg(pid).arg(ret);
    process.close();
    return true;
}

void RobodProInterfaceThread::rebootSelf()
{
    //kill robod.sh
    int pid = PublicClass::init()->getPid("robod.sh");
    PublicClass::init()->killPid(pid);
    QString cmd = QString("./robod.sh restart & ");
    SCDebug<<"cmd:"<<cmd;
    //    process->start("/bin/bash",QStringList()<<"-c"<<cmd);
    QProcess::startDetached(cmd);
    QCoreApplication::exit();
//            PublicClass::init()->reboot();
}

```

