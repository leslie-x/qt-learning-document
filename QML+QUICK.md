QML+QUICK

```javascript
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")
    Column{
        anchors.centerIn: parent
        Button{
            width: 100
            height:50
            text: "hello"
            onClicked: {
                lab.text="pressed";
                name.text="yellow";
                myrect.color="blue";
            }
        }
        Label{
            id:lab
            width: 100
            height:50
            text: "hello"
        }
        Rectangle{
            id:myrect
            width: 100
            height:50
            color: "yellow"
            Text {
                id: name
                text: qsTr("text")
            }
        }
    }
}
```

<img src="QML+QUICK.assets/image-20211123161259235.png" alt="image-20211123161259235" style="zoom:50%;" />

```javascript
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Item {
    anchors.fill: parent
    //定义属性别名,为了在page.qml中引用各控件
    property alias topic: topic //为了在外部文档中也可以访问topic
    property alias author: author

    SwipeView {
        id: view
        currentIndex: pid.currentIndex //保持标签同步
        anchors.fill: parent

        Page {
            title: qsTr("star")
            Image {
                width: 500
                height: 400
                source: "images/star.jpg"
            }
        }
        Page {
            title: qsTr("flower")
            Image {
                width: 500
                height: 400
                source: "images/sunflower.jpg"
            }
        }
    }
    PageIndicator {
        id: pid
        interactive: true
        count: view.count
        currentIndex: view.currentIndex

        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
    Label {
        id: topic
        text: view.currentItem.title
        anchors.right: parent.right
        anchors.rightMargin: 250
        TextArea {
            id: author
            anchors.top: parent.bottom
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/


```

二、

动态创建

<img src="QML+QUICK.assets/image-20211124194320842.png" alt="image-20211124194320842" style="zoom:50%;" />

```cpp
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQml 2.12

Window {
    id:mywindow
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    Component{//部件
        id:component

        Rectangle{
            parent: mywindow
            id:rect
            width: 50;height: 50;
            color: "red"
            function haha(){color="green"}
            function huhu(){anchors.bottom = parent.bottom;}
        }
    }

    Loader{//动态创建部件
        id:loader
        anchors.bottom: parent.bottom
    }

    Column{
        width: createbtn.width;height: 4*createbtn.height;

        Button{//创建方块
            id:createbtn
            text: "create rect"
            onClicked: {
                loader.sourceComponent=component;
            }
        }

        Button{
            text: "be green"
            onClicked:{
                loader.item.haha();//通过loader.item访问对象
            }
        }

        Button{//销毁方块
            text: "delete rect"
            onClicked: {
                loader.sourceComponent=undefined;
                loader.source=""
            }
        }


        Button{//使用createObject创建，可以使用其中的方法
            text: "obj create rect"
            onClicked:{
                var item = component.createObject(parent);
                item.haha();
            }
        }
    }

}

```

