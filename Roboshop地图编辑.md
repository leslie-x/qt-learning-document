# Roboshop地图编辑

窗口类`RobomapWindow`，视图类`RobomapView`

重写

```cpp
pRobomapWindow->map_view()->loadMap(mapFilePath,0,true);


pRobomapWindow->map_view()->map_scene_->setMapChanged(false);

pRobomapWindow->map_view()->fitView();
    
```

![image-20220119161255965](Roboshop地图编辑.assets/image-20220119161255965.png)



加入右键菜单

```cpp
/**鼠标右击自定义菜单
 * @brief RobomapScene::contextMenuEvent
 * @param event
 */
void RobomapScene::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    //...
   if(_selectedMarkItemList.size() > 1
                            && _findItemList.size()
                            &&( _findItemList.first()->type() == BasicGraphicsItem::Mark
                                )) {
                        //                    SCDebug<<"_selectedMarkItemList.size():"<<_selectedMarkItemList.size();
                        ac = menu->addAction(tr("Align horizontally with the site"));
                        ac->setData(E_BatchAlignHorizontally);
                        ac = menu->addAction(tr("Align vertically with the site"));
                        ac->setData(E_BatchAlignVertically);
                        //批量创建
                        ac = menu->addAction(tr("Batch create sites & bins"));
                        ac->setData(E_BatchCreation);
                    }
                    if(0 == _findItemList.size()){
                        ac = menu->addAction(tr("Mouse-centered paste"));
                        ac->setData(E_PasteAllItem);
                        ac = menu->addAction(tr("Paste the coordinates before copying"));
                        ac->setData(E_PasteAllItemKeepPoint);
                    }
                    if(isCanCopy){
                        ac = menu->addAction(tr("Delete"));
                        ac->setData(E_DeleteAll);
                    }
                    _isShowMenu = true;
                    ac = menu->exec(glMousePos);
                    if(ac){
                        switch (ac->data().toInt()) {
                        case E_CopyAll:
                            if(copySelectedItems()){
                                UiClass::init()->showToastr(tr("Copy data to clipboard successfully"));
                            }else{
                                UiClass::init()->showToastr(tr("Copy data to clipboard failed"),SCToastr::E_Warning);
                            }
                            break;
                        case E_PasteAllItem:
                            if(pasteClipboardItems(false)){
                                UiClass::init()->showToastr(tr("Paste clipboard data successfully"));
                            }else{
                                UiClass::init()->showToastr(tr("Paste clipboard data failed"),SCToastr::E_Warning);
                            }
                            break;
                        case E_PasteAllItemKeepPoint:
                            if(pasteClipboardItems(true)){
                                UiClass::init()->showToastr(tr("Paste clipboard data successfully"));
                            }else{
                                UiClass::init()->showToastr(tr("Paste clipboard data failed"),SCToastr::E_Warning);
                            }
                            break;
                        case E_DeleteAll:
                            slotDoDelete();
                            break;
                        case E_BatchAlignHorizontally:
                            setItemAglin(16,QList<QGraphicsItem*>(),dynamic_cast<BasicGraphicsItem*>(_findItemList.first())->getMenuActionText());
                            break;
                        case E_BatchAlignVertically:
                            setItemAglin(14,QList<QGraphicsItem*>(),dynamic_cast<BasicGraphicsItem*>(_findItemList.first())->getMenuActionText());
                            break;

                        case E_BatchCreation:
                            if(copySelectedItems()){
                                UiClass::init()->showToastr(tr("Copy data to clipboard successfully"));
                            }else{
                                UiClass::init()->showToastr(tr("Copy data to clipboard failed"),SCToastr::E_Warning);
                            }
                            break;

                        default:
                            break;
                        }
}
```

