# QT四叉树

头文件

```cpp
#ifndef QUADTREECLASS_H
#define QUADTREECLASS_H

#include <QCoreApplication>
#include <QRectF>
#include <QVector>

struct QTreeNode
{
    union
    {
        struct { QTreeNode* _I, * _II, * _III, * _IV; };	//以笛卡尔坐标系分割象限
        QTreeNode* _children[4];
    };
    QVector<QPointF*> _pointList;			//分割前存储点集
    QRectF _areaRect;				//每个节点的区域大小
    bool _isDivided;			//判断是否已分割，若点集已满再加入点则需要分割
    QTreeNode();
    QTreeNode(QRectF rect, QVector<QPointF*> points, QTreeNode* children[4] = {nullptr});
};

class QuadTree
{
private:

    QTreeNode* _tree;		//四叉树的根
    uint8_t _MaxPoints;		//点集最多容量
    uint8_t _minWidth;         //最小区域宽度
    uint8_t _minHeight;        //最小区域高度

public:
    QuadTree(QRectF rect, uint8_t maxPoints,uint8_t minWidth=2, uint8_t minHeight=2);
    ~QuadTree();

public:
    bool Insert(QPointF& point);    //插入点
    void Clear();                   //清空树

private:
    void Destory(QTreeNode* tree);				//销毁节点
    bool InsertPoint(QTreeNode* root, QPointF& point);
};

#endif // QUADTREECLASS_H

```

源文件

```cpp
#include "QuadTreeClass.h"

QuadTree::QuadTree(QRectF rect, uint8_t maxPointsInRect, uint8_t minWidth, uint8_t minHeight)
    :_MaxPoints(maxPointsInRect)
{
    _tree = new QTreeNode(rect, QVector<QPointF*>());

    _minWidth = minWidth;
    _minHeight = minHeight;
}

QuadTree::~QuadTree()
{
    Destory(_tree);
    _tree = nullptr;
}

bool QuadTree::Insert(QPointF& point)
{
    return InsertPoint(_tree,point);
}

void QuadTree::Clear()
{
    for (int i = 0; i < 4; ++i)
    {
        Destory(_tree->_children[i]);
        _tree->_children[i] = nullptr;
    }
    _tree->_pointList.clear();
    _tree->_isDivided = false;
}

void QuadTree::Destory(QTreeNode* tree)
{
    if (tree != nullptr)
    {
        /* 递归销毁孩子 */
        for (int i = 0; i < 4; ++i)	Destory(tree->_children[i]);
        delete tree;
    }
}

bool QuadTree::InsertPoint(QTreeNode* root, QPointF& point)
{
    if (root != nullptr)
    {
        if (!root->_areaRect.contains(point)) return false;

        if (root->_pointList.count() < _MaxPoints)		// 当前区域可以插入该点
        {
            root->_pointList.append(&point);
            return true;
        }
        else if (root->_areaRect.width() <= _minWidth ||  root->_areaRect.height() <= _minHeight) return false;
        else if (root->_isDivided)					// 当前节点已经分裂过了
        {
            for (int i = 0; i < 4; ++i)
            {
                if (InsertPoint(root->_children[i], point))
                {
                    return true;
                }
            }
            return false;
        }
        else										// 分列节点
        {
            root->_isDivided = true;
            QRectF cur = root->_areaRect;
            /* 创建所有孩子 */
            root->_I = new QTreeNode(QRectF(cur.x() + cur.width() * 0.5f, cur.y(), cur.width() * 0.5f, cur.height() * 0.5f),QVector<QPointF*>());
            root->_II = new QTreeNode(QRectF(cur.x(), cur.y(), cur.width() * 0.5f, cur.height() * 0.5f),QVector<QPointF*>());
            root->_III = new QTreeNode(QRectF(cur.x(), cur.y()+cur.height() * 0.5f, cur.width() * 0.5f, cur.height() * 0.5f),QVector<QPointF*>());
            root->_IV = new QTreeNode(QRectF(cur.x() + cur.width() * 0.5f, cur.y()+cur.height() * 0.5f, cur.width() * 0.5f, cur.height() * 0.5f),QVector<QPointF*>());
            /* 将所有点插入到孩子中 */
            for (int i = 0; i < root->_pointList.count(); ++i)
            {
                for (int j = 0; j < 4; ++j)
                {
                    if (InsertPoint(root->_children[j], *root->_pointList[i])) break;
                }
            }

            /* 清除当前的点索引 */
            root->_pointList.clear();

            /* 插入新节点 */
            for (int i = 0; i < 4; ++i)
            {
                if (InsertPoint(root->_children[i], point))
                {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    return false;
}

QTreeNode::QTreeNode()
    :_I(nullptr),_II(nullptr),  _III(nullptr),_IV(nullptr)
    , _pointList(QVector<QPointF*>()), _areaRect(QRectF()), _isDivided(false)
{

}

QTreeNode::QTreeNode(QRectF rect, QVector<QPointF*> points, QTreeNode* children[4])
    :_I(nullptr), _II(nullptr), _III(nullptr), _IV(nullptr),
      _pointList(points), _areaRect(rect), _isDivided(false)
{
    if (children != nullptr)
    {
        _I = children[0];
        _II = children[1];
        _III = children[2];
        _IV = children[3];
    }
}

```

