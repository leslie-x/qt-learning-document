# Robod编写API

```cpp
bool RoMainObject::init(int argc, char* argv[])
```



```cpp
//========Interface============
    _httpserver->route("/api/restart", QHttpServerRequest::Method::Post,[this](const QHttpServerRequest& req) {
        QVariantMap bodyMap = PublicClass::init()->getJsonVariant(req.body()).toMap();
        qDebug() << "body:" << bodyMap;
        QByteArray b = "restart";
        return b;
    });
```



```cpp
    _httpserver->route("/api/hello", QHttpServerRequest::Method::Get, [] (QHttpServerResponder&& resp, const QHttpServerRequest& req) {
        qDebug() << "url:" << req.url();
        QByteArray b = "hello";
        resp.write(b,"text/txt");
    });
    _httpserver->route("/api/hello", QHttpServerRequest::Method::Get,[]() {
        return "hello";
    });
```



如果希望自定义HTTP响应的头，则可以使用底层API，QHttpServerResponder.

```cpp
QHttpServer server;
server.route( "/blog/" , [] ( int year, QHttpServerResponder &&responder) {
     responder.write(blogs_by_year(year), "text/plain" );
});
```