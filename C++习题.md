# C++习题

### 正则表达式

1. 在 Web 服务器开发中，我们通常希望服务某些满足某个条件的路由。正则表达式便是完成这一目标的工具之一。

给定如下请求结构：

```
struct Request {
    // request method, POST, GET; path; HTTP version
    std::string method, path, http_version;
    // use smart pointer for reference counting of content
    std::shared_ptr<std::istream> content;
    // hash container, key-value dict
    std::unordered_map<std::string, std::string> header;
    // use regular expression for path match
    std::smatch path_match;
};
```

请求的资源类型：

```
typedef std::map<
    std::string, std::unordered_map<
        std::string,std::function<void(std::ostream&, Request&)>>> resource_type;
```

以及服务端模板：

```
template <typename socket_type>
class ServerBase {
public:
    resource_type resource;
    resource_type default_resource;

    void start() {
        // TODO
    }
protected:
    Request parse_request(std::istream& stream) const {
        // TODO
    }
}
```

请实现成员函数 `start()` 与 `parse_request`。使得服务器模板使用者可以如下指定路由：

```
template<typename SERVER_TYPE>
void start_server(SERVER_TYPE &server) {

    // process GET request for /match/[digit+numbers], e.g. GET request is /match/abc123, will return abc123
    server.resource["fill_your_reg_ex"]["GET"] = [](ostream& response, Request& request) {
        string number=request.path_match[1];
        response << "HTTP/1.1 200 OK\r\nContent-Length: " << number.length() << "\r\n\r\n" << number;
    };

    // peocess default GET request; anonymous function will be called if no other matches
    // response files in folder web/
    // default: index.html
    server.default_resource["fill_your_reg_ex"]["GET"] = [](ostream& response, Request& request) {
        string filename = "www/";

        string path = request.path_match[1];

        // forbidden use `..` access content outside folder web/
        size_t last_pos = path.rfind(".");
        size_t current_pos = 0;
        size_t pos;
        while((pos=path.find('.', current_pos)) != string::npos && pos != last_pos) {
            current_pos = pos;
            path.erase(pos, 1);
            last_pos--;
        }

        // (...)
    };

    server.start();
}
```

参考答案[见此](https://changkun.de/modern-cpp/exercises/6)。



### 并发

```cpp
// condition_variable example
#include <iostream>           // std::cout
#include <thread>             // std::thread
#include <mutex>              // std::mutex, std::unique_lock
#include <condition_variable> // std::condition_variable

std::mutex mtx;
std::condition_variable cv;
bool ready = false;

void print_id (int id) {
  std::unique_lock<std::mutex> lck(mtx);
  while (!ready) cv.wait(lck);
  // ...
  std::cout << "thread " << id << '\n';
}

void go() {
  std::unique_lock<std::mutex> lck(mtx);
  ready = true;
  cv.notify_all();
}

int main ()
{
  std::thread threads[10];
  // spawn 10 threads:
  for (int i=0; i<10; ++i)
    threads[i] = std::thread(print_id,i);

  std::cout << "10 threads ready to race...\n";
  go();                       // go!

  for (auto& th : threads) th.join();

  return 0;
}

```

[习题](https://changkun.de/modern-cpp/zh-cn/07-thread/#习题)

1. 请编写一个简单的线程池，提供如下功能：

   ```
   ThreadPool p(4); // 指定四个工作线程
   
   // 将任务在池中入队，并返回一个 std::future
   auto f = pool.enqueue([](int life) {
       return meaning;
   }, 42);
   
   // 从 future 中获得执行结果
   std::cout << f.get() << std::endl;
   ```

2. 请使用 `std::atomic<bool>` 实现一个互斥锁。

   ```cpp
   class Foo {
   	std::atomic<bool> first {false};
   	std::atomic<bool> second{false};
   	Foo()  {
   	}
   	void first(std::function<void ()> printFirst) {
   		printFirst();
   		first = true;
   	}
   	void second(std::function<void ()> printSecond) {
   		while (!first)
   			this_thread::sleep_for(chrono::millisecond(1));
   		printSecond();
   		second = true;
   	}
   	void third(std::function<void ()> printThird) {
   		while (!second)
   			this_thread::sleep_for(chrono::millisecond(1));
   		printThird();
   	}
   };
   
   ```

   

[习题](https://changkun.de/modern-cpp/zh-cn/02-usability/#习题)

1. 使用结构化绑定，仅用一行函数内代码实现如下函数：

   ```
   template <typename Key, typename Value, typename F>
   void update(std::map<Key, Value>& m, F foo) {
       // TODO:
   }
   int main() {
       std::map<std::string, long long int> m {
           {"a", 1},
           {"b", 2},
           {"c", 3}
       };
       update(m, [](std::string key) {
           return std::hash<std::string>{}(key);
       });
       for (auto&& [key, value] : m)
           std::cout << key << ":" << value << std::endl;
   }
   ```

2. 尝试用[折叠表达式](https://changkun.de/modern-cpp/zh-cn/02-usability/#折叠表达式)实现用于计算均值的函数，传入允许任意参数。

3. 答案

   ```cpp
   //
   // fold.expression.cpp
   //
   // exercise solution - chapter 2
   // modern cpp tutorial
   //
   // created by changkun at changkun.de
   // https://github.com/changkun/modern-cpp-tutorial
   //
   
   #include <iostream>
   template<typename ... T>
   auto average(T ... t) {
       return (t + ... ) / sizeof...(t);
   }
   int main() {
       std::cout << average(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) << std::endl;
   }
   ```

   ```cpp
   //
   // structured.binding.cpp
   //
   // exercise solution - chapter 2
   // modern cpp tutorial
   //
   // created by changkun at changkun.de
   // https://github.com/changkun/modern-cpp-tutorial
   //
   
   #include <iostream>
   #include <map>
   #include <string>
   #include <functional>
   
   template <typename Key, typename Value, typename F>
   void update(std::map<Key, Value>& m, F foo) {
       for (auto&& [key, value] : m ) value = foo(key);
   }
   
   int main() {
       std::map<std::string, long long int> m {
           {"a", 1},
           {"b", 2},
           {"c", 3}
       };
       update(m, [](std::string key) -> long long int {
           return std::hash<std::string>{}(key);
       });
       for (auto&& [key, value] : m)
           std::cout << key << ":" << value << std::endl;
   }
   ```

   

线程池

ThreadPool.h

```cpp
#pragma once

#include <iostream>
#include<stdlib.h>
#include<thread>
#include<mutex>
#include<condition_variable>
#include<vector>
#include<functional>
#include<queue>
#define N 10
using namespace std;
class ThreadPool{

public:
    //自定义void()的函数类型
    typedef function<void()>Task;

    ThreadPool();
    ~ThreadPool();
    
public:
    size_t initnum;
    //线程数组
    vector<thread>threads ;
    
    //任务队列
    queue<Task>task ;
    
    //互斥锁条件变量
    mutex _mutex ;
    condition_variable cond ;
    
    //线程池工作结束时为真
    bool done ;
    
    //队列是否为空
    bool isEmpty ;
    //队列是否为满
    bool isFull;

public:
    void addTask(const Task&f);
    void start(int num);
    void setSize(int num);
    void runTask();
    void finish();
};

```

cpp

```cpp
#include"ThreadPool.h"
ThreadPool ::ThreadPool():done(false),isEmpty(true),isFull(false){
}

//设置池中初始线程数
void ThreadPool::setSize(int num){
        (*this).initnum = num ;
}

//添加任务
void ThreadPool::addTask(const Task&f){
   
    if(!done){
        //保护共享资源    
        unique_lock<mutex>lk(_mutex);
        
        //要是任务数量到了最大,就等待处理完再添加
        while(isFull){
            cond.wait(lk);
        }

        //给队列中添加任务
        task.push(f);
        
        if(task.size()==initnum)
            isFull = true;
        
        cout<<"Add a task"<<endl;
        isEmpty = false ;
        cond.notify_one();
    }
}

void ThreadPool::finish(){
    
        //线程池结束工作
        for(size_t i =0 ;i<threads.size();i++){
               threads[i].join() ;
        }
}

void ThreadPool::runTask(){
    
    //不断遍历队列,判断要是有任务的话,就执行
    while(!done){

        unique_lock<mutex>lk(_mutex);
        
        //队列为空的话,就等待任务
        while(isEmpty){
            cond.wait(lk);
        }
        
        Task ta ;
        //转移控制快,将左值引用转换为右值引用
        ta = move(task.front());  
        task.pop();
        
        if(task.empty()){
            isEmpty = true ;    
        }    
        
        isFull =false ;
        ta();
        cond.notify_one();
    }
}

void ThreadPool::start(int num){
    
    setSize(num);
    
    for(int i=0;i<num;i++){        
        threads.push_back(thread(&ThreadPool::runTask,this));
    }
}
ThreadPool::~ThreadPool(){   
}

```

test

```cpp
#include <iostream>
#include"ThreadPool.h"
void func(int i){
    cout<<"task finish"<<"------>"<<i<<endl;
}
int main()
{

    ThreadPool p ;
    p.start(N);
    int i=0;

    while(1){
        i++;
        //调整线程之间cpu调度,可以去掉
       this_thread::sleep_for(chrono::seconds(1));
        auto task = bind(func,i);
        p.addTask(task);
    }

    p.finish();
    return 0;
}

```

或者

```cpp
#ifndef _THREADPOOL_H
#define _THREADPOOL_H
#include <vector>
#include <queue>
#include <thread>
#include <iostream>
#include <stdexcept>
#include <condition_variable>
#include <memory> //unique_ptr

const int MAX_THREADS = 1000; //最大线程数目

template <typename T>
class threadPool
{
  public:
	/*默认开一个线程*/
	threadPool(int number = 1);
	~threadPool();
	/*往请求队列＜task_queue＞中添加任务<T *>*/
	bool append(T *request);

  private:
	/*工作线程需要运行的函数,不断的从任务队列中取出并执行*/
	static void *worker(void *arg);
	void run();

  private:
	std::vector<std::thread> work_threads; /*工作线程*/
	std::queue<T *> tasks_queue;		   /*任务队列*/
	std::mutex queue_mutex;
	std::condition_variable condition;  /*必须与unique_lock配合使用*/
	bool stop;
};

template <typename T>
threadPool<T>::threadPool(int number) : stop(false)
{
	if (number <= 0 || number > MAX_THREADS)
		throw std::exception();
	for (int i = 0; i < number; i++)
	{
		std::cout << "创建第" << i << "个线程 " << std::endl;
		/*
		std::thread temp(worker, this);
		不能先构造再插入
		 */
		work_threads.emplace_back(worker, this);
	}
}
template <typename T>
inline threadPool<T>::~threadPool()
{
	/*世上最大　bug 就是因为我写的这个．shit  */
	//work_threads.clear();
	{
		std::unique_lock<std::mutex> lock(queue_mutex);
		stop = true;
	}
	condition.notify_all();
	for (auto &ww : work_threads)
		ww.join();
}
template <typename T>
bool threadPool<T>::append(T *request)
{
	/*操作工作队列时一定要加锁，因为他被所有线程共享*/
	queue_mutex.lock();
	tasks_queue.push(request);
	queue_mutex.unlock();
	condition.notify_one(); //线程池添加进去了任务，自然要通知等待的线程
	return true;
}
template <typename T>
void *threadPool<T>::worker(void *arg)
{
	threadPool *pool = (threadPool *)arg;
	pool->run();
	return pool;
}
template <typename T>
void threadPool<T>::run()
{
	while (!stop)
	{
		std::unique_lock<std::mutex> lk(this->queue_mutex);
		/*　unique_lock() 出作用域会自动解锁　*/
		this->condition.wait(lk, [this] { return !this->tasks_queue.empty(); });
		//如果任务队列不为空，就停下来等待唤醒
		if (this->tasks_queue.empty())
		{
			continue;
		}
		else
		{
			T *request = tasks_queue.front();
			tasks_queue.pop();
			if (request)
				request->process();
		}
	}
}
#endif

//测试
#include "threadPool.h"
#include<string>
using namespace std;
class Task
{
	public:
	void process()
	{
		cout << "run........." << endl;
	}
};
int main(void)
{
	threadPool<Task> pool(6);
    std::string str;
	while (1)
	{
			Task *tt = new Task();
			//使用智能指针
			pool.append(tt);
            delete tt;
    }
}

```

