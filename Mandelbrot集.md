# Mandelbrot

```cpp
/*
 * complexnum.cxx
 * 
 * Copyright 2021 lesliex <lesliex@DESKTOP-635K0N1>

fc(z)=z*z+c
 c是起始位置的复数值
 z从0开始
	norm() 函数模板会返回复数的量的平方。
	arg() 模板会返回以弧度为单位的相角，是复数 z 对应的 std::atan(z.imag()/z.real())。
	conj() 函数模板会返回共轭复数，是 a+bi 和 a-bi。
	polar() 函数模板接受量和相角作为参数，并返回和它们对应的复数对象。
	prqj() 函数模板返回的复数是复数参数在黎曼球上的投影。
*/


#include <iostream>
#include <complex>
#include <complex.h>

using namespace std;

complex<double> func(complex<double> z, complex<double> c)
{
	return z*z+c;
}

int main(int argc, char **argv)
{
	complex<double> z(0,0);
	complex<double> c(0.05,0.5);
	for(int x=0;x<10;++x){
		z=func(z,c);
		cout<<sqrtf(norm(z))<<endl;
	}
	
	return 0;
}

```

