# pad

<img src="pad音乐.assets/image-20211214180832175.png" alt="image-20211214180832175" style="zoom:50%;" />

qt播放音频

```cpp
QSound::play("file.wav");

QSound sound(filepath);
sound.setLoops(QSound::Infinite);
sound.play();
```

qt实现录音

界面控件

Midi编辑 https://www.midieditor.org/

简单版：

```cpp
class PadButton : public QPushButton
{
    Q_OBJECT
public:
    PadButton();
    ~PadButton();
    
    void setPath();
    void play();
    void stop();
    
    // QWidget interface
protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
    
private:
    bool _isplaying{false};
    QString _path;
    QSoundEffect* _sound{Q_NULLPTR};
};


#include "PadButton.h"

#include <QSoundEffect>
#include <QFileDialog>

PadButton::PadButton(QWidget* parent): QPushButton(parent)
{
    this->setFixedSize(100,100);
    QSizePolicy sizePo(QSizePolicy::Fixed,QSizePolicy::Fixed);
    this->setSizePolicy(sizePo);
    this->setStyleSheet("background-color:yellow");

    _sound = new QSoundEffect(this);
    _isplaying=false;
}

PadButton::~PadButton(){if(_sound)delete _sound;}

void PadButton::setPath()
{
    _sound->setSource(QFileDialog::getOpenFileUrl(this,"file",QUrl("D:\\"),"*.wav"));
}

void PadButton::play()
{
    _sound->setLoopCount(QSoundEffect::Infinite);//-2
    _sound->play();
    _isplaying=true;
    this->setStyleSheet("background-color:lime");
}

void PadButton::stop()
{
    _sound->stop();
    _isplaying=false;
    this->setStyleSheet("background-color:red");
}

void PadButton::mousePressEvent(QMouseEvent *event)
{
    _isplaying?stop():play();
}

void PadButton::mouseDoubleClickEvent(QMouseEvent *event)
{
    setPath();
}


```

界面布置

```cpp
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QWidget* w = new QWidget(this);
    _layout = new QGridLayout(w);
    int cnt=0;int col=0;
    while(cnt<9){
        if(cnt<3) {_layout->addWidget(new PadButton(this),0,col);col++;}
        else _layout->addWidget(new PadButton(this));
        cnt++;
    }

    this->setCentralWidget(w);
}

```

录音部分

```cpp
void MainWindow::on_actionRecord_triggered()
{
    if(_recorder->state()==QMediaRecorder::StoppedState)
        {
            //保存路径相关设置
            QFileDialog::getSaveFileUrl(this,"file",QUrl("D:\\"),"*.wav");

            //开始音频设置
            _recorder->setOutputLocation(QFileDialog::getSaveFileUrl(this,"file",QUrl("D:\\"),"*.wav"));
            _recorder->setAudioInput(_recorder->defaultAudioInput());

            QAudioEncoderSettings settings;
            settings.setCodec("audio/pcm");
            settings.setSampleRate(44100);
            //settings.setBitRate(ui->cbbitrate->currentText().toInt());
            settings.setChannelCount(1);
            settings.setQuality(QMultimedia::HighQuality);
            settings.setEncodingMode(QMultimedia::ConstantQualityEncoding );

            _recorder->setAudioSettings(settings);
        }

        _recorder->record();
}


void MainWindow::on_actionStop_triggered()
{
    _recorder->stop();
}

```



