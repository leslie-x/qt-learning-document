###### 问题遇到的现象和发生背景 
使用 qzint 库生成DM二维码图片，与网上其他的二维码生成器对比下来，内容一样，但是图片有出入

###### 问题相关代码，请勿粘贴截图 
自己的代码：
```cpp
void QRcodeDialog::on_pushButton_createDMcode_clicked()
{
    QString text = ui->textEdit_QRcode->toPlainText();
    QSize size = ui->label_qrcode->size();
    Zint::QZint bc;
    Zint::QZint::AspectRatioMode ar = Zint::QZint::KeepAspectRatio;
    //init
    bc.setSymbol(BARCODE_DATAMATRIX);//BARCODE_GRIDMATRIX  BARCODE_DATAMATRIX
    bc.setBorderWidth(0);
    bc.setBgColor(QColor(255,255,255,255));
    bc.setText(text);
    bc.setScale(1);//图片放大缩小
    bc.setOption2(ui->comboBox_dm->currentData().toInt());//1[10*10],2[12*12],3[14*14]
    bc.setWhitespace(1);

    QImage image(size,QImage::Format_RGB32);
    QPainter painter(&image);
    //二维码绘制区域
    QRectF rectF2(0,0,size.width(),size.width());
    bc.render(painter,rectF2,0,ar);

    ui->label_qrcode->setPixmap(QPixmap::fromImage(image));
}
```

###### 运行结果及报错内容 
自己生成的：

![img](https://img-mid.csdnimg.cn/release/static/image/mid/ask/737253881346140.jpg "#left")


其他生成器生成的：

![img](https://img-mid.csdnimg.cn/release/static/image/mid/ask/412853881346155.jpg "#left")

两个二维码解析出来都是 10，但是图片不一样

###### 我的解答思路和尝试过的方法 
试过修改ECI、input_mode、security_level 但是都不管用

###### 我想要达到的结果
希望看看问题在哪，使得内容一样，但是生成的DM二维码图片不一样



网上的码

![image-20220211111639802](DM二维码生成问题.assets/image-20220211111639802.png)